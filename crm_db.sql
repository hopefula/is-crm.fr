/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 8.0.22 : Database - crm_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`asformacall_db` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `asformacall_db`;

/*Table structure for table `absences` */

DROP TABLE IF EXISTS `absences`;

CREATE TABLE `absences` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `start_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `medical_certificate` tinyint(1) DEFAULT NULL,
  `user_id` int unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `absences_user_id_foreign` (`user_id`),
  CONSTRAINT `absences_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `absences` */

/*Table structure for table `activities` */

DROP TABLE IF EXISTS `activities`;

CREATE TABLE `activities` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `log_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `causer_id` bigint unsigned DEFAULT NULL,
  `causer_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `source_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `source_id` bigint unsigned DEFAULT NULL,
  `ip_address` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `activities_chk_1` CHECK (json_valid(`properties`))
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `activities` */

insert  into `activities`(`id`,`external_id`,`log_name`,`causer_id`,`causer_type`,`text`,`source_type`,`source_id`,`ip_address`,`properties`,`created_at`,`updated_at`) values 
(1,'','client',1,'App\\Models\\User','Client 1 was assigned to Admin','App\\Models\\Client',1,'','{\"action\":\"created\"}','2020-10-18 15:52:06','2020-10-18 15:52:06'),
(2,'','lead',1,'App\\Models\\User','test was created by Admin and assigned to Admin','App\\Models\\Lead',9,'','{\"action\":\"created\"}','2020-10-18 15:52:31','2020-10-18 15:52:31'),
(3,'','lead',1,'App\\Models\\User','test11 was created by Admin and assigned to Admin','App\\Models\\Lead',19,'','{\"action\":\"created\"}','2020-10-19 23:59:33','2020-10-19 23:59:33');

/*Table structure for table `appointments` */

DROP TABLE IF EXISTS `appointments`;

CREATE TABLE `appointments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `source_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `source_id` bigint unsigned DEFAULT NULL,
  `color` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int unsigned NOT NULL,
  `client_id` int unsigned DEFAULT NULL,
  `start_at` timestamp NULL DEFAULT NULL,
  `end_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `appointments_source_type_source_id_index` (`source_type`,`source_id`),
  KEY `appointments_user_id_foreign` (`user_id`),
  KEY `appointments_client_id_foreign` (`client_id`),
  CONSTRAINT `appointments_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  CONSTRAINT `appointments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `appointments` */

/*Table structure for table `business_hours` */

DROP TABLE IF EXISTS `business_hours`;

CREATE TABLE `business_hours` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `day` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `open_time` time DEFAULT NULL,
  `close_time` time DEFAULT NULL,
  `settings_id` int unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `business_hours_settings_id_foreign` (`settings_id`),
  CONSTRAINT `business_hours_settings_id_foreign` FOREIGN KEY (`settings_id`) REFERENCES `settings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `business_hours` */

insert  into `business_hours`(`id`,`day`,`open_time`,`close_time`,`settings_id`,`created_at`,`updated_at`) values 
(1,'monday','09:00:00','18:00:00',1,'2020-10-10 13:35:15','2020-10-10 13:35:15'),
(2,'tuesday','09:00:00','18:00:00',1,'2020-10-10 13:35:15','2020-10-10 13:35:15'),
(3,'wednesday','09:00:00','18:00:00',1,'2020-10-10 13:35:15','2020-10-10 13:35:15'),
(4,'thursday','09:00:00','18:00:00',1,'2020-10-10 13:35:15','2020-10-10 13:35:15'),
(5,'friday','09:00:00','18:00:00',1,'2020-10-10 13:35:15','2020-10-10 13:35:15'),
(6,'saturday','09:00:00','18:00:00',1,'2020-10-10 13:35:15','2020-10-10 13:35:15'),
(7,'sunday','09:00:00','18:00:00',1,'2020-10-10 13:35:15','2020-10-10 13:35:15');

/*Table structure for table `clients` */

DROP TABLE IF EXISTS `clients`;

CREATE TABLE `clients` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `vat` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_number` bigint DEFAULT NULL,
  `user_id` int unsigned NOT NULL,
  `industry_id` int unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clients_user_id_foreign` (`user_id`),
  KEY `clients_industry_id_foreign` (`industry_id`),
  CONSTRAINT `clients_industry_id_foreign` FOREIGN KEY (`industry_id`) REFERENCES `industries` (`id`),
  CONSTRAINT `clients_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `clients` */

/*Table structure for table `comments` */

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `source_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `source_id` bigint unsigned NOT NULL,
  `user_id` int unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_source_type_source_id_index` (`source_type`,`source_id`),
  KEY `comments_user_id_foreign` (`user_id`),
  CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `comments` */

insert  into `comments`(`id`,`external_id`,`description`,`source_type`,`source_id`,`user_id`,`deleted_at`,`created_at`,`updated_at`) values 
(10,'','<p>Achète  deux abonnement ps stp frérot pour jouer à fifa wAllah</p>','App\\Models\\Lead',16,24,NULL,'2020-11-09 12:26:09','2020-11-09 12:26:09'),
(16,'','<p>OK</p>','App\\Models\\Lead',35,28,NULL,'2020-11-10 04:42:48','2020-11-10 04:42:48'),
(20,'','<p>VU</p>','App\\Models\\Lead',47,10,NULL,'2020-11-11 06:37:39','2020-11-11 06:37:39'),
(21,'','<p>TIPIAKE</p>','App\\Models\\Lead',56,10,'2020-11-11 15:07:34','2020-11-11 10:22:18','2020-11-11 15:07:34'),
(22,'','<p>pirate</p>','App\\Models\\Lead',56,10,'2020-11-11 15:07:32','2020-11-11 15:07:22','2020-11-11 15:07:32'),
(23,'','<p>OK</p>','App\\Models\\Lead',56,10,'2020-11-12 05:31:50','2020-11-12 05:30:51','2020-11-12 05:31:50'),
(24,'','<p>ok</p>','App\\Models\\Lead',59,25,NULL,'2020-11-13 06:17:40','2020-11-13 06:17:40'),
(25,'','<p>Date de creation 13/11  - Date de RDV : 24/11</p>','App\\Models\\Lead',61,49,NULL,'2020-11-13 06:48:58','2020-11-13 06:48:58'),
(26,'','<p>jsdghjqshgdj</p>','App\\Models\\Lead',57,10,'2020-11-13 08:10:13','2020-11-13 08:09:47','2020-11-13 08:10:13'),
(27,'','<p>rdv c\'est à 11h</p>','App\\Models\\Lead',65,39,NULL,'2020-11-13 09:40:06','2020-11-13 09:40:06'),
(28,'','<p>comment</p>','App\\Models\\Lead',1,10,NULL,'2020-11-14 13:19:45','2020-11-14 13:19:45'),
(29,'','<p>lkhklhkj</p>','App\\Models\\Lead',5,10,NULL,'2020-11-15 03:19:32','2020-11-15 03:19:32'),
(30,'','<p>jdjklfjklsdjlfk</p>','App\\Models\\Lead',5,10,NULL,'2020-11-15 03:20:52','2020-11-15 03:20:52'),
(31,'','<p>khklhkjh\r\n</p>','App\\Models\\Lead',5,10,NULL,'2020-11-15 04:23:21','2020-11-15 04:23:21');

/*Table structure for table `contacts` */

DROP TABLE IF EXISTS `contacts`;

CREATE TABLE `contacts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `primary_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `secondary_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` int unsigned NOT NULL,
  `is_primary` tinyint(1) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contacts_client_id_foreign` (`client_id`),
  CONSTRAINT `contacts_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `contacts` */

/*Table structure for table `department_user` */

DROP TABLE IF EXISTS `department_user`;

CREATE TABLE `department_user` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `department_id` int unsigned NOT NULL,
  `user_id` int unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `option` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `department_user_department_id_foreign` (`department_id`),
  KEY `department_user_user_id_foreign` (`user_id`),
  CONSTRAINT `department_user_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `department_user` */

insert  into `department_user`(`id`,`department_id`,`user_id`,`created_at`,`updated_at`,`option`) values 
(2,6,24,'2020-11-14 22:03:43','2020-11-14 22:03:43',NULL),
(3,6,28,'2020-11-14 22:03:43','2020-11-14 22:03:43',NULL),
(4,6,56,NULL,NULL,NULL),
(5,6,57,NULL,NULL,NULL),
(6,6,58,NULL,NULL,NULL),
(8,16,24,'2020-11-15 01:07:14','2020-11-15 01:07:14',NULL),
(9,16,25,'2020-11-15 01:07:14','2020-11-15 01:07:14',NULL),
(10,16,28,'2020-11-15 01:07:14','2020-11-15 01:07:14',NULL),
(12,1,0,'2020-11-15 02:26:47','2020-11-15 02:33:15',NULL),
(13,29,25,'2020-11-15 02:29:10','2020-11-15 02:29:10','4'),
(14,29,28,'2020-11-15 02:29:10','2020-11-15 02:29:10','4'),
(15,1,25,'2020-11-15 02:56:48','2020-11-15 02:56:48',NULL),
(16,1,28,'2020-11-15 02:56:48','2020-11-15 02:56:48',NULL),
(17,6,37,NULL,NULL,NULL),
(18,6,46,NULL,NULL,NULL),
(19,1,25,'2020-11-15 03:00:50','2020-11-15 03:00:50',NULL),
(20,1,28,'2020-11-15 03:00:50','2020-11-15 03:00:50',NULL),
(21,6,59,NULL,NULL,NULL),
(22,6,55,NULL,NULL,NULL),
(23,6,60,NULL,NULL,NULL);

/*Table structure for table `departments` */

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `departments` */

insert  into `departments`(`id`,`external_id`,`name`,`description`,`created_at`,`updated_at`) values 
(1,'f35a8482-cea8-43ed-9313-fd8956ce9f60','Default Department','Default Department','2020-10-30 18:54:41','2020-11-11 07:13:03'),
(6,'f35a8482-cea8-43ed-9313-fd8956ce9f60','is call maroc','Youssouf\r\nChoukri','2020-10-30 18:54:41','2020-11-11 07:13:03'),
(16,'39ea7ece-3e08-4b62-89be-bfa56681c7be','is call Algérie','WALID','2020-11-09 11:53:11','2020-11-11 07:12:40'),
(17,'83a66b24-3145-490a-9789-8bdd650d0aeb','Les Calls','Walid MCH','2020-11-11 05:20:38','2020-11-11 07:13:34'),
(18,'101fea94-3384-4dca-b247-0d16766f2a86','test  simo','sdqsdqsd','2020-11-11 14:43:12','2020-11-11 14:43:12'),
(19,'cb69fea5-fa24-41ce-a774-0f11b236686d','okok','','2020-11-14 14:45:05','2020-11-14 14:45:05'),
(22,'1cb71c1d-9ae5-455d-a66d-422938ebc25e','check2','','2020-11-14 15:32:21','2020-11-14 15:32:21'),
(24,'32a6734f-2be4-499d-91ac-464274bc31f4','88888','88888','2020-11-14 15:36:05','2020-11-14 15:36:05'),
(25,'dc1e2d23-f1ab-48a3-aa9e-3935adf1a867','okok','','2020-11-14 15:37:03','2020-11-14 15:37:03'),
(26,'b5d9fb37-cdcd-48a1-ab3a-42d07e476472','1111','12','2020-11-14 15:46:15','2020-11-14 15:46:15'),
(29,'54ff65d5-dd2c-494a-9e88-f73bed7e5ab9','12323','','2020-11-15 02:29:10','2020-11-15 02:29:10');

/*Table structure for table `documents` */

DROP TABLE IF EXISTS `documents`;

CREATE TABLE `documents` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `original_filename` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mime` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `integration_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `integration_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `source_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `source_id` bigint unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_source_type_source_id_index` (`source_type`,`source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `documents` */

/*Table structure for table `industries` */

DROP TABLE IF EXISTS `industries`;

CREATE TABLE `industries` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `industries` */

insert  into `industries`(`id`,`external_id`,`name`) values 
(1,'52dc21f1-e843-4dbc-920c-f50fceb0d1d3','Accommodations'),
(2,'4f3f6883-7e6d-4b03-8195-9504f5a30176','Accounting'),
(3,'3e2908b8-7398-4809-834b-fd6e85cb3207','Auto'),
(4,'b31858ec-e8bc-41d0-92e3-0abda2ca1f46','Beauty & Cosmetics'),
(5,'a438f732-85ad-4f8d-989a-758bb56612ae','Carpenter'),
(6,'f6b269f5-8212-4af6-aca5-57e98eaa163f','Communications'),
(7,'7db064a8-776c-4a09-8f8c-d42fdf8f2400','Computer & IT'),
(8,'b68290f5-ba0b-4a13-9e1d-650f2c86617b','Construction'),
(9,'32a06ab5-bc5f-41c5-85c0-3e801e8f20ef','Consulting'),
(10,'e6d5b9b5-5451-49e5-a3e7-0ee29c548aaf','Education'),
(11,'02dcdbd5-1193-4fd8-855c-1b1e90a3b4bf','Electronics'),
(12,'84b46049-7d34-4658-a28b-ae1b9184e676','Entertainment'),
(13,'e1e4800f-304a-432e-b1f0-43baf2901d49','Food & Beverages'),
(14,'e1bd7ff4-6691-48f5-9e18-912bdafe011d','Legal Services'),
(15,'44ddb954-24b7-4f87-be0f-108d23b5338c','Marketing'),
(16,'6395052b-86a2-4f86-a790-80836d146260','Real Estate'),
(17,'4f482e75-34ef-4a72-a889-3869a584b83d','Retail'),
(18,'a58b3691-87ff-42a7-8d45-e0c28f778b57','Sports'),
(19,'2ce1a8f1-6f4a-444e-9127-8fdd1141daa4','Technology'),
(20,'8f46cb22-e4bb-4564-9108-b5d81d6b6526','Tourism'),
(21,'a4dc3a1f-a7c7-4684-8513-20e7a2bcebdd','Transportation'),
(22,'8fadf376-12c2-4d94-87b9-5bc86f1d86e7','Travel'),
(23,'5d66b4ab-d3e2-4971-b013-6c76b9bfd8b5','Utilities'),
(24,'723eb5ad-4990-4140-b594-b0ea41cf2b08','Web Services'),
(25,'608d60b1-ef0d-4bd2-8e77-4d965473143b','Other');

/*Table structure for table `integrations` */

DROP TABLE IF EXISTS `integrations`;

CREATE TABLE `integrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `client_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `client_secret` int DEFAULT NULL,
  `api_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `org_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `integrations` */

/*Table structure for table `invoice_lines` */

DROP TABLE IF EXISTS `invoice_lines`;

CREATE TABLE `invoice_lines` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `price` int NOT NULL,
  `invoice_id` int unsigned NOT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `product_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_lines_invoice_id_foreign` (`invoice_id`),
  CONSTRAINT `invoice_lines_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `invoice_lines` */

/*Table structure for table `invoices` */

DROP TABLE IF EXISTS `invoices`;

CREATE TABLE `invoices` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `invoice_number` bigint DEFAULT NULL,
  `sent_at` datetime DEFAULT NULL,
  `due_at` datetime DEFAULT NULL,
  `integration_invoice_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `integration_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` int unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoices_client_id_foreign` (`client_id`),
  CONSTRAINT `invoices_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `invoices` */

/*Table structure for table `leads` */

DROP TABLE IF EXISTS `leads`;

CREATE TABLE `leads` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `lead_type` int DEFAULT NULL,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status_id` int unsigned NOT NULL,
  `Nom` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `lname` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Codepostal` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Ville` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel1` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel2` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile1` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile2` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Demander` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Surface` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Surface_des` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Surface_Comble` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Configuration` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Maison` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Zone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ancienne` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Quelle` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Commentaire` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Mode` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Mode_de` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Situation` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Situation_de` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Emploi` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Quel` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Hauteur` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Hauteursous` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `endroit` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Personne` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `du_mur` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sous_sol` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `isolation` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `comble` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `actuelle` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `niveau` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `descendant` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `agricole` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Plafond` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Metier` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `quelques` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Fiscal1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `RefAvis1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Fiscal2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `RefAvis2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Revenue` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Nom_de` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Date_de` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` int unsigned NOT NULL,
  `user_assigned_id` int unsigned DEFAULT NULL,
  `user_created_id` int unsigned NOT NULL,
  `result` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `deadline` datetime DEFAULT NULL,
  `invoice_id` int unsigned DEFAULT NULL,
  `qualified` tinyint(1) DEFAULT '0',
  `app_created_date` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_confirm_date` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_install_date` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Solutions` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `produit1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Sur_quoi` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Campagne` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `budget_cpf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Formation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Date_de_rdv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Heure_de_rdv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `leads_status_id_foreign` (`status_id`),
  KEY `leads_user_assigned_id_foreign` (`user_assigned_id`),
  KEY `leads_client_id_foreign` (`client_id`),
  KEY `leads_user_created_id_foreign` (`user_created_id`),
  KEY `leads_invoice_id_foreign` (`invoice_id`),
  KEY `leads_qualified_index` (`qualified`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `leads` */

insert  into `leads`(`id`,`lead_type`,`external_id`,`description`,`status_id`,`Nom`,`lname`,`Codepostal`,`address`,`Ville`,`tel1`,`tel2`,`mobile1`,`mobile2`,`email`,`Demander`,`Surface`,`Surface_des`,`Surface_Comble`,`Configuration`,`Maison`,`Zone`,`ancienne`,`Quelle`,`Commentaire`,`Mode`,`Mode_de`,`Situation`,`Situation_de`,`Emploi`,`Quel`,`Nombre`,`Hauteur`,`Hauteursous`,`endroit`,`category`,`Personne`,`du_mur`,`sous_sol`,`isolation`,`comble`,`actuelle`,`niveau`,`descendant`,`agricole`,`Plafond`,`Metier`,`quelques`,`Fiscal1`,`RefAvis1`,`Fiscal2`,`RefAvis2`,`Revenue`,`Nom_de`,`Date_de`,`client_id`,`user_assigned_id`,`user_created_id`,`result`,`title`,`deadline`,`invoice_id`,`qualified`,`app_created_date`,`app_confirm_date`,`app_install_date`,`deleted_at`,`created_at`,`updated_at`,`Solutions`,`produit1`,`Sur_quoi`,`Campagne`,`budget_cpf`,`Formation`,`Date_de_rdv`,`Heure_de_rdv`) values 
(1,1,'b58f1597-f589-4255-8023-986188365def','',8,'1','123','1','1','1','1',NULL,'1',NULL,'121121@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,'[\"G\\u00e9rant\",\"Demandeur d\\u2019emploi\"]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,10,NULL,'',NULL,NULL,0,NULL,NULL,NULL,NULL,'2020-11-14 12:51:04','2020-11-14 16:07:15','',NULL,NULL,NULL,'1','1','0001-01-01','01:01'),
(2,1,'415c0c00-9647-4906-b6c3-8ba8b0cca1e5','',10,'name',NULL,'654','465','46','654',NULL,'32132132132',NULL,'admin@is-crm.fr',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'789',NULL,NULL,'[\"G\\u00e9rant\",\"autres\"]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,10,NULL,'',NULL,NULL,0,NULL,NULL,NULL,NULL,'2020-11-14 13:13:37','2020-11-14 13:13:37','',NULL,NULL,NULL,'211321','321','2020-11-12','08:00'),
(3,1,'09ae59f9-0afc-4c08-a653-b3fb40b8b09b','',7,'uh','jkhjk','hkj','hkj','hkj','hkj',NULL,'hkj',NULL,'hkj@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'hj',NULL,NULL,'[\"G\\u00e9rant\"]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,37,NULL,'',NULL,NULL,0,NULL,NULL,NULL,NULL,'2020-11-14 16:13:16','2020-11-14 16:13:16','',NULL,NULL,NULL,'23','ezrzer','2020-11-14','08:14'),
(4,1,'529c6991-881d-435b-a1c1-1749ff6296b6','',10,'name','123','121','465','46','321',NULL,'321',NULL,'321@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'321',NULL,NULL,'[\"G\\u00e9rant\",\"autres\"]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,55,NULL,'',NULL,NULL,0,NULL,NULL,NULL,NULL,'2020-11-14 22:08:13','2020-11-14 22:08:13','',NULL,NULL,NULL,'211321','654','2020-11-12','17:10'),
(5,1,'c8bfc4b7-45fc-4bf8-aed8-53463173e134','',9,'31','132','132','132','132','132',NULL,'132',NULL,'132@dfd.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1321',NULL,NULL,'[\"Demandeur d\\u2019emploi\"]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,55,NULL,'',NULL,NULL,0,NULL,NULL,NULL,NULL,'2020-11-14 22:15:32','2020-11-14 22:15:32','',NULL,NULL,NULL,'211321','321','2020-11-06','15:16');

/*Table structure for table `mails` */

DROP TABLE IF EXISTS `mails`;

CREATE TABLE `mails` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `body` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int unsigned DEFAULT NULL,
  `send_at` timestamp NULL DEFAULT NULL,
  `sent_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mails_user_id_foreign` (`user_id`),
  CONSTRAINT `mails_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `mails` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

/*Table structure for table `notifications` */

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` char(36) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `notifiable_id` bigint unsigned NOT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `notifications` */

insert  into `notifications`(`id`,`type`,`notifiable_type`,`notifiable_id`,`data`,`read_at`,`created_at`,`updated_at`) values 
('649256f7-3aba-458c-9453-2b31fbd791e5','App\\Notifications\\LeadActionNotification','App\\Models\\User',1,'{\"assigned_user\":1,\"created_user\":1,\"message\":\"test11 was created by Admin and assigned to you\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":19,\"url\":\"http:\\/\\/localhost:8088\\/leads\\/d179ce69-77ac-4c1a-b915-16a0594b1e5f\",\"action\":\"created\"}',NULL,'2020-10-19 23:59:33','2020-10-19 23:59:33'),
('8a4dacd5-88cb-4a0d-b7c7-db979d3df8f1','App\\Notifications\\LeadActionNotification','App\\Models\\User',1,'{\"assigned_user\":1,\"created_user\":1,\"message\":\"test was created by Admin and assigned to you\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":9,\"url\":\"http:\\/\\/localhost:8000\\/leads\\/cd7a9b20-e2fc-42e6-b857-39b92863d1d4\",\"action\":\"created\"}',NULL,'2020-10-18 15:52:31','2020-10-18 15:52:31'),
('c18ad54c-f182-4b88-8200-f67108342b2f','App\\Notifications\\ClientActionNotification','App\\Models\\User',1,'{\"assigned_user\":1,\"created_user\":1,\"message\":\"Client 1 was assigned to you\",\"type\":\"App\\\\Models\\\\Client\",\"type_id\":1,\"url\":\"http:\\/\\/localhost:8000\\/clients\\/58d6af2b-8476-4278-bf5b-c1ed10b44367\",\"action\":\"created\"}',NULL,'2020-10-18 15:52:06','2020-10-18 15:52:06');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `password_resets` */

insert  into `password_resets`(`email`,`token`,`created_at`) values 
('mohamed@gmail.com','$2y$10$FSTM4.40RGBUnXLBXPuhlOC85GRwVSs0/yzi9aOPuLcCR8.yyNfHC','2020-10-23 09:07:27'),
('choukri.elbarodi@gmail.com','$2y$10$iCGVQBdPl8rLAQPaOZCvTeA0dylaYhwNvJ6fLVy60h6pJwoOFLMai','2020-11-12 06:33:45');

/*Table structure for table `payments` */

DROP TABLE IF EXISTS `payments`;

CREATE TABLE `payments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `amount` int NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `payment_source` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `payment_date` date NOT NULL,
  `integration_payment_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `integration_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_id` int unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payments_invoice_id_foreign` (`invoice_id`),
  CONSTRAINT `payments_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `payments` */

/*Table structure for table `permission_role` */

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `permission_id` int unsigned NOT NULL,
  `role_id` int unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `permission_role` */

insert  into `permission_role`(`permission_id`,`role_id`) values 
(1,1),
(2,1),
(3,1),
(4,1),
(5,1),
(6,1),
(7,1),
(8,1),
(9,1),
(10,1),
(11,1),
(12,1),
(13,1),
(14,1),
(15,1),
(16,1),
(17,1),
(18,1),
(19,1),
(20,1),
(21,1),
(22,1),
(23,1),
(24,1),
(25,1),
(26,1),
(27,1),
(28,1),
(29,1),
(30,1),
(31,1),
(32,1),
(33,1),
(1,2),
(2,2),
(3,2),
(19,2),
(20,2),
(19,4),
(19,5);

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `grouping` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`external_id`,`name`,`display_name`,`description`,`grouping`,`created_at`,`updated_at`) values 
(1,'','user-create','Create user','Be able to create a new user','user','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(2,'','user-update','Update user','Be able to update a user\'s information','user','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(3,'','user-delete','Delete user','Be able to delete a user','user','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(4,'','client-create','Create client','Permission to create client','client','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(5,'','client-update','Update client','Permission to update client','client','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(6,'','client-delete','Delete client','Permission to delete client','client','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(7,'','document-delete','Delete document','Permission to delete a document associated with a client','document','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(8,'','document-upload','Upload document','Be able to upload a document associated with a client','document','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(9,'','task-create','Create task','Permission to create task','task','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(10,'','task-update-status','Update task status','Permission to update task status','task','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(11,'','task-update-deadline','Change task deadline','Permission to update a tasks deadline','task','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(12,'','can-assign-new-user-to-task','Change assigned user','Permission to change the assigned user on a task','task','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(13,'','task-update-linked-project','Changed linked project','Be able to change the project which is linked to a task','task','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(14,'','task-upload-files','Upload files to task','Allowed to upload files for a task','task','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(15,'','modify-invoice-lines','Modify invoice lines on a invoice / task','Permission to create and update invoice lines on task, and invoices','invoice','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(16,'','invoice-see','See invoices and it\'s invoice lines','Permission to see invoices on customer, and it\'s associated task','invoice','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(17,'','invoice-send','Send invoices to clients','Be able to set an invoice as send to an customer (Or Send it if billing integration is active)','invoice','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(18,'','invoice-pay','Set an invoice as paid','Be able to set an invoice as paid or not paid','invoice','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(19,'','lead-create','Create lead','Permission to create lead','lead','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(20,'','lead-update-status','Update lead status','Permission to update lead status','lead','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(21,'','lead-update-deadline','Change lead deadline','Permission to update a lead deadline','lead','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(22,'','can-assign-new-user-to-lead','Change assigned user','Permission to change the assigned user on a lead','lead','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(23,'','project-create','Create project','Permission to create project','project','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(24,'','project-update-status','Update project status','Permission to update project status','project','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(25,'','project-update-deadline','Change project deadline','Permission to update a projects deadline','project','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(26,'','can-assign-new-user-to-project','Change assigned user','Permission to change the assigned user on a project','project','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(27,'','project-upload-files','Upload files to project','Allowed to upload files for a project','project','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(28,'','payment-create','Add payment','Be able to add a new payment on a invoice','payment','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(29,'','payment-delete','Delete payment','Be able to delete a payment','payment','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(30,'','calendar-view','View calendar','Be able to view the calendar for appointments','appointment','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(31,'','appointment-create','Add appointment','Be able to create a new appointment for a user','appointment','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(32,'','appointment-edit','Edit appointment','Be able to edit appointment such as times and title','appointment','2020-10-10 13:35:15','2020-10-10 13:35:15'),
(33,'','appointment-delete','Delete appointment','Be able to delete an appointment','appointment','2020-10-10 13:35:15','2020-10-10 13:35:15');

/*Table structure for table `projects` */

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status_id` int unsigned NOT NULL,
  `user_assigned_id` int unsigned NOT NULL,
  `user_created_id` int unsigned NOT NULL,
  `client_id` int unsigned NOT NULL,
  `invoice_id` int unsigned DEFAULT NULL,
  `deadline` date NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_status_id_foreign` (`status_id`),
  KEY `projects_user_assigned_id_foreign` (`user_assigned_id`),
  KEY `projects_user_created_id_foreign` (`user_created_id`),
  KEY `projects_client_id_foreign` (`client_id`),
  KEY `projects_invoice_id_foreign` (`invoice_id`),
  CONSTRAINT `projects_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `projects_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`),
  CONSTRAINT `projects_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`),
  CONSTRAINT `projects_user_assigned_id_foreign` FOREIGN KEY (`user_assigned_id`) REFERENCES `users` (`id`),
  CONSTRAINT `projects_user_created_id_foreign` FOREIGN KEY (`user_created_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `projects` */

/*Table structure for table `role_user` */

DROP TABLE IF EXISTS `role_user`;

CREATE TABLE `role_user` (
  `user_id` int unsigned NOT NULL,
  `role_id` int unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `role_user` */

insert  into `role_user`(`user_id`,`role_id`) values 
(10,2),
(24,4),
(25,4),
(28,4),
(36,4),
(37,5),
(38,5),
(39,5),
(40,5),
(42,5),
(45,5),
(46,5),
(47,5),
(48,5),
(49,5),
(50,5),
(51,5),
(53,5),
(54,5),
(55,5),
(56,5),
(57,5),
(58,5),
(59,5),
(60,5);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`external_id`,`name`,`display_name`,`description`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'7ed74ed1-f1af-4ba0-bbd5-fcc2b9a76e58','owner','Administrator','Owner','2020-10-10 13:35:15','2020-10-10 13:35:15','2020-10-20 17:28:49'),
(2,'fdbc85e0-0c57-497e-9279-30382c89734c','administrator','Administrator','System Administrator','2020-10-10 13:35:15','2020-10-10 13:35:15',NULL),
(4,'f4b82bca-42c0-49e4-9183-e0765f042f84','supervisor','Supervisor','Supervisor','2020-10-10 13:35:15','2020-10-10 13:35:15',NULL),
(5,'f4b92bca-42c0-49e4-9183-e0765f042f96','agent','Agent','agent is the end of the users','2020-10-21 00:21:44','2020-10-21 00:21:44',NULL);

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `client_number` int NOT NULL,
  `invoice_number` int NOT NULL,
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` varchar(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'USD',
  `vat` smallint NOT NULL DEFAULT '725',
  `max_users` int NOT NULL,
  `language` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'EN',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `settings` */

insert  into `settings`(`id`,`client_number`,`invoice_number`,`country`,`company`,`currency`,`vat`,`max_users`,`language`,`created_at`,`updated_at`) values 
(1,10001,10000,'en','Media','USD',725,10,'EN',NULL,'2020-10-18 15:52:04');

/*Table structure for table `statuses` */

DROP TABLE IF EXISTS `statuses`;

CREATE TABLE `statuses` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `source_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '#000000',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `statuses` */

insert  into `statuses`(`id`,`external_id`,`title`,`source_type`,`color`,`created_at`,`updated_at`) values 
(1,'3b9820e8-1982-4b4f-90cc-9109e9859270','Open','App\\Models\\Task','#2FA599','2020-10-10 13:35:14','2020-10-10 13:35:14'),
(2,'bf47d7d9-fb1a-45f7-80ce-69954d224ce3','In-progress','App\\Models\\Task','#2FA55E','2020-10-10 13:35:14','2020-10-10 13:35:14'),
(3,'473259ff-1f43-47b0-ad58-389a9b4c7b46','Pending','App\\Models\\Task','#EFAC57','2020-10-10 13:35:14','2020-10-10 13:35:14'),
(4,'0be9dae8-0f41-4139-9e3f-f24006d59892','Waiting client','App\\Models\\Task','#60C0DC','2020-10-10 13:35:14','2020-10-10 13:35:14'),
(5,'a839b48d-aea3-442e-ba7f-deff98820dfe','Blocked','App\\Models\\Task','#E6733E','2020-10-10 13:35:14','2020-10-10 13:35:14'),
(6,'070a7e63-426f-406d-9186-a3f68fd07a46','Closed','App\\Models\\Task','#D75453','2020-10-10 13:35:14','2020-10-10 13:35:14'),
(7,'be1cc886-4eed-419c-9694-f0256fae52aa','Nouveau lead','App\\Models\\Lead','WHITE','2020-10-10 13:35:14','2020-10-10 13:35:14'),
(8,'3c0d8055-c507-4426-a907-49ed41433ba7','Vente','App\\Models\\Lead','#ff414d','2020-10-10 13:35:14','2020-10-10 13:35:14'),
(9,'9d50bf5a-734b-4517-a76b-22dd0062433c','Annulé','App\\Models\\Lead','#f1fa3c','2020-10-10 13:35:14','2020-10-10 13:35:14'),
(10,'be959c5d-e761-4990-9563-043e96b6ce6b','A retraiter télépro','App\\Models\\Lead','#28df99','2020-10-10 13:35:14','2020-10-10 13:35:14'),
(11,'993680df-80a4-4917-a370-eda9142d1ad7','NRP','App\\Models\\Lead','#2FA599','2020-10-10 13:35:14','2020-10-10 13:35:14'),
(12,'9b91c201-0c21-4346-a61d-2f4d583415dd','In-progress','App\\Models\\Project','#3CA3BA','2020-10-10 13:35:14','2020-10-10 13:35:14'),
(14,'3ca82e74-d523-43be-b636-f3a3a061df96','Cancelled','App\\Models\\Project','#821414','2020-10-10 13:35:14','2020-10-10 13:35:14'),
(15,'c9a0e41f-e9e5-42ed-884b-e37b09a40705','En attente chez AL','App\\Models\\Project','#D75453','2020-10-10 13:35:14','2020-10-10 13:35:14');

/*Table structure for table `subscriptions` */

DROP TABLE IF EXISTS `subscriptions`;

CREATE TABLE `subscriptions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `stripe_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `stripe_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `stripe_plan` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subscriptions_user_id_stripe_status_index` (`user_id`,`stripe_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `subscriptions` */

/*Table structure for table `tasks` */

DROP TABLE IF EXISTS `tasks`;

CREATE TABLE `tasks` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status_id` int unsigned NOT NULL,
  `user_assigned_id` int unsigned NOT NULL,
  `user_created_id` int unsigned NOT NULL,
  `client_id` int unsigned NOT NULL,
  `invoice_id` int unsigned DEFAULT NULL,
  `project_id` int unsigned DEFAULT NULL,
  `deadline` date NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tasks_status_id_foreign` (`status_id`),
  KEY `tasks_user_assigned_id_foreign` (`user_assigned_id`),
  KEY `tasks_user_created_id_foreign` (`user_created_id`),
  KEY `tasks_client_id_foreign` (`client_id`),
  KEY `tasks_invoice_id_foreign` (`invoice_id`),
  KEY `tasks_project_id_foreign` (`project_id`),
  CONSTRAINT `tasks_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tasks_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE SET NULL,
  CONSTRAINT `tasks_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE SET NULL,
  CONSTRAINT `tasks_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`),
  CONSTRAINT `tasks_user_assigned_id_foreign` FOREIGN KEY (`user_assigned_id`) REFERENCES `users` (`id`),
  CONSTRAINT `tasks_user_created_id_foreign` FOREIGN KEY (`user_created_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tasks` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `primary_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `secondary_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'EN',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `option` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`external_id`,`name`,`email`,`password`,`address`,`primary_number`,`secondary_number`,`image_path`,`remember_token`,`language`,`deleted_at`,`created_at`,`updated_at`,`option`) values 
(10,'79ed51d3-8762-4f50-b642-36f476d9e2fe','admin','admin@is-crm.fr','$2a$10$ThNOk6GkkNKIdYA9ewUc2eA9ZE55eBbJBJRKHWvtKX9g09WUmWX9O','','','',NULL,'t5kLR2oOv82KCnnufeENgsnf3CnhBMoG310mQJ51auKFqeHtetd8NtyLsxT6','en',NULL,'2020-11-09 11:54:21','2020-11-11 06:03:07',NULL),
(24,'79ed51d3-8762-4f50-b642-36f476d9e2ff','Choukri ','choukri.elbarodi@gmail.com','$2y$10$E02gr57zSAr6G.RixW6x2uxUQXo/ooE1nL0//WGOqQ5rjMcBYzJdy','SQDQD','32234','234234',NULL,'0W6lbKQmZj2oGhcPANxTpDyi6um6ZOea205KxEXLA4hX9GNSG5PNLuDwIG9C','en',NULL,'2020-11-09 11:54:21','2020-11-14 16:18:44',NULL),
(25,'3e8ae672-c3c2-44df-982b-cbdb29aec9f9','Walid','contactenergiesolidarite@gmail.com','$2y$10$v49E5EhsH18C5py.pnracOT..RFc8VKUrb69L/Ac82IpqCVTCaVDa','','','',NULL,NULL,'en',NULL,'2020-11-09 11:59:46','2020-11-09 11:59:46',NULL),
(28,'c3ffa683-d896-465c-9696-aaf0adca57d9','Youssouf','youssouf.sup.iso@gmail.com','$2a$10$ThNOk6GkkNKIdYA9ewUc2eA9ZE55eBbJBJRKHWvtKX9g09WUmWX9O','','','',NULL,'bhggQbQUsDnW2LUR7TSLXeed0gQUPU1JXibZ3fQxEXlhJV5bdxbrEyN9V3US','en',NULL,'2020-11-09 12:41:36','2020-11-09 12:41:36',NULL),
(36,'caffc5bf-0b6f-48bd-93dd-53ca8c9ec886','MCH courtage','MCH.COURTAGE@GMAIL.COM','$2y$10$Bk1fNeLkhqnSOP4KanTAJu5653ntnM/qwzpGQOT4X6U4Y9wtpzVEG','','','',NULL,NULL,'en',NULL,'2020-11-11 07:08:26','2020-11-11 07:08:26',NULL),
(37,'89fa6c21-856f-46da-a0b0-626ec9a21d57','Ahmed ','julien.conseiller.isolation1e@gmail.com','$2y$10$IWv8UViG2LYNa2gDbYkn/.WiepWK2X/Xq6q0LayB0BqeOMWob.FDO','','','',NULL,NULL,'en',NULL,'2020-11-12 05:50:26','2020-11-14 16:11:31',NULL),
(38,'79f03aef-0ef4-4d49-ad05-e60902e68638','Oum Layya','conseillere.isolation.1euro.r@gmail.com','$2y$10$G1hpdoXiTImGvuccJe5Bi.F5UDAaJppmh5d4WfNfjwZXjQ4sYMo8y','','','',NULL,NULL,'en',NULL,'2020-11-12 05:51:54','2020-11-12 05:51:54',NULL),
(39,'dd503f0e-9c1a-4d47-ba93-3d55b90c772b','Haroun','rudymakongatombi82@gmail.com','$2y$10$c0.zzCEWAyOMXf7qPnuALelSR3xWL.3e6v2swFoWftpMJfRPVETtC','','','',NULL,'xj5oMUGyVMIxKnP6oMzKUswcesZ11a79GZgHDCxEJ7mO6VGMSGrFltCEoemN','en',NULL,'2020-11-12 05:53:45','2020-11-12 05:53:45',NULL),
(40,'b703bcb0-11f4-4b0b-814e-49ab857f5f3a','Salim','julienvalletmileguy@gmail.com','$2y$10$YHhgI4924sxO6h3QOibbYuuGsqkDCtGn.r7WKJJIjvpNm1YVhI552','','','',NULL,NULL,'en',NULL,'2020-11-12 05:54:56','2020-11-12 05:54:56',NULL),
(42,'82653ec7-784a-4169-828c-06e613c7fe9b','Said','sarisaro2020@gmail.com','$2y$10$YokW1jrpdjr2LZixxR09D.MSf0TwHEUGlJ5RtR6uA0AX703XSFFji','','','',NULL,NULL,'en',NULL,'2020-11-12 06:00:36','2020-11-12 06:00:36',NULL),
(45,'c8e1d707-5ff1-4610-85d7-875e5280a882','Yacine','danni.conseillerisolation1euro@gmail.com','$2y$10$JEy7MlggEL3XAO3HStJ1JuHpCxV/ZHnHCvMo5ulA8.Nc.dY6aN9bG','','','',NULL,'tFToyblIuPsED7uTjnUshJfmJzyppSntfum1OMyipKwS34WWGOYHuLshD2ec','en',NULL,'2020-11-13 03:43:31','2020-11-13 03:43:31',NULL),
(46,'85e416e0-9568-4ab0-abff-bb3b0881f699','Ayyoub','sonny.daverdin@gmail.com','$2y$10$KAeveS78uPMMOIWZLXx6qOhC/M188EM/Doz9Tg4SdGJaBiq8ZIRPa','','','',NULL,NULL,'en',NULL,'2020-11-13 03:44:47','2020-11-13 03:44:47',NULL),
(47,'1ad47d13-de7d-41bc-ae1d-2257140fb92a','Salah','salatanger8@gmail.com','$2y$10$wYNa2nrezTDIPSlfVacffuCepe2s25eQCkHRbHcfvpCbjYmKyX3im','','','',NULL,NULL,'en',NULL,'2020-11-13 04:17:16','2020-11-13 04:17:16',NULL),
(48,'52f2d131-621b-4c82-aa0f-140a59568f00','Salah abou Anas','salahkouzzi@gmail.com','$2y$10$ilBzFnt0D0oEIj4k8ZGME.jNS6nKQ1Itat6zrEeP9.WD6Bm89r.GS','','','',NULL,NULL,'en',NULL,'2020-11-13 04:42:35','2020-11-13 04:42:35',NULL),
(49,'2f0dca9b-325a-4205-8a24-c2b36fed4c9c','Badro','badroxjoubax@gmail.com','$2y$10$8P8zo1dYQ9Jy00qlVAvTnuqfzd0OCbKaZOOOA8Ef4TE6M2lph/VFG','','','',NULL,NULL,'en',NULL,'2020-11-13 04:47:31','2020-11-13 04:47:31',NULL),
(50,'8013c384-9b90-40fa-8e05-3ceb7952a241','Sulayman','sulayman3700@gmail.com','$2y$10$NKqFlf7f1MtsqD86A6HPR.hHaBLp.nKOCur0jlho5608uT6yuFWDG','','','',NULL,NULL,'en',NULL,'2020-11-13 05:23:50','2020-11-13 05:23:50',NULL),
(51,'93764756-1587-4ca6-8169-29a7140854b2','Oum jourry','ummjourry@gmail.com','$2y$10$joUopCE748yVsvNQ0HpIjeB8Gl7WAQjr/p1GZiEDs0A7JvjLYbvD2','','','',NULL,NULL,'en',NULL,'2020-11-13 08:15:17','2020-11-13 08:15:17',NULL),
(53,'a8819137-cf6c-47d0-9524-1a2ec118f9b2','Redouane','call.center.2690@gmail.com','$2y$10$/1jWm4NLaJKcheWj/P6XUO8k9.8Wjk.ikpzIM4UvEi4lOJWql2nTq','','','',NULL,NULL,'en',NULL,'2020-11-13 08:32:35','2020-11-13 08:32:35',NULL),
(54,'47b35c51-8e9b-4580-a17e-3e0bdaaff12d','Oum Ismail','anaisge63@gmail.com','$2y$10$OSmgUYPbtdvq19ZfFMp/8.GWu7NyfeULtsB3P7uRhf6DL5pxRjbDy','','','',NULL,NULL,'en',NULL,'2020-11-13 08:39:08','2020-11-13 08:39:08',NULL),
(55,'dd92e3db-8452-4a99-8110-d99cac52a329','testesting star lane stockton','test@tubenow.com','$2a$10$ThNOk6GkkNKIdYA9ewUc2eA9ZE55eBbJBJRKHWvtKX9g09WUmWX9O','10944 Flaming star lane stockton','1321','1',NULL,NULL,'en',NULL,'2020-11-14 21:35:32','2020-11-14 21:35:32',NULL),
(56,'e7c25dc6-525e-4991-bb22-4e393cd1e168','ssssss','aasisor@is-crm.fr','$2y$10$NeBPR3XCXLgJawYosZuU1.QkDx3acf/7h2Ayfm/OM9.0Ob1/41.Fu','10944 Flaming star lane stockton','1321','1',NULL,NULL,'en',NULL,'2020-11-14 23:08:48','2020-11-14 23:08:48',NULL),
(57,'3e2e014c-108f-4c0f-b075-99f9380bc625','cccccc','crodi@gmail.com','$2y$10$EbcfBENX.blpL72YW72WeOYyOrwMI5qJ9X462xKZ9kox0/2HHgpXO','10944 Flaming star lane stockton','1321','1',NULL,NULL,'en',NULL,'2020-11-14 23:09:51','2020-11-14 23:09:51',NULL),
(58,'f79b7152-c353-4eee-8fb8-a3d387cfafc8','role','rolwe@gmail.com','$2y$10$mNlV/wtrZsFlouApeDGP1uwwJhrZIVQ3rs7R4m9RBho5NV19bIS6O','role','1212','1212121',NULL,NULL,'en',NULL,'2020-11-14 23:11:04','2020-11-14 23:11:04','5'),
(59,'c05e3549-92ab-4c48-be98-eda20afde7e1','cccssss','ssss@tubenow.com','$2a$10$ThNOk6GkkNKIdYA9ewUc2eA9ZE55eBbJBJRKHWvtKX9g09WUmWX9O','10944 Flaming star lane stockton','1321','1',NULL,'fCBdFogWQvDLYgemXY7ih2q6qqFIHGQr6krYJkUfEDcJFKIvb4VGAwbiFrV0','en',NULL,'2020-11-14 23:11:57','2020-11-14 23:11:57','5'),
(60,'9c81de20-2cec-4ec9-9194-48376c5eb708','simo','simo@gmail.com','$2y$10$aItjpGiV5P0EmuadSM3D5.97nwb4ZHrukJkDwt7XkqWm9VdHPrVyG','kjkljkjj','987987','987897987',NULL,NULL,'en',NULL,'2020-11-15 03:12:28','2020-11-15 03:12:28','5');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
