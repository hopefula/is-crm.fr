<?php
namespace App\Http\Controllers;

use Auth;
use Session;
use Mews\Purifier;
use App\Http\Requests;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentDeleteController extends Controller
{
    /**
     * Create a comment for tasks and leads
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function delete(Request $request, $id){
        $comment_id = $id;
        $comment = Comment::find($id);
        if($comment)
            $comment->delete();        
        else{
            
        }
        // DB::delete('delete from comments where id = ?',[$comment_id]);
        return response()->json(['success'=>'it was delete successfuly.']);
    }
}
