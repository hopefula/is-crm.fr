<?php
namespace App\Http\Controllers;

use Ramsey\Uuid\Uuid;
use Session;
use App\Http\Requests;
use App\Models\Department;
use App\Models\DepartmentUser;
use App\Http\Requests\Department\StoreDepartmentRequest;
use Datatables;
use App\Models\Lead;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\RoleUser;

class DepartmentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('user.is.admin', ['only' => ['create', 'destroy']]);
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return view('departments.index')
            ->withDepartment(Department::all());
    }

    /**
     * @return mixed
     */
    public function indexData()
    {
        $departments = Department::select(['id', 'external_id', 'name', 'description']);
        return Datatables::of($departments)
            ->editColumn('name', function ($departments) {
                return $departments->name;
            })
            ->editColumn('description', function ($departments) {
                return $departments->description;
            })
            ->addColumn('supervisors', function ($departments) {
                $result = "";
                $user_id = RoleUser::where('role_id', 4)->pluck('user_id');
                $current_super = DepartmentUser::where('department_id', $departments->id)->whereIn('user_id', $user_id)->groupby('user_id')->get();
                if(count($current_super) > 0){
                    foreach($current_super as $temp){
                        $user = User::find($temp->user_id);
                        if($user)
                            $result .= $user->name.", ";
                    }
                }
                return $result;
            })
            ->addColumn('details', '
                <a href="{{ route(\'departments.details\', $external_id) }}">
            <input type="button" value="' . __('Details') . '" class="btn btn-link btn-info">
            </a>')
            ->addColumn('edit', '
                <a href="{{ route(\'departments.edit\', $external_id) }}">
            <input type="button" value="' . __('Edit') . '" class="btn btn-link btn-success">
            </a>')
            ->addColumn('delete', '
                <form action="{{ route(\'departments.destroy\', $external_id) }}" method="POST">
            <input type="hidden" name="_method" value="DELETE">
            {{csrf_field()}}
            <input type="submit" name="submit" value="' . __('Delete') . '" class="btn btn-danger" onClick="return confirm(\'Are you sure?\')"">
            </form>')
            ->rawColumns(['details','delete','edit','supervisors'])
            ->make(true);
    }

    /**
     * @return mixed
     */
    public function create()
    {
        $user_id = RoleUser::where('role_id', 4)->pluck('user_id');
        $users = User::whereIn('id', $user_id)->get();
        return view('departments.create')
            ->with('users', $users);
    }
    public function edit($external_id)
    {
        $user_id = RoleUser::where('role_id', 4)->pluck('user_id');
        $department = Department::whereExternalId($external_id)->first();
        $current_super = DepartmentUser::where('department_id', $department->id)->pluck('user_id')->toArray();
        // foreach($user_id as $temp_user) {
        //     $temp = DepartmentUser::where('department_id', $department->id)->where('user_id', $temp_user)->first();
        //     if($temp) {
        //         $current_super = $temp_user;
        //     }
        // }
        $users = User::whereIn('id', $user_id)->get();

        return view('departments.edit')
            ->with('department', $department)
            ->with('current_super', $current_super)
            ->with('users', $users);
    }
    /**
     * @param StoreDepartmentRequest $request
     * @return mixed
     */
    public function store(StoreDepartmentRequest $request)
    {
        $newDep = Department::create([
            'external_id' => Uuid::uuid4(),
            'name' => $request->name,
            'description' => $request->description
        ]);
        foreach($request->supervisor as $temp) {
            $new_dep_user = DepartmentUser::create([
                'department_id' => $newDep->id,
                'user_id' => $temp
            ]);
        }
        
        Session::flash('flash_message', __('Equipe créée avec succès'));
        return redirect()->route('departments.index');
    }
    public function update(Request $request)
    {
        $department = Department::whereExternalId($request->external_id)->first();
        $department->name = $request->name;
        $department->description = $request->description;
        $department->save();

        $old_deps = DepartmentUser::where('department_user.department_id', $department->id)->leftjoin('role_user', 'role_user.user_id', 'department_user.user_id')
                ->where('role_user.role_id', 4)
                ->get();
        foreach($old_deps as $temp_old) {
            $temp_old->delete();
        }
        foreach($request->Situation as $temp) {
            $new_dep_user = DepartmentUser::create([
                'department_id' => $department->id,
                'user_id' => $temp
            ]);
            $new_dep_user->user_id = $temp;
            $new_dep_user->save();
        }
  
        Session::flash('flash_message', __('Equipe mis à jour avec succès'));
        return redirect()->route('departments.index');
    }
    /**
     * @param $external_id
     * @return mixed
     */
    public function destroy($external_id)
    {
        $department = Department::whereExternalId($external_id)->first();
        if($department->id == 6) {
            Session::flash('flash_message_warning', __("Vous ne pouvez pas supprimer le département par default"));
            return back();
        }

        // if (!$department->users->isEmpty()) {
        //     Session::flash('flash_message_warning', __("Can't delete department with users, please remove users"));
        //     return redirect()->route('departments.index');
        // }
        $department_users = DepartmentUser::where('department_id', $department->id)->get();
        foreach($department_users as $temp) {
            $temp->department_id = 1;
            $temp->save();
        }

        $department->delete();
        Session::flash('flash_message', __("Equipe supprimer avec succès"));
        return redirect()->route('departments.index');
    }
    public function details($external_id) {
        $department = Department::whereExternalId($external_id)->first();
        $user_id = RoleUser::where('role_id', 4)->pluck('user_id');
        $current_super = DepartmentUser::where('department_id', $department->id)->whereIn('user_id', $user_id)->get();
        $temp = [];
        foreach($current_super as $temp_user) {
            $user = User::where('id', $temp_user->user_id)->first();
            if($user)
                $temp[] = $user->name;
        }
        
        if($department) {
            return view('departments.details')->with('department', $department)->with('users', $temp);
        }else{
            return back();
        }
    }
}
