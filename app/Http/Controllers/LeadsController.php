<?php
namespace App\Http\Controllers;

use App\Models\Invoice;
use DB;
use Auth;
use Carbon;
use Session;
use Datatables;
use App\Models\Lead;
use App\Models\LeadProduct;
use App\Models\DepartmentUser;
use App\Models\User;
use App\Models\Client;
use App\Http\Requests;
use App\Models\Status;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Requests\Lead\StoreLeadRequest;
use App\Http\Requests\Lead\UpdateLeadFollowUpRequest;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Validator;

class LeadsController extends Controller
{
    const CREATED = 'created';
    const UPDATED_STATUS = 'updated_status';
    const UPDATED_DEADLINE = 'updated_deadline';
    const UPDATED_ASSIGN = 'updated_assign';

    public function __construct()
    {
        $this->middleware('lead.create', ['only' => ['create']]);
        $this->middleware('lead.assigned', ['only' => ['updateAssign']]);
        $this->middleware('lead.update.status', ['only' => ['updateStatus']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function unqualified()
    {
        return view('leads.unqualified')->withStatuses(Status::typeOfLead()->get());
    }

    /**
     * Data for Data tables
     * @return mixed
     */
    public function unqualifiedLeads()
    {
        $status_id = Status::typeOfLead()->where('title', 'Closed')->first()->id;
        $leads = Lead::isNotQualified()
            ->where('status_id', '!=', $status_id)
            ->with(['user', 'creator', 'client.primaryContact'])->get();

        $leads->map(function ($item) {
            return [$item['visible_deadline_date'] = $item['deadline']->format(carbonDate()), $item["visible_deadline_time"] = $item['deadline']->format(carbonTime())];
        });
        return $leads->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $client_external_id = null)
    {
        $client =  Client::whereExternalId($client_external_id);
        $data = [
            'category_name' => 'dashboard',
            'page_name' => 'analytics',
            'has_scrollspy' => 0,
            'scrollspy_offset' => '',
            'alt_menu' => 0,
        ];

        if($request->product == 1)
        return view('leads.create')
            ->withClients(Client::pluck('company_name', 'external_id'))
            ->withClient($client ?: null)
            ->withStatuses(Status::typeOfLead()->pluck('title', 'id'));
        else if($request->product == 2)
        return view('leads.create_product2')
            ->withClients(Client::pluck('company_name', 'external_id'))
            ->withClient($client ?: null)
            ->withStatuses(Status::typeOfLead()->pluck('title', 'id'));
        else if($request->product == 3)
        return view('leads.create_product3')
            ->withClients(Client::pluck('company_name', 'external_id'))
            ->withClient($client ?: null)
            ->withStatuses(Status::typeOfLead()->pluck('title', 'id'));
        else if($request->product == 4)
        return view('leads.create_product4')
            ->withClients(Client::pluck('company_name', 'external_id'))
            ->withClient($client ?: null)
            ->withStatuses(Status::typeOfLead()->pluck('title', 'id'));        
        else if($request->product == 5)
        return view('leads.create_product5')
            ->withClients(Client::pluck('company_name', 'external_id'))
            ->withClient($client ?: null)
            ->withStatuses(Status::typeOfLead()->pluck('title', 'id'));        
        else    
        return view('leads.create')
            ->withClients(Client::pluck('company_name', 'external_id'))
            ->withClient($client ?: null)
            ->withStatuses(Status::typeOfLead()->pluck('title', 'id'));   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreLeadRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    
    public function new_create(Request $request)
    {
        // dd($request->all());
        if(!auth()->user()->can('lead-create')) {
            return back();
        }
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'Nom'         => 'required',
            'Ville'       => '',
            'Codepostal'  => '',
            'tel1'        => 'required',
            'mobile1'     => '',
            'description' => '',
            'lname'       => '',
            'address'     => '',
            'email'       => '',
            'status_id'   => '',
            'lead_type'   => ''
        ],[
            'Nom.required' => 'Le nom est obligatoire',
            'Ville.required' => 'La ville est obligatoire',
            'Codepostal.required' => 'Le Code Postal est obligatoire',
            'tel1.required' => 'Le Numéro de téléphone principale est obligatoire',
            'mobile1.required' => 'Your Mobile 1 is Required',
            'description.required' => 'Your Description is Required',
            'lname.required' => 'Your Last Name is Required',
            'address.required' => 'Your Address is Required',
            'email.required' => 'Your email is Required',
            'status_id.required' => 'Lead Status  is Required',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);

             Session::flash('error', $validator->messages()->first());
             return redirect()->back()->withInput();
        }
        
        if ($request->client_external_id) {
            $client = Client::whereExternalId($request->client_external_id);
        }
        
        if($request->lead_type == 1)
        
        $lead = Lead::create(
            [
                'Nom' => $request->Nom,
                'Ville' => $request->Ville,
                'user_created_id' => auth()->id(),
                'external_id' => Uuid::uuid4()->toString(),
                'Codepostal' => $request->Codepostal,
                'tel1' => $request->tel1,
                'tel2' => $request->tel2,
                'mobile1' => $request->mobile1,
                'mobile2' => $request->mobile2,
                'description' => clean($request->description),
                'lname' => $request->lname,
                'address' => $request->address,
                'email' => $request->email,
                'lead_type' => $request->lead_type,
                'status_id' => $request->status_id,
                'Demander' => $request->Demander,
                'Surface' => $request->Surface,
                'Maison' => $request->Maison,
                'Zone' => $request->Zone,
                'Quelle' => json_encode($request->Quelle),
                'Commentaire' => $request->Commentaire,
				'Campagne' => $request->Campagne,
                'Mode' => $request->Mode,
                'Situation' => $request->Situation,
                'Emploi' => $request->Emploi,
                'Quel' => $request->Quel,
                'Nombre' => $request->Nombre,
                'Fiscal1' => $request->Fiscal1,
                'RefAvis1' => $request->RefAvis1,
                'Fiscal2' => $request->Fiscal2,
                'RefAvis2' => $request->RefAvis2,
                'Revenue' => $request->Revenue,
				'produit1' => $request->produit1,
                'app_created_date' => $request->app_created_date,
                'app_confirm_date' => $request->app_confirm_date,
                'app_install_date' => $request->app_install_date
            ]
        );
        else if($request->lead_type == 2){
            $lead = Lead::create(
            [
                'Nom' => $request->Nom,
                'Ville' => $request->Ville,
                'user_created_id' => auth()->id(),
                'external_id' => Uuid::uuid4()->toString(),
                'Codepostal' => $request->Codepostal,
                'tel1' => $request->tel1,
                'lname' => $request->lname,
                'address' => $request->address,
                'tel2' => $request->tel2,
                'mobile1' => $request->mobile1,
                'mobile2' => $request->mobile2,
                'description' => clean($request->description),
                'email' => $request->email,
                'lead_type' => $request->lead_type,
                'status_id' => $request->status_id,
				'Campagne' => $request->Campagne,

                'Quelle' => $request->Quelle ? json_encode($request->Quelle) : '',
                'Surface' => $request->Surface,
                'Plafond' => $request->Plafond,
                'Surface_des' => $request->Surface_des,
                'comble' => $request->comble ? json_encode($request->comble) : '',
                'isolation' => $request->isolation ? json_encode($request->isolation) : '',
                'sous_sol' => $request->sous_sol ? json_encode($request->sous_sol) :'',
                'ancienne' => $request->ancienne ? json_encode($request->ancienne):'',
                'du_mur' => $request->du_mur,
                'Hauteur' => $request->Hauteur,
                'Hauteursous' => $request->Hauteursous,
                'endroit' => $request->endroit,
                'Commentaire' => $request->Commentaire,
                'Situation' => $request->Situation,
                'category' => $request->category,
                'Mode' => $request->Mode,
                'Personne' => $request->Personne,

                'Fiscal1' => $request->Fiscal1,
                'RefAvis1' => $request->RefAvis1,
                'Fiscal2' => $request->Fiscal2,
                'RefAvis2' => $request->RefAvis2,
                'Revenue' => $request->Revenue,
                'Nom_de' => $request->Nom_de,
                'Date_de' => $request->Date_de,
				'produit1' => $request->produit1,

                'app_created_date' => $request->app_created_date,
                'app_confirm_date' => $request->app_confirm_date,
                'app_install_date' => $request->app_install_date
            ]
        );
        }
        
        else if($request->lead_type == 3){
        $lead = Lead::create(
            [
                'Nom' => $request->Nom,
                'Ville' => $request->Ville,
                'user_created_id' => auth()->id(),
                'external_id' => Uuid::uuid4()->toString(),
                'Codepostal' => $request->Codepostal,
                'tel1' => $request->tel1,
                'lname' => $request->lname,
                'address' => $request->address,
                'tel2' => $request->tel2,
                'mobile1' => $request->mobile1,
                'mobile2' => $request->mobile2,
                'description' => clean($request->description),
                'email' => $request->email,
                'lead_type' => $request->lead_type,
                'status_id' => $request->status_id,
				'Campagne' => $request->Campagne,

                'actuelle' => $request->actuelle,
                'Situation' => $request->Situation,
                'Solutions' => $request->Solutions,
                'agricole' => $request->agricole,
                'niveau' => $request->niveau,
                'descendant' => $request->descendant,
                'Personne' => $request->Personne,

                'Fiscal1' => $request->Fiscal1,
                'RefAvis1' => $request->RefAvis1,
                'Fiscal2' => $request->Fiscal2,
                'RefAvis2' => $request->RefAvis2,
                'Revenue' => $request->Revenue,
                'Nom_de' => $request->Nom_de,
                'Date_de' => $request->Date_de,
				'produit1' => $request->produit1,

                'app_created_date' => $request->app_created_date,
                'app_confirm_date' => $request->app_confirm_date,
                'app_install_date' => $request->app_install_date
            ]
        );
    }
        else if($request->lead_type == 4)
        $lead = Lead::create(
            [
                'Nom' => $request->Nom,
                'Ville' => $request->Ville,
                'user_created_id' => auth()->id(),
                'external_id' => Uuid::uuid4()->toString(),
                'Codepostal' => $request->Codepostal,
                'tel1' => $request->tel1,
                'lname' => $request->lname,
                'address' => $request->address,
                'tel2' => $request->tel2,
                'mobile1' => $request->mobile1,
                'mobile2' => $request->mobile2,
                'description' => clean($request->description),
                'email' => $request->email,
                'lead_type' => $request->lead_type,
                'status_id' => $request->status_id,
				'Campagne' => $request->Campagne,
                'Metier' => $request->Metier,

                'Surface' => $request->Surface,
                'Zone' => $request->Zone,
                'Quelle' => json_encode($request->Quelle),
                'Commentaire' => $request->Commentaire,
                'Mode_de' => $request->Mode_de,
                'Situation' => $request->Situation,
                'Emploi' => $request->Emploi,
                'descendant' => $request->descendant,
                'Personne' => $request->Personne,

                'Fiscal1' => $request->Fiscal1,
                'RefAvis1' => $request->RefAvis1,
                'Fiscal2' => $request->Fiscal2,
                'RefAvis2' => $request->RefAvis2,
                'Revenue' => $request->Revenue,
                'Nom_de' =>$request->Nom_de,
				'produit1' => $request->produit1,

                'app_created_date' => $request->app_created_date,
                'app_confirm_date' => $request->app_confirm_date,
                'app_install_date' => $request->app_install_date
            ]
        );
        else if($request->lead_type == 5)
        $lead = Lead::create(
            [
                'Nom' => $request->Nom,
                'Ville' => $request->Ville,
                'user_created_id' => auth()->id(),
                'external_id' => Uuid::uuid4()->toString(),
                'Codepostal' => $request->Codepostal,
                'tel1' => $request->tel1,
                'lname' => $request->lname,
                'address' => $request->address,
                'tel2' => $request->tel2,
                'mobile1' => $request->mobile1,
                'mobile2' => $request->mobile2,
                'description' => clean($request->description),
                'email' => $request->email,
                'lead_type' => $request->lead_type,
                'status_id' => $request->status_id,
				'Campagne' => $request->Campagne,
                'Surface_Comble' => $request->Surface_Comble,
                'Configuration' => json_encode($request->Configuration),
                'Surface_des' => $request->Surface_des,
				'Sur_quoi' => json_encode($request->Sur_quoi),
                'Maison' => $request->Maison,
                'Zone' => $request->Zone,
                'Quelle' => json_encode($request->Quelle),
                'Mode_de' => $request->Mode_de,
                'Situation_de' => $request->Situation_de,
                'Emploi' => $request->Emploi,
                'Metier' => $request->Metier,
                'Commentaire' => $request->Commentaire,

                'quelques' => json_encode($request->quelques),
                'Nombre' => $request->Nombre,
                'Fiscal1' => $request->Fiscal1,
                'RefAvis1' => $request->RefAvis1,
                'Fiscal2' => $request->Fiscal2,
                'RefAvis2' => $request->RefAvis2,
                'Revenue' => $request->Revenue,
                'Nom_de' => $request->Nom_de,
                'Date_de' => $request->Date_de,
				'produit1' => $request->produit1,
                'app_created_date' => $request->app_created_date,
                'app_confirm_date' => $request->app_confirm_date,
                'app_install_date' => $request->app_install_date
            ]
        );
         
        $insertedExternalId = $lead->external_id;
         
        Session()->flash('flash_message', __('RDV créé avec succès'));
     
        return redirect()->route('new_web.leads.show_all', $insertedExternalId);
    }
    public function new_edit(Request $request, $id) {
        $lead = Lead::find($id);
        if($lead) {
            return view('leads.edit_product.edit_product'.$lead->lead_type)->with('lead_type', $lead->lead_type)->with('lead', $lead)
                ->withStatuses(Status::typeOfLead()->pluck('title', 'id'));
        }
        else
            return back();
    }
    public function new_update(Request $request, $id)
    {
        // dd($request->all());
        // if(!auth()->user()->can('lead-update')) {
        //     return back();
        // }
        
        $validator = Validator::make($request->all(), [
            'Nom'         => 'required',
            'Ville'       => '',
            'Codepostal'  => '',
            'tel1'        => 'required',
            'mobile1'     => '',
            'lname'       => '',
            'address'     => '',
            'email'       => '',
            'status_id'   => ''
        ],[
            'Nom.required' => 'Le nom est obligatoire',
            'Ville.required' => 'Your Ville is Required',
            'Codepostal.required' => 'Your Code Postal is Required',
            'tel1.required' => 'Le numéro de téléphone est obligatoire',
            'tel2.required' => 'Your Telephone 2 is Required',
            'mobile1.required' => 'Your Mobile 1 is Required',
            'mobile2.required' => 'Your Mobile 2 is Required',
            'lname.required' => 'Your Last Name is Required',
            'address.required' => 'Your Address is Required',
            'email.required' => 'Your email is Required',
            'status_id.required' => 'Lead Status  is Required',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);

             Session::flash('error', $validator->messages()->first());
             return redirect()->back()->withInput();
        }
        
        if ($request->client_external_id) {
            $client = Client::whereExternalId($request->client_external_id);
        }
        
        $lead = Lead::where('id', $id)->update(
            [
                'Nom' => $request->Nom,
                'Ville' => $request->Ville,
                'Codepostal' => $request->Codepostal,
                'tel1' => $request->tel1,
                'lname' => $request->lname,
                'address' => $request->address,
                'tel2' => $request->tel2,
                'mobile1' => $request->mobile1,
                'mobile2' => $request->mobile2,
                'description' => clean($request->description),
                'email' => $request->email,
                'lead_type' => $request->lead_type,
                'status_id' => $request->status_id,
				'Campagne' => $request->Campagne,

                'Surface_Comble' => $request->Surface_Comble ? $request->Surface_Comble:"",
				'Sur_quoi' => $request->Sur_quoi ? $request->Sur_quoi:"",
                'Configuration' => $request->Configuration ? json_encode($request->Configuration):"",
                'Surface_des' => $request->Surface_des ? $request->Surface_des:"",
                'Maison' => $request->Maison ? $request->Maison:"",
                'Zone' => $request->Zone ? $request->Zone:"",
				'Quelle' => $request->Quelle ? json_encode($request->Quelle):"",
                'Mode_de' => $request->Mode_de ? $request->Mode_de:"",
                'Situation_de' => $request->Situation_de ? $request->Situation_de:'',
                'Emploi' => $request->Emploi ? $request->Emploi:'',
                'Metier' => $request->Metier ? $request->Metier:'' ,
                'quelques' => $request->quelques? json_encode($request->quelques):'',
                'Nombre' => $request->Nombre ? $request->Nombre:"",

                'Fiscal1' => $request->Fiscal1 ? $request->Fiscal1:"",
                'RefAvis1' => $request->RefAvis1 ? $request->RefAvis1:"",
                'Fiscal2' => $request->Fiscal2 ? $request->Fiscal2: "",
                'RefAvis2' => $request->RefAvis2 ? $request->RefAvis2:"",
                'Revenue' => $request->Revenue ? $request->Revenue:"",
                'Nom_de' => $request->Nom_de ? $request->Nom_de:"",
                'Date_de' => $request->Date_de ? $request->Date_de:"",
				'produit1' => $request->produit1 ? $request->produit1:"",

                'app_created_date' => $request->app_created_date ? $request->app_created_date:"",
                'app_confirm_date' => $request->app_confirm_date ? $request->app_confirm_date:"",
                'app_install_date' => $request->app_install_date ? $request->app_install_date:""
            ]
        );
        // dd($lead);
        // $insertedExternalId = random();
        Session()->flash('flash_message', __('RDV mis à jour avec succès'));
        return redirect()->route('new_web.leads.show_all');
    }
    public function new_detail($id) {
        $lead = Lead::find($id);
        // dd(json_decode($lead->Quelle));
        if($lead) {
            return view('leads.detail_product.detail_product'.$lead->lead_type)->with('lead_type', $lead->lead_type)->with('lead', $lead)
                ->with('lead_Quelle', json_decode($lead->Quelle,false))
                ->with('lead_comble', json_decode($lead->comble,false))
                ->with('lead_isolation', json_decode($lead->isolation,false))
                ->with('lead_sous_sol', json_decode($lead->sous_sol,false))
                ->with('lead_ancienne', json_decode($lead->ancienne,false))
                ->withStatuses(Status::typeOfLead()->pluck('title', 'id'));
        }
        else
            return back();
    }
    public function new_delete($id) {
        DB::delete('delete from leads where id = ?',[$id]);
        Session()->flash('flash_message', __('RDV supprimer avec succès'));
        return back();
    }
    public function store(StoreLeadRequest $request)
    {
        if ($request->client_external_id) {
            $client = Client::whereExternalId($request->client_external_id);
        }

        $lead = Lead::create(
            [
                'title' => $request->title,
                'description' => clean($request->description),
                'user_assigned_id' => $request->user_assigned_id,
                'status_id' => $request->status_id,
                'client_id' => $client->id,
                'deadline' => Carbon::parse($request->deadline . " " . $request->contact_time . ":00"),
                'user_created_id' => auth()->id(),
                'external_id' => Uuid::uuid4()->toString(),
                'fname' => $request->fname,
                'lname' => $request->lname,
                'postal' => $request->postal,
                'tel1' => $request->tel1,
                'tel2' => $request->tel2,
                'mobile1' => $request->mobile1,
                'mobile2' => $request->mobile2,
                'email' => $request->email
            ]
        );
        
        $insertedExternalId = $lead->external_id;
        Session()->flash('flash_message', __('RDV créé avec succès'));

        event(new \App\Events\LeadAction($lead, self::CREATED));
        Session()->flash('flash_message', __('RDV créé avec succès'));
        return redirect()->route('leads.show', $insertedExternalId);
    }

    public function updateAssign($external_id, Request $request)
    {
        $lead = $this->findByExternalId($external_id);
        $input = $request->get('user_assigned_id');
        $input = array_replace($request->all());
        $lead->fill($input)->save();
        $insertedName = $lead->user->name;

        event(new \App\Events\LeadAction($lead, self::UPDATED_ASSIGN));
        Session()->flash('flash_message', __('Nouveau utilisateur est affecté'));
        return redirect()->back();
    }

    /**
     * Update the follow up date (Deadline)
     * @param UpdateLeadFollowUpRequest $request
     * @param $external_id
     * @return mixed
     */
    public function updateFollowup(UpdateLeadFollowUpRequest $request, $external_id)
    {
        if (!auth()->user()->can('lead-update-deadline')) {
            session()->flash('flash_message_warning', __('You do not have permission to change task deadline'));
            return redirect()->route('tasks.show', $external_id);
        }
        $lead = $this->findByExternalId($external_id);
        $lead->fill(['deadline' => Carbon::parse($request->deadline . " " . $request->contact_time . ":00")])->save();
        event(new \App\Events\LeadAction($lead, self::UPDATED_DEADLINE));
        Session()->flash('flash_message', __('New follow up date is set'));
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $external_id
     * @return \Illuminate\Http\Response
     */
    public function show($external_id)
    {
        return view('leads.show')
            ->withLead($this->findByExternalId($external_id))
            ->withUsers(User::with(['department'])->get()->pluck('nameAndDepartmentEagerLoading', 'id'))
            ->withCompanyname(Setting::first()->company)
            ->withStatuses(Status::typeOfLead()->pluck('title', 'id'));
    }
    public function show_all()
    {
        if(Auth()->user()->userRole->role_id == 1 || Auth()->user()->userRole->role_id == 2){
            $product  = Lead::all();
        }else if(Auth()->user()->userRole->role_id == 4) {
            $department = DepartmentUser::where('user_id', auth()->user()->id)->pluck('department_id')->toArray();
            $user_ids = DepartmentUser::whereIn('department_id', $department)->pluck('user_id');
            $lead_statuses = Status::where('source_type', 'App\Models\Lead')->get()->toArray();
            $lead_ids = Lead::whereIn('user_created_id', $user_ids)->get()->toArray();

            if($department) {
                // $departmentId = $department->department_id;
                // $user_list = DepartmentUser::where('department_id', $departmentId)->pluck('user_id')->toArray();
                $product  = Lead::whereIn('user_created_id', $user_ids)->get();
            }else
                $product = new Lead();
        }
        else {
            $product  = Lead::where('user_created_id', auth()->user()->id)->get();
        }
        
        return view('leads.show_all')
            ->with('product', $product);
    }
    /**
     * Complete lead
     * @param $external_id
     * @param Request $request
     * @return mixed
     */
    public function updateStatus($external_id, Request $request)
    {
        if (!auth()->user()->can('lead-update-status')) {
            session()->flash('flash_message_warning', __('Vous n\'avez pas le droit de changer le status'));
            return redirect()->route('tasks.show', $external_id);
        }
        $lead = $this->findByExternalId($external_id);
        if (isset($request->closeLead) && $request->closeLead === true) {
            $lead->status_id = Status::typeOfLead()->where('title', 'Closed')->first()->id;
            $lead->save();
        } else {
            $lead->fill($request->all())->save();
        }
        event(new \App\Events\LeadAction($lead, self::UPDATED_STATUS));
        Session()->flash('flash_message', __('Lead status updated'));
        return redirect()->back();
    }

    public function convertToQualifiedLead(Lead $lead)
    {
        Session()->flash('flash_message', __('Lead status updated'));
        return $lead->convertToQualified();
    }


    public function convertToOrder(Lead $lead)
    {
        $invoice = $lead->canConvertToOrder();
        return $invoice->external_id;
    }
    /**
     * @param $external_id
     * @return mixed
     */
    public function findByExternalId($external_id)
    {
        return Lead::whereExternalId($external_id)->first();
    }
}
