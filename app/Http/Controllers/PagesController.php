<?php
namespace App\Http\Controllers;

use App\Models\Absence;
use App\Models\Client;
use App\Models\Lead;
use App\Models\Project;
use App\Models\Setting;
use App\Models\Task;
use App\Models\User;
use App\Models\Status;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use App\Models\RoleUser;
use DB;
use App\Models\DepartmentUser;
use Illuminate\Support\Facades\Route;

class PagesController extends Controller
{
    /**
     * Dashobard view
     * @return mixed
     */
    public function dashboard()
    {
        $today = today();
        $startDate = today()->subdays(14);
        $period = CarbonPeriod::create($startDate, $today);
        $datasheet = [];

        // Iterate over the period
        foreach ($period as $date) {
            $datasheet[$date->format(carbonDate())] = [];
            $datasheet[$date->format(carbonDate())]["monthly"] = [];
            $datasheet[$date->format(carbonDate())]["monthly"]["tasks"] = 0;
            $datasheet[$date->format(carbonDate())]["monthly"]["leads"] = 0;
        }

        $tasks = Task::whereBetween('created_at', [$startDate, now()])->get();
        $leads = Lead::whereBetween('created_at', [$startDate, now()])->get();
        foreach ($tasks as $task) {
            $datasheet[$task->created_at->format(carbonDate())]["monthly"]["tasks"]++;
        }

        foreach ($leads as $lead) {
            $datasheet[$lead->created_at->format(carbonDate())]["monthly"]["leads"]++;
        }
        if (!auth()->user()->can('absence-view')) {
            $absences = [];
        } else {
            $absences = Absence::with('user')->groupBy('user_id')->where('start_at', '>=', today())->orWhere('end_at', '>', today())->get();
        }
        $lead_static = [];
        if(Auth()->user()->userRole->role_id == 1 || Auth()->user()->userRole->role_id == 2){
            $lead_status = Status::where('source_type', 'App\Models\Lead')->get()->toArray();
            foreach($lead_status as $temp) {
                    $temp['count'] = Lead::where('status_id', $temp['id'])->count();
                    $lead_static[] = $temp;
            }
        }else if(Auth()->user()->userRole->role_id == 4) {
            $department = DepartmentUser::where('user_id', auth()->user()->id)->pluck('department_id')->toArray();
            $user_ids = DepartmentUser::whereIn('department_id', $department)->pluck('user_id');
            $lead_statuses = Status::where('source_type', 'App\Models\Lead')->get()->toArray();
            $lead_ids = Lead::whereIn('user_created_id', $user_ids)->get()->toArray();
            
            foreach($lead_statuses as $lead_status){
                $lead = Status::find($lead_status['id']);
                foreach($user_ids as $user_id){
                    $count = 0;
                    if(count($lead_ids) > 0) {
                        foreach($lead_ids as $lead_id){
                            if($lead_id['status_id'] == $lead->id){
                                $count++;
                            }
                        }
                    }
                    $temp_lead['count'] = $count;
                    $temp_lead['color'] = $lead['color'];
                    $temp_lead['title'] = $lead['title'];
                }
                $lead_static[] = $temp_lead;
            }
        }
        else{
            $lead_status = Status::where('source_type', 'App\Models\Lead')->get()->toArray();
            $lead_list = Lead::where('user_created_id', auth()->user()->id)->get()->toArray();
            foreach($lead_status as $temp) {
                $temp_count=0;
                foreach($lead_list as $user){
                    if($temp['id'] == $user['status_id']) {
                        $temp_count++;
                    }
                }
                $temp_lead['count'] = $temp_count;
                $temp_lead['color'] = $temp['color'];
                $temp_lead['title'] = $temp['title'];
                $lead_static[] = $temp_lead;
            }
        }
        $data = [
            'lead_static' => $lead_static,
            'category_name' => 'dashboard',
            'page_name' => 'analytics',
            'has_scrollspy' => 0,
            'scrollspy_offset' => '',
            'alt_menu' => 0,
        ];
        return view('pages.dashboard')
            ->with($data)
            ->withUsers(User::with(['department'])->get())
            ->withDatasheet($datasheet)
            ->withTotalTasks(Task::count())
            ->withTotalLeads(Lead::count())
            ->withTotalProjects(Project::count())
            ->withTotalClients(Client::count())
            ->withSettings(Setting::first())
            ->withAbsencesToday($absences);
    }
}
