<?php
namespace App\Http\Controllers;

use Gate;
use Datatables;
use Illuminate\Support\Facades\Storage;
use Session;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Task;
use App\Models\Client;
use App\Models\Setting;
use App\Models\Status;
use App\Models\Lead;
use App\Models\Role;
use App\Models\Department;
use App\Models\DepartmentUser;
use Illuminate\Http\Request;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Requests\User\StoreUserRequest;
use Ramsey\Uuid\Uuid;
use App\Models\RoleUser;

class UsersController extends Controller
{
    protected $users;
    protected $roles;

    public function __construct()
    {
        $this->middleware('user.create', ['only' => ['create']]);
        $this->data = [
            'category_name' => 'dashboard',
            'page_name' => 'analytics',
            'has_scrollspy' => 0,
            'scrollspy_offset' => '',
            'alt_menu' => 0,
        ];
    }

    /**
     * @return mixed
     */
    public function index()
    {
        if(Auth()->user()->userRole->role_id == 1 || Auth()->user()->userRole->role_id == 2){
            $user  = User::all();
        }
        else if(Auth()->user()->userRole->role_id == 5) {
            $department = DepartmentUser::where('user_id', auth()->user()->id)->first();
            if($department) {
                $departmentId = $department->department_id;
                $user_list = DepartmentUser::where('department_id', $departmentId)->pluck('user_id')->toArray();
                $user  = User::whereIn('id', $user_list)->get();
            }else
                $user = new User();
        }
        else {
            $user  = User::where('id', auth()->user()->id)->get();
        }
        return view('users.index')->with('users',$user);
    }

    public function calendarUsers()
    {
        return User::with(['department', 'absences' =>  function ($q) {
            return $q->whereBetween('start_at', [today()->subWeeks(2)->startOfDay(), today()->addWeeks(4)->endOfDay()])
                      ->orWhereBetween('end_at', [today()->subWeeks(2)->startOfDay(), today()->addWeeks(4)->endOfDay()]);
        }
        ])->get();
    }

    public function users($id)
    {

        return User::with(['department'])->get();
    }

    public function anyData()
    {
        $canUpdateUser = auth()->user()->can('update-user');
        // $users = User::select(['id', 'external_id', 'name', 'email', 'primary_number']);
        if(Auth()->user()->userRole->role_id == 1 || Auth()->user()->userRole->role_id == 2){
            $users  = User::all();
        }
        else if(Auth()->user()->userRole->role_id == 4) {
            $department = DepartmentUser::where('user_id', auth()->user()->id)->pluck('department_id');
            if($department) {
                $user_list = DepartmentUser::whereIn('department_id', $department)->pluck('user_id')->toArray();
                $users  = User::whereIn('id', $user_list)->get();
            }else
                $users = new User();
        }
        else {
            $users  = User::where('id', auth()->user()->id)->get();
        }
        
        return Datatables::of($users)
            ->addColumn('teamname', function ($user) {
                $departmentUser = DepartmentUser::where('user_id', $user->id)->first();
                if($departmentUser) {
                    $department = Department::find($departmentUser->department_id);
                    $teamname = $department->name;
                }else
                    $teamname = "Not Found";

                return '<p>' . $teamname .'</p>';
            })
            ->addColumn('user_type', function ($user) {
                $role_name = "";
                $user_data = RoleUser::where('user_id',$user->id)->first();
                if($user_data) {
                    $role = Role::find($user_data->role_id);
                    if($role)
                        $role_name = $role->display_name;
                }
                // if($role_name == "owner"){
                //     $role_name = "administrator";
                // }
                return $role_name;
            })
            ->addColumn('namelink', function ($users) {
                return '<a href="/users/' . $users->external_id . '" ">' . $users->name . '</a>';
            })
            ->addColumn('view', function ($user) {
                return '<a href="' . route("users.show", $user->external_id) . '" class="btn btn-success btn-link">' . __('View') .'</a>';
            })
            ->addColumn('edit', function ($user) {
                return '<a href="' . route("users.edit", $user->external_id) . '" class="btn btn-info btn-link">' . __('Edit') .'</a>';
            })
            ->addColumn('delete', function ($user) {
                return '<a href="' . route("users.new_delete", $user->external_id) . '" class="btn btn-danger btn-link">' . __('Delete') .'</a>';
            })
            ->rawColumns(['teamname', 'namelink','view', 'edit', 'delete','user_type'])
            ->make(true);
    }

    /**
     * Json for Data tables
     * @param $id
     * @return mixed
     */
    public function taskData($id)
    {
        $tasks = Task::with(['status', 'client'])->select(
            ['id', 'external_id', 'title', 'created_at', 'deadline', 'user_assigned_id', 'client_id', 'status_id']
        )
            ->where('user_assigned_id', $id)->get();
        return Datatables::of($tasks)
            ->addColumn('titlelink', function ($tasks) {
                return '<a href="' . route('tasks.show', $tasks->external_id) . '">' . $tasks->title . '</a>';
            })
            ->addColumn('user_type', function ($leads) {
                $role_name = "";
                $user_data = RoleUser::where($lead->user_created_id)->first();
                if($user_data) {
                    $role = Role::find($user_data->role_id);
                    if($role)
                        $role_name = $role->name;
                }
                return $role_name;
            })
            ->editColumn('created_at', function ($tasks) {
                return $tasks->created_at ? with(new Carbon($tasks->created_at))
                    ->format(carbonDate()) : '';
            })
            ->editColumn('deadline', function ($tasks) {
                return $tasks->deadline ? with(new Carbon($tasks->deadline))
                    ->format(carbonDate()) : '';
            })
            ->editColumn('status_id', function ($tasks) {
                return '<span class="label label-success" style="background-color:' . $tasks->status->color . '"> ' .$tasks->status->title . '</span>';
            })
            ->editColumn('client_id', function ($tasks) { $tasks->created_at ? with(new Carbon($tasks->created_at))
                    ->format(carbonDate()) : '';
            })
            ->editColumn('deadline', function ($tasks) {
                return $tasks->deadline ? with(new Carbon($tasks->deadline))
                    ->format(carbonDate()) : '';
            })
            ->editColumn('status_id', function ($tasks) {
                return '<span class="label label-success" style="background-color:' . $tasks->status->color . '"> ' .$tasks->status->title . '</span>';
            })
            ->editColumn('client_id', function ($tasks) {
                return $tasks->client->primaryContact->name;
            })
            ->rawColumns(['titlelink','status_id','user_type'])
            ->make(true);
    }

    /**
     * Json for Data tables
     * @param $id
     * @return mixed
     */
    public function leadData($id)
    {
        $leads = Lead::with(['status', 'client'])->select(
            ['id', 'external_id', 'title', 'created_at', 'deadline', 'user_assigned_id', 'client_id', 'status_id']
        )
            ->where('user_assigned_id', $id)->get();
        return Datatables::of($leads)
            ->addColumn('titlelink', function ($leads) {
                return '<a href="' . route('leads.show', $leads->external_id) . '">' . $leads->title . '</a>';
            })
            ->addColumn('user_type', function ($leads) {
                $role_name = "";
                $user_data = RoleUser::where($lead->user_created_id)->first();
                if($user_data) {
                    $role = Role::find($user_data->role_id);
                    if($role)
                        $role_name = $role->name;
                }
                return $role_name;
            })
            ->editColumn('created_at', function ($leads) {
                return $leads->created_at ? with(new Carbon($leads->created_at))
                    ->format(carbonDate()) : '';
            })
            ->editColumn('deadline', function ($leads) {
                return $leads->deadline ? with(new Carbon($leads->deadline))
                    ->format(carbonDate()) : '';
            })
            ->editColumn('status_id', function ($leads) {
                return '<span class="label label-success" style="background-color:' . $leads->status->color . '"> ' .
                    $leads->status->title . '</span>';
            })
            ->editColumn('client_id', function ($tasks) {
                return $tasks->client->primaryContact->name;
            })
            ->rawColumns(['titlelink','status_id','user_type'])
            ->make(true);
    }

    /**
     * Json for Data tables
     * @param $id
     * @return mixed
     */
    public function clientData($id)
    {
        $clients = Client::select(['external_id', 'company_name', 'vat', 'address'])->where('user_id', $id);
        return Datatables::of($clients)
            ->addColumn('clientlink', function ($clients) {
                return '<a href="' . route('clients.show', $clients->external_id) . '">' . $clients->company_name . '</a>';
            })
            ->editColumn('created_at', function ($clients) {
                return $clients->created_at ? with(new Carbon($clients->created_at))
                    ->format(carbonDate()) : '';
            })
            ->rawColumns(['clientlink'])
            ->make(true);
    }


    /**
     * @return mixed
     */
    public function create()
    {
        $admin_count = RoleUser::whereIn('role_id', [1, 2])->count();

        return view('users.create')
            ->with('option', 'create')
            ->with('admin_count', $admin_count)
            ->withRoles($this->allRoles()->pluck('display_name', 'id'))
            ->withDepartments(Department::pluck('name', 'id'));
    }

    /**
     * @param StoreUserRequest $userRequest
     * @return mixed
     */
    public function store(StoreUserRequest $request)
    {   
        // $settings = Setting::first();
        // if (User::count() >= $settings->max_users) {
        //     Session::flash('flash_message_warning', __('Max number of users reached'));
        //     return redirect()->back();
        // }
        $path = null;
        if ($request->hasFile('image_path')) {
            $file =  $request->file('image_path');
            $filename = str_random(8) . '_' . $file->getClientOriginalName() ;
            $path = Storage::put($settings->external_id, $file);
        }
        
        $user = new User();
        $user->name = $request->name;
        $user->external_id = Uuid::uuid4()->toString();
        $user->email = $request->email;
        $user->address = $request->address;
        $user->primary_number = $request->primary_number;
        $user->secondary_number = $request->secondary_number;
        $user->password = bcrypt($request->password);
        $user->image_path = $path;
        $user->language = $request->language == "dk" ?: "en";
        $user->option = $request->roles;
        $user->save();
        $role_id = $user->roles()->attach($request->roles);
        $user->department()->attach($request->departments);
        $user->save();
        Session::flash('flash_message', __('Utilisateur créé avec succès'));
        return redirect()->route('users.index');
    }

    /**
     * @param $external_id
     * @return mixed
     */
    public function show($external_id)
    {
        /** @var User $user */
        $user = $this->findByExternalId($external_id);
        return view('users.show')
            ->with($this->data)
            ->withUser($user)
            ->withCompanyname(Setting::first()->company);
    }


    /**
     * @param $external_id
     * @return mixed
     */
    public function edit($external_id)
    {
        $user = User::where('external_id', $external_id)->get();
        foreach($user as $temp){
            $tempp = $temp->id;
        }
        $admin_count = RoleUser::where('role_id')->count();
        $user_id = RoleUser::where('user_id', $tempp)->get();
        foreach($user_id as $tem){
            $vir = $tem->role_id;
        }
        
        return view('users.edit')
            ->with('vir', $vir)
            ->with('option', 'edit')
            ->with('admin_count', $admin_count)
            ->withUser($this->findByExternalId($external_id))
            ->withRoles($this->allRoles()->pluck('display_name', 'id'))
            ->withDepartments(Department::pluck('name', 'id'));
    }

    /**
     * @param $external_id
     * @param UpdateUserRequest $request
     * @return mixed
     */
    public function update($external_id, Request $request)
    {
            $user = $this->findByExternalId($external_id);
            $password = bcrypt($request->password);
            $role = $request->roles;
            $department = $request->departments; 
            if ($request->hasFile('image_path')) {
                $companyname = Setting::first()->external_id;
                $file =  $request->file('image_path');
                
                $filename = str_random(8) . '_' . $file->getClientOriginalName() ;

                $path = Storage::put($companyname, $file);
                if ($request->password == "") {
                    
                    $input =  array_replace($request->except('password'), ['image_path'=>"$path"]);
                  
                } else {
                   
                    $input =  array_replace($request->all(), ['image_path'=>"$path", 'password'=>"$password"]);
                    
                }
            } else {
                if ($request->password == "") {
                    $input =  array_replace($request->except('password'));
                } else {
                    $input =  array_replace($request->all(), ['password'=>"$password"]);
                }
            }
            
            $owners = User::whereHas('roles', function ($q) {
                $q->where('name', Role::OWNER_ROLE);
            })->get();
            $user->fill($input)->save();
            $role = $user->roles->first();
            if ($role && $role->name == Role::OWNER_ROLE && $owners->count() <= 1) {
                Session()->flash('flash_message_warning', __('Not able to change owner role, please choose a new owner first'));
            } else {
                $user->roles()->sync([$request->roles]);
            }
            
            $user->department()->sync([$department]);
            Session()->flash('flash_message', __('Utilisateur mis à jour avec succès'));
            return redirect()->back();
    }

    /**
     * @param $external_id
     * @return mixed
     */
    public function new_delete(Request $request, $external_id)
    {
        $user = $this->findByExternalId($external_id);

        if ($user->hasRole('owner')) {
            return Session()->flash('flash_message_warning', __('Vous n\'avez pas le droit de supprimer l\'administrateur'));
        }

        // if ($request->tasks == "move_all_tasks" && $request->task_user != "") {
        //     $user->moveTasks($request->task_user);
        // }
        // if ($request->leads == "move_all_leads" && $request->lead_user != "") {
        //     $user->moveLeads($request->lead_user);
        // }
        // if ($request->clients == "move_all_clients" && $request->client_user != "") {
        //     $user->moveClients($request->client_user);
        // }

        try {
            $lead_list = Lead::where('user_created_id', $user->id)->get();
            $admin_count = RoleUser::where('role_id', 1)->first();
            if(!$admin_count){
                $admin_count = RoleUser::where('role_id', 2)->first();
            }
            $admin_id = $admin_count->user_id;
            $leads = Lead::where('user_created_id', $user->id)->get();
            foreach($leads as $lead) {
                $lead->user_created_id = $admin_id;
                $lead->save();
            }

            $user->delete();
            Session()->flash('flash_message', __('Utilisateur supprimer avec succès'));
        } catch (\Illuminate\Database\QueryException $e) {
            Session()->flash('flash_message_warning', __('User can NOT have, leads, clients, or tasks assigned when deleted'));
        }

        return redirect()->route('users.index');
    }

    /**
    * @param $external_id
    * @return mixed
    */
    public function findByExternalId($external_id)
    {
        return User::whereExternalId($external_id)->first();
    }
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function allRoles()
    {
        if (auth()->user()->roles->first()->name == Role::OWNER_ROLE) {
            // return Role::all('display_name', 'id', 'name', 'external_id')->sortBy('display_name');
            return Role::where('name', '!=', 'owner')->get();
        }

        return Role::all('display_name', 'id', 'name', 'external_id')->filter(function ($value, $key) {
            return $value->name != "owner";
        })->sortBy('display_name');
    }
}
