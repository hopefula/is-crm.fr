<?php

namespace App\Http\Requests\Lead;

use Illuminate\Foundation\Http\FormRequest;

class StoreLeadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('lead-create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'title'         => 'required',
            // 'description'   => 'required',
            // 'status_id'         => 'required',
            // 'client_id'         => 'required',
            // 'deadline'          => 'required',
            // 'external_id'       => 'required',
            // 'fname'             => 'required',
            // 'lname'             => 'required',
            // 'postal'            => 'required',
            // 'tel1'              => 'required',
            // 'tel2'              => 'required',
            // 'mobile1'           => 'required',
            // 'mobile2'           => 'required',
            // 'email'             => 'required'
            // 'title' => 'required',
            // 'description' => 'required'
            // 'status_id' => 'required',
            // 'user_assigned_id' => 'required',
            // 'user_created_id' => '',
            // 'client_external_id' => 'required',
            // 'deadline' => 'required'
        ];
    }
}
