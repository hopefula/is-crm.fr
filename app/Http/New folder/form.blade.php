<div class="row">
    <br>
    <div class="col-md-6">
        <div class="col-sm-3">
            <label for="name" class="base-input-label">@lang('Nom')</label>
        </div>
        <div class="col-sm-9">
                <div class="form-group col-sm-8">
                    <input type="text" name="name" class="form-control" value="{{isset($user) ? optional($user)->name : ''}}">
                </div>
        </div>
        <div class="col-sm-12">
            <hr>
        </div>
        <div class="col-sm-3">
            <label for="image_path" class="base-input-label">@lang('Image')</label>
        </div>
        <div class="col-sm-9">
            <div class="form-group form-inline col-sm-8">
                <div class="input-group ">
                    <img src="{{isset($user) ? optional($user)->avatar : '/images/default_avatar.jpg'}}" style="max-height: 40px; border-radius: 25px;">
                </div>
                <div class="input-group" style="margin-left: 0.7em;">
                    <input type="file" name="image_path">
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <hr>
        </div>
        <div class="col-sm-3">
            <label for="name" class="base-input-label">@lang('Adresse')</label>
        </div>
        <div class="col-sm-9">
            <div class="form-group col-sm-8">
                <input type="text" name="address" class="form-control" value="{{isset($user) ? optional($user)->address : ''}}">
            </div>
        </div>
        <div class="col-sm-12">
            <hr>
        </div>
        <div class="col-sm-3">
            <label for="name" class="base-input-label">@lang('Informations')</label>
        </div>
        <div class="col-sm-9">
            <div class="form-group col-sm-8">
                <label for="email" class="control-label thin-weight">@lang('Email')</label>
                <input type="email" name="email" class="form-control" value="{{isset($user) ? optional($user)->email : ''}}">
            </div>
            <div class="form-group col-sm-8">
                <label for="primary_number" class="control-label thin-weight">@lang('Téléphone principale')</label>
                <input type="number" name="primary_number" class="form-control" value="{{isset($user) ? optional($user)->primary_number : ''}}">
            </div>
            <div class="form-group col-sm-8">
                <label for="secondary_number" class="control-label thin-weight">@lang('Téléphone secondaire')</label>
                <input type="number" name="secondary_number" class="form-control" value="{{isset($user) ? optional($user)->secondary_number : ''}}">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-sm-3">
            <label for="name" class="base-input-label">@lang('Sécurité')</label>
        </div>
        <div class="col-sm-9">
            <div class="form-group col-sm-8">
                <label for="password" class="control-label thin-weight">@lang('Mot de passe')</label>
                <input type="password" name="password" class="form-control" value="">
            </div>
            <div class="form-group col-sm-8">
                <label for="password_confirmation" class="control-label thin-weight">@lang('Confirmer le mot de passe')</label>
                <input type="password" name="password_confirmation" class="form-control" value="">
            </div>
        </div>
        <div class="col-sm-12">
            <hr>
        </div>
        <div class="col-sm-3">
            <label for="name" class="base-input-label">@lang('Management')</label>
        </div>
        <div class="col-sm-9">
            <div class="form-group col-sm-8">
                <label for="roles" class="control-label thin-weight">@lang('Poste')</label>
                <select name="roles" id="role_id" class="form-control" onchange="ddd()">
                @foreach($roles as $key => $role)
                    @if($role != "Owner")
                        <option {{ isset($user) && optional($user->userRole)->role_id === $key ? "selected" : "" }} value="{{$key}}">{{$role}}</option>
                    @endif
                @endforeach
                </select>
            </div>
            <div class="form-group col-sm-8">
                <label for="departments" class="control-label thin-weight">@lang('Équipe')</label>
                <select id ="team1" name="departments" class="form-control" style="display:block;">
                    @foreach($departments as $key => $department)
                        <option {{ isset($user) && $user->department->first()->id === $key ? "selected" : "" }} value="{{$key}}">{{$department}}</option>
                    @endforeach
                </select>
                <div style="display:none;" id ="team2">
                <select  name="departments[]" class="form-control tagging form-small" multiple>
                    @foreach($departments as $key => $department)
                        <option {{ isset($user) && $user->department->first()->id === $key ? "selected" : "" }} value="{{$key}}">{{$department}}</option>
                    @endforeach
                </select>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <hr>
        </div>
        <!-- <div class="col-sm-3">
            <label for="name" class="base-input-label">@lang('Settings')</label>
        </div> -->
        <input value="en" class="d-none" type="radio" name="language" checked>
        <!-- <div class="col-sm-9">
            <div class="form-group col-sm-8">
                <label for="language" class="control-label thin-weight">@lang('Language')</label> <br>
                <label class="radio-inline">
                </label>
                <label class="radio-inline">
                    <input value="en" type="radio" name="language" {{isset($user) && strtolower($user->language) == "en" ? 'checked': ''}}>@lang('English')
                </label>
                <label class="radio-inline">
                    <input value="es" type="radio" name="language" {{isset($user) && strtolower($user->language) == "es" ? 'checked': ''}}>@lang('Spanish')
                </label>
            </div>
        </div> -->
    </div>
</div>
<div class="col-sm-12">
    <hr>
</div>
<div class="col-lg-12">
    <input type="submit" value="{{$submitButtonText}}" class="btn btn-outline-primary mb-2">
</div>
