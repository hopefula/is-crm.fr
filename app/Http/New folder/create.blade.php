@extends('layouts.master')
@section('heading')
    {{ __('Create user') }}
@stop

@section('content')
<div class="col-lg-12 layout-spacing">
    <div class="statbox widget box box-shadow">
        <div class="widget-header">                                
            <div class="row">
                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>Création d'un nouvel utilisateur</h4><br>
                    <a class="btn btn-info" href="{{route('users.index')}}">Retour</a><br>
                </div>                  
            </div>
        </div>
        <div class="widget-content widget-content-area">
            

        {!! Form::open([
                'route' => 'users.store',
                'files'=>true,
                'enctype' => 'multipart/form-data'

                ]) !!}
        @include('users.form', ['submitButtonText' => __('Valider')])

        {!! Form::close() !!}

        </div>
    </div>
</div>

<script>
    function ddd(){
        var role_id = document.getElementById("role_id").value;
        var team1 = document.getElementById("team1");
        var team2 = document.getElementById("team2");
        if(role_id == 4){
            team2.style.display = "block";
            team1.style.display = "none"
        }
        else{
            team2.style.display = "none";
            team1.style.display = "block"
        }
    }
</script>
@stop