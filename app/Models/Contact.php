<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use  SoftDeletes;

    protected $fillable = [
        'external_id',
        'name',
        'email',
        'primary_number',
        'secondary_number',
        'client_id',
        'is_primary',
        ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public static function departments($user_id) {
        $t = "";
        $departments = DepartmentUser::where('user_id', $user_id)->pluck('department_id');
        if(count($departments)>0) {
            $departments_list = Department::whereIn('id', $departments)->pluck('name');
            if(count($departments_list) > 0){
                foreach($departments_list as $t){
                    $t = $t.",";
                }
            }
            return $t;
        }

    }
}
