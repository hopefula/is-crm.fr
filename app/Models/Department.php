<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable =
    [
        'name',
        'external_id',
        'description',
    ];

    protected $hidden = ['pivot'];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
    public function supervisors($department_id)
    {
        // $department = Department::find($department_id);
        $department_users = DepartmentUser::where("department_id", $department_id)->pluck("user_id")->toArray();
        $super_users = RoleUser::where('role_id', 4)->pluck('user_id')->toArray();
        $supervisor = new User;;
        foreach($super_users as $temp) {
            if(in_array($temp, $department_users)) {
                $supervisor = User::find($temp);
            }
        }
        return $supervisor;
    }
}
