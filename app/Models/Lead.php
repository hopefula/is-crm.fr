<?php
namespace App\Models;

use App\Observers\ElasticSearchObserver;
use App\Services\Comment\Commentable;
use App\Traits\DeadlineTrait;
use App\Traits\SearchableTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Carbon;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;

/**
 * @property bool qualified
 * @property string title
 * @property string external_id
 * @property integer user_assigned_id
 * @property Status status
 * @property Client client
 * @property integer invoice_id
 * @property integer status_id
 * @property Invoice invoice
 */
class Lead extends Model implements Commentable
{
    use SearchableTrait, SoftDeletes, DeadlineTrait;

    protected $searchableFields = ['title'];

    const LEAD_STATUS_CLOSED = "closed";

    protected $fillable = [
        'external_id',
        'title',
        'description',
        'status_id',
		'Campagne',
        'user_assigned_id',
        'user_created_id',
        'client_id',
        'qualified',
        'result',
        'deadline',
        'invoice_id',
        'Nom',
        'lname',
        'Ville',
        'address',
        'Codepostal',
        'tel1',
        'tel2',
        'mobile1',
        'mobile2',
        'email',
        'lead_type',
        'Demander',
        'Surface',
		'Sur_quoi',
        'Surface_des',
        'Surface_Comble',
        'Configuration',
        'Maison',
        'Zone',
        'Quelle',
        'ancienne',
        'Commentaire',
        'Mode',
        'Mode_de',
        'Situation',
        'Situation_de',
		'Sur_quoi',
        'Solutions',
        'Emploi',
        'Quel',
        'Nombre',
        'Hauteur',
        'Hauteursous',
        'endroit',
        'category',
        'Personne',
        'du_mur',
        'sous_sol',
        'isolation',
        'comble',
        'actuelle',
        'niveau',
        'descendant',
        'agricole',
        'Plafond',
        'Metier',
        'quelques',
        'Fiscal1',
        'RefAvis1',
        'Fiscal2',
        'RefAvis2',
        'Revenue',
        'Nom_de',
        'Date_de',
		'produit1',

        'budget_cpf',
        'Formation',
        'Date_de_rdv',
        'Heure_de_rdv',
        'Commentaire',
        'Campagne',
        
        'app_created_date',
        'app_confirm_date',
        'app_install_date'
    ];

    protected $casts = [
        'sous_sol' => 'array',
        'ancienne' => 'array',
        'comble' => 'json',
        'isolation' => 'array'
    ];

    protected $dates = ['deadline'];

    protected $hidden = ['remember_token'];


    public static function boot()
    {
        parent::boot();

        // This makes it easy to toggle the search feature flag
        // on and off. This is going to prove useful later on
        // when deploy the new search engine to a live app.
        //if (config('services.search.enabled')) {
        static::observe(ElasticSearchObserver::class);
        //}
    }

    public function getRouteKeyName()
    {
        return 'external_id';
    }

    public function displayValue()
    {
        return $this->title;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_assigned_id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_created_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'source');
    }

    public function comment()
    {
        return $this->hasMany(Comment::class, 'source_id', 'id');
    }

    public function getCreateCommentEndpoint(): String
    {
        return route('comments.create', ['type' => 'lead', 'external_id' => $this->external_id]);
    }

    public function getShowRoute()
    {
        return route('leads.show', [$this->external_id]);
    }

    public function convertToQualified()
    {
        $this->qualified = true;
        $this->save();
    }

    public function getIsQualifiedAttribute()
    {
        return $this->qualified;
    }

    public function activity()
    {
        return $this->morphMany(Activity::class, 'source');
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function getAssignedUserAttribute()
    {
        return User::findOrFail($this->user_assigned_id);
    }

    public function isClosed()
    {
        return $this->status == self::LEAD_STATUS_CLOSED;
    }

    public function scopeIsNotQualified(Builder $query)
    {
        return $query->where('qualified', false);
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
    /**
     * @return array
     */
    public function getSearchableFields(): array
    {
        return $this->searchableFields;
    }

    public function convertToOrder()
    {
        if(!$this->canConvertToOrder()) {
            return false;
        }
        $invoice = Invoice::create([
            'status' => 'draft',
            'client_id' => $this->client->id,
            'external_id' =>  Uuid::uuid4()->toString()
        ]);

        $this->invoice_id = $invoice->id;
        $this->status_id = Status::typeOfLead()->where('title', 'Closed')->first()->id;
        $this->save();

        return $invoice;
    }

    public function canConvertToOrder()
    {
        if($this->invoice) {
            return false;
        }
        return true;
    }
}
