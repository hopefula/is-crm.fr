<?php

namespace App\Models;

use App\Observers\ElasticSearchObserver;
use App\Services\Comment\Commentable;
use App\Traits\DeadlineTrait;
use App\Traits\SearchableTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Carbon;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;


class LeadProduct extends Model
{
    //
    use SoftDeletes;
    protected $fillable = [
        'lead_id',
        'Demander',
        'Surface',
		'Sur_quoi',
        'Maison',
        'Zone',
        'Quelle',
        'Commentaire',
        'Mode',
        'Situation',
        'Emploi',
        'Quel',
        'Nombre',
        'Fiscal1',
        'RefAvis1',
        'Fiscal2',
        'RefAvis2',
		'produit1',
        'Revenue'
    ];

}
