@extends('layouts.master')
@section('heading')
    {{__('Create department')}}
@stop

@section('content')

<div class="col-lg-12 layout-spacing">
    <div class="statbox widget box box-shadow">
        <div class="widget-content widget-content-area">
                
            <a class="btn btn-info" href="{{route('departments.index')}}">Retour</a><br>
            {!! Form::open([
                    'route' => 'departments.store',
                    ]) !!}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label for="supervisor" class="control-label thin-weight">Superviseur</label>
                <select name="supervisor[]" id="supervisor" class="form-control tagging form-small" multiple required>
                    @foreach($users as $user)
                        <option value={{$user->id}}>{{ $user->name}}</option>
                    @endforeach
                </select>
            </div>
    
            <div class="form-group">
                {!! Form::label('Name', __("Nom de l'équipe") . ':', ['class' => 'control-label thin-weight']) !!}
                {!! Form::text('name', null,['class' => 'form-control', 'required' => 'required']) !!}
            </div>

            <div class="form-group">
                {!! Form::label( __('Description'),  __("Déscription de l'équipe") . ':', ['class' => 'control-label thin-weight']) !!}
                {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
            </div>
            {!! Form::submit(__("Créer l'équipe"), ['class' => 'btn btn-md btn-primary']) !!}

            {!! Form::close() !!}
        </div>    
    </div>    
</div>    

@endsection