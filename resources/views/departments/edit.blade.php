@extends('layouts.master')
@section('heading')
    {{__('Create department')}}
@stop

@section('content')
<div class="col-lg-12 layout-spacing">
    <div class="statbox widget box box-shadow">
        <div class="widget-content widget-content-area">
            <a class="btn btn-info" href="{{route('departments.index')}}">Retour</a><br>
        <form method="post" action="update">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="external_id" value="{{ $department->external_id }}">
            <div class="form-group">
                <label for="Situation" class="control-label thin-weight">Superviseur</label>
                <select name="Situation[]" id="Situation" class="form-control tagging form-small" multiple required>
                    @foreach($users as $user)
                        <option value={{$user->id}} {{in_array($user->id, $current_super) ? "selected":''}}>{{ $user->name}}</option>
                    @endforeach
                </select>
            </div>
    
            <div class="form-group">
                {!! Form::label('Name', __("Nom de l'équipe") . ':', ['class' => 'control-label thin-weight']) !!}
                {!! Form::text('name', $department->name,['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label( __('Description'),  __("Déscription de l'équipe") . ':', ['class' => 'control-label thin-weight']) !!}
                {!! Form::textarea('description', $department->description, ['class' => 'form-control']) !!}
            </div>
            {!! Form::submit(__("Valider"), ['class' => 'btn btn-md btn-primary']) !!}

            {!! Form::close() !!}
        </div>    
    </div>    
</div>    

@endsection