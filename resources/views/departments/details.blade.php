@extends('layouts.master')
@section('heading')
    {{__('Details department')}}
@stop

@section('content')

<div class="col-lg-12 layout-spacing">
    <div class="statbox widget box box-shadow">
        <div class="widget-content widget-content-area">
            <a class="btn btn-info" href="{{route('departments.index')}}">Retour</a><br>
            <div class="form-group"><br>
                <label>Nom de l'équipe</label>
                <p>{{$department->name}}</p>
            </div>
            <div class="form-group">
                <label>Description de l'équipe</label>
                <p>{{$department->description}}</p>
            </div>
            <div class="form-group">
                <label for="Situation" class="control-label thin-weight">Superviseur</label><br/>
                    @foreach($users as $user)
                        <span>{{$user}}, </span>
                    @endforeach
            </div>
    
            <div class="form-group">
                <label>Agents</label>
                @if($department->users->count() > 0)
                @foreach($department->users as $temp)
                    @if($department->supervisors($department->id))
                        @if($temp->id != $department->supervisors($department->id)->id)
                        <p>{{$temp->name}}</p>
                        @endif
                    @else
                        <p>{{$temp->name}}</p>
                    @endif
                @endforeach
                @endif
            </div>
        </div>    
    </div>    
</div>    

@endsection