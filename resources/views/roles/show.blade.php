@extends('layouts.master')

@section('content')
<div class="col-lg-10">
    <div class="statbox widget box box-shadow">
        <div class="widget-header">                                
            <div class="">
                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h1>{{ $role->display_name }}</h1> <span>{{ __('Permission management') }}</span>
                    <a class="btn btn-info" href="{{route('roles.index')}}">Back</a>
                </div>                                                                        
            </div>
        </div>
        <div class="widget-content widget-content-area">
            
            <div class="col-xs-12">
            {!! Form::model($permissions_grouping, [
                'method' => 'PATCH',
                'url'    => ['roles/update', $role->external_id],
            ]) !!}
        @foreach($permissions_grouping as $permissions)
        <div class="">
        @if($permissions->first)
        <div class="col-md-2">
            <p class="calm-header">{{ucfirst(__($permissions->first()->grouping))}} </p>
        </div>
        @endif
        <div class="col-md-9">
            @foreach($permissions as $permission)
        <div class="col-xs-6 col-md-6">
                <?php $isEnabled = !current(
            array_filter(
                            $role->permissions->toArray(),
                            function ($element) use ($permission) {
                                    return $element['id'] === $permission->id;
                                }
                        )
        );  ?>
            <div class="white-box">
                <input type="checkbox"
                            {{ !$isEnabled ? 'checked' : ''}} name="permissions[ {{ $permission->id }} ]"
                        value="1" data-role="{{ $role->id }}">

                    <span class="perm-name lead"><small>{{ $permission->display_name }}</small></span><br/>
                    {{ $permission->description }}
            </div>
            </div>
            @endforeach
            </div>
            </div>
            <hr>
            @endforeach
            <div class="row">
                <div class="col-xs-6">
                    {!! Form::submit( __('Update Role') , ['class' => 'btn btn-primary']) !!}
                    {!! Form::close(); !!}
                </div>
            
            @if($role->name == "owner" || $role->name == "administrator")
            @else
                <div class="col-xs-6">
                {!! Form::open([
                    'method' => 'DELETE',
                    'route' => ['roles.destroy', $role->id]
                ]); !!}
                                
                                    {!! Form::submit(__('Delete'), ['class' => 'btn btn-danger', 'onclick' => 'return confirm("Are you sure?")']); !!}
                {!! Form::close(); !!}
                </div>
            @endif
            </div>
                <!-- <div class="col-sm-12">
                    <h3>{{ __('Users with this role') }} ({{ $role->users->count() }}):</h3>
                    @foreach($role->users as $user)
                        {{ $user->name }} <br />
                    @endforeach
                </div> -->
		</div>
</div>
</div>
</div>
@endsection