@extends('layouts.master')

@section('content')
<div class="col-lg-10">
    <div class="statbox widget box box-shadow">
        <div class="widget-header">                                
            <div class="">
                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h1>Create New Role</h1>
                    <a class="btn btn-info" href="{{route('roles.index')}}">Back</a>
                </div>                                                                        
            </div>
        </div>
        <div class="widget-content widget-content-area">
        {!! Form::open([
                'route' => 'roles.store',
                ]) !!}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            {!! Form::label('name', __('Name'), ['class' => 'control-label']) !!}
            {!! Form::text('name', null,['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('description', __('Description'), ['class' => 'control-label']) !!}
            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        </div>
        {!! Form::submit( __('Add new Role'), ['class' => 'btn btn-md btn-brand']) !!}

        {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection