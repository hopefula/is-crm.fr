


@extends('layouts.master')
@push('style')
<style>
    .widget-one_hybrid .widget-heading * {
        color: black !important;
    }
</style>
@endpush
@section('content')
<div class="row widget-content widget-content-area">
    @foreach($lead_static as $temp)
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 mb-4">
        <div class="statbox widget box box-shadow">

            <div class="widget widget-one_hybrid" style="background:{{$temp['color']}};color:black!important; text-align: center;">
                <div class="widget-heading">
                    <div class="w-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                    </div>
                    
                    <p class="w-value" style="color:black!important; text-align: center;">{{$temp["count"]}}</p>
                    <h5 class="" style="color:black!important; text-align: center;">{{$temp["title"]}}</h5>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>

@endsection

