
    <div class="col-md-4 product_info">
        <div class="form-group">
            <h4>Informations du produit</h4>
        </div>
		<div class="form-group">
            <label for="produit1" class="control-label thin-weight">Nom du Produit</label>
		<input type="text" value="Douche" name="produit1" id="produit1" class="form-control" >
        </div>
        <div class="form-group">
            <label for="actuelle" class="control-label thin-weight">installation de la salle de bain actuelle</label>
            <select name="actuelle" id="actuelle" class="form-control" >
                <option value=""></option>
                <option value="Baignoire">Baignoire</option>
                <option value="douche">douche</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Situation" class="control-label thin-weight">Situation</label>
            <select name="Situation" id="Situation" class="form-control" >
                <option value=""></option>
                <option value="Propriétaire  occupant">Propriétaire  occupant</option>
                <option value="Locataire">Locataire</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Solutions" class="control-label thin-weight">Solutions adaptées aux Personnes à Mobilité Réduite</label>
            <input type="text" name="Solutions" id="Solutions" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="agricole" class="control-label thin-weight">retraité d’une entreprise du secteur privé âgé, y compris agricole, de 70 ans et plus</label>
            <input type="text" name="agricole" id="agricole" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="niveau" class="control-label thin-weight">Agé de 60 ans et un plus en situation de perte d'autonomie avec in niveau GIR de 1 à 4</label>
            <input type="text" name="niveau" id="niveau" class="form-control"  />
        </div>
        
    </div>
    <div class="col-md-4 product_info">
        <div class="form-group">
            <h4>&nbsp</h4>
        </div>
        <div class="form-group">
            <label for="descendant" class="control-label thin-weight">hébergé chez un descendant salarié d’une entreprise du secteur privé</label>
            <input type="text" name="descendant" id="descendant" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Personne" class="control-label thin-weight">Nombre de Personne(s) déclarée(s) à cette adresse</label>
            <input type="text" name="Personne" id="Personne" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Fiscal1" class="control-label thin-weight">Numéro Fiscal 1</label>
            <input type="text" name="Fiscal1" id="Fiscal1" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="RefAvis1" class="control-label thin-weight">Référence de l'avis 1</label>
            <input type="text" name="RefAvis1" id="RefAvis1" class="form-control"  />
        </div>

        <div class="form-group">
            <label for="Fiscal2" class="control-label thin-weight">Numéro Fiscal 2</label>
            <input type="text" name="Fiscal2" id="Fiscal2" class="form-control"  />
        </div>
    </div>
    
    <div class="col-md-4 product_info">
        <div class="form-group">
            <h4>&nbsp</h4>
        </div>
        <div class="form-group">
            <label for="RefAvis2" class="control-label thin-weight">Référence de l'avis 2</label>
            <input type="text" name="RefAvis2" id="RefAvis2" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Revenue" class="control-label thin-weight">Revenu fiscal de référence</label>
            <input type="text" name="Revenue" id="Revenue" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Nom_de" class="control-label thin-weight">Nom de l'agent</label>
            <input type="text" name="Nom_de" id="Nom_de" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Date_de" class="control-label thin-weight">Date de rdv</label>
            <input type="text" name="Date_de" id="Date_de" class="form-control"  />
        </div>
 
    </div>
