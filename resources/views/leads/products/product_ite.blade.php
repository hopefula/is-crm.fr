<div class="col-md-4 product_info">
        <div class="col-md-12">
            <h4>Informations du produit</h4>
        </div>
	<div class="form-group">
            <label for="produit1" class="control-label thin-weight">Nom du Produit</label>
		<input type="text" value="ITE" name="produit1" id="produit1" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Sur_quoi" class="control-label thin-weight">Sur quoi pose-t-on l'isolation ?</label>
            <select name="Sur_quoi[]" id="Sur_quoi" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Parpaing">Parpaing</option>
                <option value="Brique creuse">Brique creuse</option>
                <option value="Béton">Béton</option>
                <option value="brique pleine">brique pleine</option>
                <option value="crépi">crépi</option>
                <option value="autres">autres</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Surface_Comble" class="control-label thin-weight">Surface Comble</label>
            <input type="text" name="Surface_Comble" id="Surface_Comble" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Configuration" class="control-label thin-weight">Configuration de la maison ? *</label>
            <select name="Configuration[]" id="Configuration" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Maison de ville">Maison de ville (avec au moins une courette)</option>
                <option value="Maison individuelle">Maison individuelle</option>
                <option value="mitoyenne un seul coté">mitoyenne d'un seul coté</option>
                <option value="mitoyenne des deux cotés">mitoyenne des deux cotés</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Surface_des" class="control-label thin-weight">Surface des murs en m2 de la maison</label>
            <input type="text" name="Surface_des" id="Surface_des" class="form-control"  />
        </div>
        
        <div class="form-group">
            <label for="Maison" class="control-label thin-weight">Maison de plus de 2 ans</label>
            <select name="Maison" id="Maison" class="form-control" >
                <option value=""></option>
                <option value="Oui">Oui</option>
                <option value="Non">Non</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Zone" class="control-label thin-weight">Zone</label>
            <select name="Zone" id="Zone" class="form-control" >
                <option value=""></option>
                <option value="Zone B2">Zone B2</option>
                <option value="Zone C">Zone C</option>
                <option value="ACTION COEUR DE VILLE">ACTION CŒUR DE VILLE</option>
                <option value="Autre">Autre</option>
            </select>
        </div>
	
	<div class="form-group">
            <label for="Quelle" class="control-label thin-weight">Quelle isolation à jour *</label>
            <select name="Quelle[]" id="Quelle" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Combles">Combles</option>
                <option value="Fenetre (double vitrage)">Fenêtre (double vitrage)</option>
                <option value="Sous-sol">Sous-sol</option>
            </select>
        </div>
	
	
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <h4>&nbsp</h4>
        </div>
        <div class="form-group">
            <label for="Commentaire" class="control-label thin-weight">Commentaire isolation</label>
            <textarea class="form-control" name = "Commentaire" cols="50" rows="5"></textarea>
        </div>
        <div class="form-group">
            <label for="Mode_de" class="control-label thin-weight">Mode de Chauffage</label>
            <select name="Mode_de" id="Mode_de" class="form-control" >
                <option value=""></option>
                <option value="Electrique">Electricité</option>
                <option value="Gaz">Gaz</option>
                <option value="Fioul">Fioul</option>
                <option value="Bois">Bois</option>
                <option value="PAC">PAC</option>
                <option value="Poele a granulés">Poêle à granulés</option>
                <option value="Autre">Autre</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Situation_de" class="control-label thin-weight">Situation de maison</label>
            <select name="Situation_de" id="Situation_de" class="form-control" >
                <option value=""></option>
                <option value="Propriétaire occupant">Propriétaire occupant</option>
                <option value="Locataire">Locataire</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Emploi" class="control-label thin-weight">Emploi salarié</label>
            <select name="Emploi" id="Emploi" class="form-control" >
                <option value=""></option>
                <option value="Secteur Privé">Secteur Privé</option>
                <option value="Secteur publique">Secteur publique</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Metier" class="control-label thin-weight">Métier</label>
            <input type="text" name="Metier" id="Metier" class="form-control"  />
        </div>

        <div class="form-group">
            <label for="quelques" class="control-label thin-weight">Il y a t'il quelques choses de fixés sur mur extérieur ?</label>
            <select name="quelques[]" id="quelques" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Pergolas">Pergolas</option>
                <option value="Appentis">Appentis</option>
                <option value="Vérandas">Vérandas</option>
                <option value="Autre">Autre</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Nombre" class="control-label thin-weight">Nombre de Personne(s) déclarée(s) à cette adresse</label>
            <input type="text" name="Nombre" id="Nombre" class="form-control"  />
        </div>
    </div>
    <div class="col-md-4">        
        <div class="form-group">
            <h4>&nbsp</h4>
        </div>
        
        <div class="form-group">
            <label for="Fiscal1" class="control-label thin-weight">Numéro Fiscal 1</label>
            <input type="text" name="Fiscal1" id="Fiscal1" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="RefAvis1" class="control-label thin-weight">Référence de l'avis 1</label>
            <input type="text" name="RefAvis1" id="RefAvis1" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Fiscal2" class="control-label thin-weight">Numéro Fiscal 2</label>
            <input type="text" name="Fiscal2" id="Fiscal2" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="RefAvis2" class="control-label thin-weight">Référence de l'avis 2</label>
            <input type="text" name="RefAvis2" id="RefAvis2" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Revenue" class="control-label thin-weight">Revenu fiscal de référence</label>
            <input type="text" name="Revenue" id="Revenue" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Nom_de" class="control-label thin-weight">Nom de l'agent</label>
            <input type="text" name="Nom_de" id="Nom_de" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Date_de" class="control-label thin-weight">Date de rdv</label>
            <input type="text" name="Date_de" id="Date_de" class="form-control"  />
        </div>
    </div>
