<div class="col-md-4 product_info">
        <div class="col-md-12">
            <h4>Informations du produit</h4>
        </div>
	<div class="form-group">
            <label for="produit1" class="control-label thin-weight">Nom du Produit</label>
		<input type="text" value="Isolation Intérieur" name="produit1" id="produit1" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Quelle" class="control-label thin-weight">Quelle est la partie à isoler ?</label>
            <select name="Quelle[]" id="Quelle" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Comble aménageable">Comble aménageable</option>
                <option value="Comble perdu">Comble perdu</option>
                <option value="Rampants Intérieurs">Rampants Intérieurs</option>
                <option value="Mur Pignon">Mur Pignon</option>
                <option value="Plafond de garage attenant">Plafond de garage attenant</option>
                <option value="Plafond de sous-sol">Plafond de sous-sol</option>
                <option value="Plafond de cave">Plafond de cave</option>
                <option value="Vide Sanitaire">Vide Sanitaire</option>
                <option value="Mur Attenant">Mur Attenant</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Surface" class="control-label thin-weight">Surface Comble</label>
            <input type="text" name="Surface" id="Surface" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Plafond" class="control-label thin-weight">Surface Plafond-Bas (sous-sol, cave, vide sanitaire) : Rappel : Il doit y avoir obligatoirement une pièce de vie au-dessus</label>
            <input type="text" name="Plafond" id="Plafond" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Surface_des" class="control-label thin-weight">Surface des murs</label>
            <input type="text" name="Surface_des" id="Surface_des" class="form-control" >
        </div>
        
        <div class="form-group">
            <label for="comble" class="control-label thin-weight">Accessibilité comble</label>
            <select name="comble[]" id="comble" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Trappe intérieure (Comble)">Trappe intérieure (Comble)</option>
                <option value="Escalier intérieur (Comble)">Escalier intérieur (Comble)</option>
                <option value="Entrée extérieure (Comble)">Entrée extérieure (Comble)</option>
                <option value="Détuilage (Comble)">Détuilage (Comble)</option>
            </select>
        </div>
        <div class="form-group">
            <label for="isolation" class="control-label thin-weight">Sur quoi pose-t-on l'isolation ?</label>
            <select name="isolation[]" id="isolation" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Solives/ Fermettes / poutres (Comble)">Solives/ Fermettes / poutres (Comble)</option>
                <option value="Plancher (Comble)">Plancher (Comble)</option>
                <option value="Plafond Parpaings">Plafond Parpaings</option>
                <option value="Plafond Briques">Plafond Briques</option>
                <option value="Plafond Bois">Plafond Bois</option>
                <option value="Plafond hourdi">Plafond hourdi</option>
            </select>
        </div>
        <div class="form-group">
            <label for="sous_sol" class="control-label thin-weight">Accessibilité sous-sol</label>
            <select name="sous_sol[]" id="sous_sol" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Porte Garage">Porte Garage (Garage en rez de chaussée)</option>
                <option value="Escalier">Escalier (Sous-sol, Cave)</option>
                <option value="Accès trappe extérieur">Accès trappe extérieur Vide Sanitaire (trappe 60x70cm min)</option>
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <h4>&nbsp</h4>
        </div>
        <div class="form-group">
            <label for="du_mur" class="control-label thin-weight">Matière du mur ?</label>
            <select name="du_mur" id="du_mur" class="form-control" >
                <option value=""></option>
                <option value="Parpaing">Parpaing</option>
                <option value="Brique creuse">Brique creuse</option>
                <option value="Béton">Béton</option>
                <option value="Autre">Autre</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Hauteur" class="control-label thin-weight">Hauteur de Détuilage</label>
            <input type="text" name="Hauteur" id="Hauteur" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Hauteursous" class="control-label thin-weight">Hauteur sous plafond du Vide sanitaire en cm</label>
            <input type="text" name="Hauteursous" id="Hauteursous" class="form-control" >
        </div>
        <div class="form-group">
            <label for="endroit" class="control-label thin-weight">L'endroit est-il Dégagé</label>
            <select name="endroit" id="endroit" class="form-control" >
                <option value=""></option>
                <option value="Oui">Oui</option>
                <option value="Non">Non, mais le prospect s'engage à le dégager d'ici la date de rdv</option>
            </select>
        </div>
        <div class="form-group">
            <label for="ancienne" class="control-label thin-weight">Y a-t-il une ancienne isolation</label>
            <select name="ancienne[]" id="ancienne" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Vieille de 5 ans ou + (comble)">Vieille de 5 ans ou + (comble)</option>
                <option value="4 cm ou moins de poly ou autre (plafond)">4 cm ou moins de poly ou autre (plafond)</option>
                <option value="+ de 4 cm de poly ou autre (plafond)">+ de 4 cm de poly ou autre (plafond)</option>
                <option value="Autres">Autres (précisez de quoi il s'agit dans ''commentaire concernant l'isolation'')</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Commentaire" class="control-label thin-weight">Commentaire(s) Isolation</label>
            <input type="text" name="Commentaire" id="Commentaire" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Situation" class="control-label thin-weight">Situation</label>
            <select name="Situation" id="Situation" class="form-control" >
                <option value=""></option>
                <option value="Propriétaire">Propriétaire</option>
                <option value="Locataire">Locataire</option>
            </select>
        </div>
        <div class="form-group">
            <label for="category" class="control-label thin-weight">Catégorie</label>
            <select name="category" id="category" class="form-control" >
                <option value=""></option>
                <option value="Grand Précaire">Grand Précaire</option>
                <option value="Précaire (A)">Précaire (A)</option>
                <option value="Classique (C)">Classique (C)</option>
            </select>
        </div>
    </div>
    <div class="col-md-4 product_info">
        <div class="col-md-12">
            <h4>&nbsp</h4>
        </div>
        
        <div class="form-group">
            <label for="Mode" class="control-label thin-weight">Mode de Chauffage</label>
            <select name="Mode" id="Mode" class="form-control" >
                <option value=""></option>
                <option value="Electricité">Electricité</option>
                <option value="Gaz">Gaz</option>
                <option value="Bois">Bois</option>
                <option value="Fioul">Fioul</option>
                <option value="Pompe à chaleur (électricité)">Pompe à chaleur (électricité)</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Personne" class="control-label thin-weight">Nombre de Personne(s) déclarée(s) à cette adresse</label>
            <input type="text" name="Personne" id="Personne" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Fiscal1" class="control-label thin-weight">Numéro Fiscal 1</label>
            <input type="text" name="Fiscal1" id="Fiscal1" class="form-control" >
        </div>
        <div class="form-group">
            <label for="RefAvis1" class="control-label thin-weight">Référence de l'avis 1</label>
            <input type="text" name="RefAvis1" id="RefAvis1" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Fiscal2" class="control-label thin-weight">Numéro Fiscal 2</label>
            <input type="text" name="Fiscal2" id="Fiscal2" class="form-control" >
        </div>
        <div class="form-group">
            <label for="RefAvis2" class="control-label thin-weight">Référence de l'avis 2</label>
            <input type="text" name="RefAvis2" id="RefAvis2" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Revenue" class="control-label thin-weight">Revenu fiscal de référence</label>
            <input type="text" name="Revenue" id="Revenue" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Date_de" class="control-label thin-weight">Date de rdv</label>
            <input type="text" name="Date_de" id="Date_de" class="form-control" >
        </div>
    </div>
