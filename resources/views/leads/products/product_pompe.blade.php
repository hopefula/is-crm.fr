
    <div class="col-md-4 product_info">
        <div class="form-group">
            <h4>Informations du produit</h4>
        </div>
		<div class="form-group">
            <label for="produit1" class="control-label thin-weight">Nom du Produit</label>
		<input type="text" value="Pompe à chaleur" name="produit1" id="produit1" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Surface" class="control-label thin-weight">Surface habitable en m2 de la maison</label>
            <input type="text" name="Surface" id="Surface" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Zone" class="control-label thin-weight">Zone</label>
            <select name="Zone" id="Zone" class="form-control" >
                <option value=""></option>
                <option value="Zone B2">Zone B2</option>
                <option value="Zone C">Zone C</option>
                <option value="ACTION COEUR DE VILLE">ACTION COEUR DE VILLE</option>
                <option value="Autre">Autre</option>
            </select>
        </div>

        <div class="form-group">
            <label for="Quelle" class="control-label thin-weight">Quelle isolation à jour *</label>
            <select name="Quelle[]" id="Quelle" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Combles">Combles</option>
                <option value="Fenetre (double vitrage)">Fenêtre (double vitrage)</option>
                <option value="Sous-sol">Sous-sol</option>
            </select>
        </div>				
		
        <div class="form-group">
            <label for="Commentaire" class="control-label thin-weight">Commentaire isolation</label>
            <input type="text" name="Commentaire" id="Commentaire" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Mode_de" class="control-label thin-weight">Mode de Chauffage</label>
            <select name="Mode_de" id="Mode_de" class="form-control" >
                <option value=""></option>
                <option value="Electricité">Electricité</option>
                <option value="Gaz">Gaz</option>
                <option value="Fioul">Fioul</option>
                <option value="Bois">Bois</option>
                <option value="PAC">PAC</option>
                <option value="Poêle à granulés">Poêle à granulés</option>
                <option value="Autre">Autre</option>
            </select>
        </div>
    </div>
    <div class="col-md-4 product_info">
        <div class="form-group">
            <label for="Situation" class="control-label thin-weight">Situation de maison</label>
            <select name="Situation" id="Situation" class="form-control" >
                <option value=""></option>
                <option value="Propriétaire occupant">Propriétaire occupant</option>
                <option value="Locataire">Locataire</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Emploi" class="control-label thin-weight">Emploi salarié</label>
            <select name="Emploi" id="Emploi" class="form-control" >
                <option value=""></option>
                <option value="Secteur Privé">Secteur Privé</option>
                <option value="Secteur publique">Secteur publique</option>
            </select>
        </div>
        
        <div class="form-group">
            <label for="Metier" class="control-label thin-weight">Métier</label>
            <input type="text" name="Metier" id="Metier" class="form-control"  />
        </div>
    
        <div class="form-group">
            <label for="Personne" class="control-label thin-weight">Nombre de Personne(s) déclarée(s) à cette adresse</label>
            <input type="text" name="Personne" id="Personne" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Fiscal1" class="control-label thin-weight">Numéro Fiscal 1</label>
            <input type="text" name="Fiscal1" id="Fiscal1" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="RefAvis1" class="control-label thin-weight">Référence de l'avis 1</label>
            <input type="text" name="RefAvis1" id="RefAvis1" class="form-control"  />
        </div>
    </div>
    <div class="col-md-4 product_info">
        <div class="form-group">
            <label for="Fiscal2" class="control-label thin-weight">Numéro Fiscal 2</label>
            <input type="text" name="Fiscal2" id="Fiscal2" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="RefAvis2" class="control-label thin-weight">Référence de l'avis 2</label>
            <input type="text" name="RefAvis2" id="RefAvis2" class="form-control"  />
        </div>

        <div class="form-group">
            <label for="Revenue" class="control-label thin-weight">Revenu fiscal de référence</label>
            <input type="text" name="Revenue" id="Revenue" class="form-control"  />
        </div>

        <div class="form-group">
            <label for="Revenue" class="control-label thin-weight">Date de rdv</label>
            <input type="text" name="Revenue" id="Revenue" class="form-control"  />
        </div>
    </div>
    