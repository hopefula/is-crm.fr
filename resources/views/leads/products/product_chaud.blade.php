
    <div class="col-md-4 product_info">
        <div class="form-group">
            <h4>Informations du produit</h4>
        </div>
		<div class="form-group">
            <label for="produit1" class="control-label thin-weight">Nom du produit</label>
            <input value="Chaudière" type="text" name="produit1" id="produit1" class="form-control" />
        </div>
        <div class="form-group">
            <label for="Demander" class="control-label thin-weight">Demander impérativement l'ancienne marque de la chaudière murale</label>
            <input type="text" name="Demander" id="Demander" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Surface" class="control-label thin-weight">Surface habitable en m2 de la maison ?</label>
            <input type="text" name="Surface" id="Surface" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Maison" class="control-label thin-weight">Maison de plus de 2 ans</label>
            <select name="Maison" id="Maison" class="form-control" >
                <option value=""></option>
                <option value="Oui">Oui</option>
                <option value="Non">Non</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Zone" class="control-label thin-weight">Zone</label>
            <select name="Zone" id="Zone" class="form-control" >
                <option value=""></option>
                <option value="B2">B2</option>
                <option value="C">C</option>
                <option value="Action">Action cœur de ville</option>
                <option value="Autres">Autres</option>
            </select>
        </div>
       <div class="form-group">
            <label for="Quelle" class="control-label thin-weight">Quelle isolation à jour *</label>
            <select name="Quelle[]" id="Quelle" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Combles">Combles</option>
                <option value="Fenetre (double vitrage)">Fenêtre (double vitrage)</option>
                <option value="Sous-sol">Sous-sol</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Commentaire" class="control-label thin-weight">Commentaire pour la pose</label>
            <input type="text" name="Commentaire" id="Commentaire" class="form-control"  />
        </div>
    </div>
    <div class="col-md-4 product_info">
        <div class="form-group">
            <h4>&nbsp</h4>
        </div>
        <div class="form-group">
            <label for="Mode" class="control-label thin-weight">Mode de Chauffage</label>
            <select name="Mode" id="Mode" class="form-control" >
                <option value=""></option>
                <option value="GAZ">GAZ</option>
                <option value="AUTRES">AUTRES</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Situation" class="control-label thin-weight">Situation</label>
            <select name="Situation" id="Situation" class="form-control" >
                <option value=""></option>
                <option value="Prop">Propriétaire occupant</option>
                <option value="Locataire">Locataire</option>
            </select>
        </div>
     
        <div class="form-group">
            <label for="Emploi" class="control-label thin-weight">Emploi salarié une personne dans la famille qui travaille dans le secteur privée </label>
            <select name="Emploi" id="Emploi" class="form-control" >
                <option value=""></option>
                <option value="priv">Secteur privé</option>
                <option value="publique">Secteur publique</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Quel" class="control-label thin-weight">Quel métier?</label>
            <input type="text" name="Quel" id="Quel" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Nombre" class="control-label thin-weight">Nombre de Personne(s) déclarée(s) à cette adresse</label>
            <input type="text" name="Nombre" id="Nombre" class="form-control"  />
        </div>
    </div>
        
    <div class="col-md-4 product_info">
        <div class="form-group">
            <h4>&nbsp</h4>
        </div>
        <div class="form-group">
            <label for="Fiscal1" class="control-label thin-weight">Numéro Fiscal 1</label>
            <input type="text" name="Fiscal1" id="Fiscal1" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="RefAvis1" class="control-label thin-weight">Référence de l'avis 1</label>
            <input type="text" name="RefAvis1" id="RefAvis1" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Fiscal2" class="control-label thin-weight">Numéro Fiscal 2</label>
            <input type="text" name="Fiscal2" id="Fiscal2" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="RefAvis2" class="control-label thin-weight">Référence de l'avis 2</label>
            <input type="text" name="RefAvis2" id="RefAvis2" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Revenue" class="control-label thin-weight">Revenu fiscal de référence</label>
            <input type="text" name="Revenue" id="Revenue" class="form-control"  />
        </div>
    </div>