@extends('layouts.master')

@section('content')
<div class="statbox widget box box-shadow">
    
    <div class="widget-content widget-content-area border-top-tab">
        <ul>
            <div class="toggle-list">
				<a class="btn btn-info toggle-vis mb-4 ml-2" data-column="0">Id</a>
                <a class="btn btn-info toggle-vis mb-4 ml-2" data-column="1">nom</a>
                <a class="btn btn-info toggle-vis mb-4 ml-2" data-column="2">Prénom</a>
                <a class="btn btn-info toggle-vis mb-4 ml-2" data-column="3">Ville</a>
                <a class="btn btn-info toggle-vis mb-4 ml-2" data-column="4">Code Postal</a>
                <a class="btn btn-info toggle-vis mb-4 ml-2" data-column="5">Adresse</a>
                <a class="btn btn-info toggle-vis mb-4 ml-2" data-column="6">Téléphone</a>
                <a class="btn btn-info toggle-vis mb-4 ml-2" data-column="7">Créé par</a>
                <a class="btn btn-info toggle-vis mb-4 ml-2" data-column="8">Commentaires</a>
                <a class="btn btn-info toggle-vis mb-4 ml-2" data-column="9">Statut</a>
				<a class="btn btn-info toggle-vis mb-4 ml-2" data-column="10">Produit</a>
                
            </div>
        </ul>
        
        <ul class="nav nav-tabs mb-3 mt-3 d-none" id="borderTop" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="border-top-home-tab" data-toggle="tab" href="#border-top-home" role="tab" aria-controls="border-top-home" aria-selected="true"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg> 
                Chaudière</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="border-top-profile-tab" data-toggle="tab" href="#border-top-profile" role="tab" aria-controls="border-top-profile" aria-selected="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg> 
                ISOLATION INTERIEUR</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="border-top-contact-tab" data-toggle="tab" href="#border-top-contact" role="tab" aria-controls="border-top-contact" aria-selected="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone"><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg> 
                DOUCHE</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="border-top-setting-tab" data-toggle="tab" href="#border-top-setting" role="tab" aria-controls="border-top-setting" aria-selected="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg> 
                POMPE A CHALEURE</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="border-top-end-tab" data-toggle="tab" href="#border-top-end" role="tab" aria-controls="border-top-setting" aria-selected="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg> 
                ITE</a>
            </li>
        </ul>
        <div class="tab-content" id="borderTopContent">
            <div class="tab-pane fade show active" id="border-top-home" role="tabpanel" aria-labelledby="border-top-home-tab">
                <table id="leads_product1_table" class="table table-hover non-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Ville</th>
                            <th>Code postal</th>
                            <th>Adresse</th>
                            <th>Téléphone</th>
                            <th>Créé par</th>
                            <th>Commentaires</th>
                            <th>Statut</th>
							<th>produit</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody style="">
                        @if(count($product) > 0) 
                        @foreach($product as $temp_product)
                            <tr style="background:{{$temp_product->status->color}}">
                                <td>{{$temp_product->id}}</td>
                                <td>{{$temp_product->Nom}}</td>
                                <td>{{$temp_product->lname}}</td>
                                <td>{{$temp_product->Ville}}</td>
                                <td>{{$temp_product->Codepostal}}</td>
                                <td>{{$temp_product->address}}</td>
                                <td>{{$temp_product->tel1}}</td>
                                <td>{{$temp_product->creator->name}}</td>
                                <td>{{count($temp_product->comments)}}</td>
                                <td>
                                    <div class="d-flex">
                                        <span class="label label-success">{{$temp_product->status->title}}</span>
                                    </div>
                                </td>
								<td>{{$temp_product->produit1}}</td>
                                <!-- <td class="text-center"><span class="shadow-none badge badge-danger">Closed</span></td> -->
                                <td>
                                   
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-dark btn-sm dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuReference1">
                                            <a class="dropdown-item" href="{{route('new_web.leads.detail', $temp_product->id)}}">Détails</a>
											@if(Auth()->user()->userRole->role_id != 5)
                                            <a class="dropdown-item" href="{{route('new_web.leads.edit', $temp_product->id)}}">Modifier</a>
                                            <a class="dropdown-item" href="{{route('new_web.leads.delete', $temp_product->id)}}">Supprimer</a>
                                            <!-- <a class="dropdown-item" href="#">Something else here</a> -->
                                        </div>
                                    </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

@stop 
@push('scripts')
<style>
    .table > tbody > tr > td {
        color: black !important;
        font-weight: 1000;
    }
</style>
<script>
var table1 = $('#leads_product1_table').DataTable( {
    // dom: '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
    // buttons: {
    //     buttons: [
    //         { extend: 'Copier', className: 'btn' },
    //         { extend: 'csv', className: 'btn' },
    //         { extend: 'excel', className: 'btn' },
    //         { extend: 'Imprimer', className: 'btn' }
    //     ]
    // },
    dom: 'Blfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
    "pagingType": "full_numbers",
    "oLanguage": {
        "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
        "sInfo": "Affichage page _PAGE_ de _PAGES_",
        "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
        "sSearchPlaceholder": "Recherche...",
        "sLengthMenu": "Résultats :  _MENU_",
    },
    "stripeClasses": [],
    "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
    "pageLength": 25 
});

$('a.toggle-vis').on( 'click', function (e) {
    e.preventDefault();
    // Get the column API object
    var column1 = table1.column( $(this).attr('data-column') );
    // Toggle the visibility
    column1.visible( ! column1.visible() );
});
</script>

<script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
<script src="{{asset('plugins/table/datatable/custom_miscellaneous.js')}}"></script>
@endpush