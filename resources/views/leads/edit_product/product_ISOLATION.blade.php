<div class="col-md-4 product_info">
        <div class="col-md-12">
            <h4>Product Info</h4>
        </div>
        <div class="form-group">
            <label for="Quelle" class="control-label thin-weight">Quelle est la partie à isoler ?</label>
            <select name="Quelle" id="Quelle" class="form-control" multiple required>
                <option value=""></option>
                <option value="Comble1">Comble aménageable</option>
                <option value="Comble2">Comble perdu</option>
                <option value="Rampants">Rampants Intérieurs</option>
                <option value="Mur1">Mur Pignon </option>
                <option value="Plafond1">Plafond de garage attenant</option>
                <option value="Plafond2">Plafond de sous-sol</option>
                <option value="Plafond3">Plafond de cave</option>
                <option value="BLANK">BLANK Sanitaire</option>
                <option value="Mur2">Mur Attenant</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Surface" class="control-label thin-weight">Surface Comble</label>
            <input type="text" name="Surface" id="Surface" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="Plafond" class="control-label thin-weight">Surface Plafond-Bas (sous-sol, cave, BLANK sanitaire) : Rappel : Il doit y avoir obligatoirement une pièce de vie au-dessus</label>
            <select name="Plafond" id="Plafond" class="form-control" required>
                <option value=""></option>
                <option value="Oui">Oui</option>
                <option value="Non">Non</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Surface_des" class="control-label thin-weight">Surface des murs</label>
            <input type="text" name="Surface_des" id="Surface_des" class="form-control" required>
        </div>
        
        <div class="form-group">
            <label for="comble" class="control-label thin-weight">Accessibilité comble</label>
            <select name="comble" id="comble" class="form-control" multiple="multiple" required>
                <option value=""></option>
                <option value="Trappe">Trappe intérieure (Comble)</option>
                <option value="Escalier">Escalier intérieur (Comble)</option>
                <option value="Ent">Entrée extérieure (Comble)</option>
                <option value="Détuilage">Détuilage (Comble)</option>
            </select>
        </div>
        <div class="form-group">
            <label for="isolation" class="control-label thin-weight">Sur quoi pose-t-on l'isolation ?</label>
            <select name="isolation" id="isolation" class="form-control" multiple="multiple" required>
                <option value=""></option>
                <option value="Solives">Solives/ Fermettes / poutres (Comble)</option>
                <option value="Plancher">Plancher (Comble)</option>
                <option value="Plafond1">Plafond Parpaings</option>
                <option value="Plafond2">Plafond Briques</option>
                <option value="Plafond3">Plafond Bois</option>
                <option value="Plafond4">Plafond hourdi</option>
            </select>
        </div>
        <div class="form-group">
            <label for="sous_sol" class="control-label thin-weight">Accessibilité sous-sol</label>
            <select name="sous_sol" id="sous_sol" class="form-control tagging form-small" multiple="multiple" required>
                <option value=""></option>
                <option value="Porte">Porte Garage (Garage en rez de chaussée)</option>
                <option value="Escalier">Escalier (Sous-sol, Cave)</option>
                <option value="trappe">Accès trappe extérieur BLANK Sanitaire (trappe 60x70cm min)</option>
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <h4>&nbsp</h4>
        </div>
        <div class="form-group">
            <label for="du_mur" class="control-label thin-weight">Matière du mur ?</label>
            <select name="du_mur" id="du_mur" class="form-control" required>
                <option value=""></option>
                <option value="Porte">Parpaing</option>
                <option value="Brique">Brique creuse</option>
                <option value="Béton">Béton</option>
                <option value="Autre">Autre</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Hauteur" class="control-label thin-weight">Hauteur de Détuilage</label>
            <input type="text" name="Hauteur" id="Hauteur" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="Hauteursous" class="control-label thin-weight">Hauteur sous plafond du BLANK sanitaire en cm</label>
            <input type="text" name="Hauteursous" id="Hauteursous" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="endroit" class="control-label thin-weight">L'endroit est-il Dégagé</label>
            <select name="endroit" id="endroit" class="form-control" required>
                <option value=""></option>
                <option value="Oui">Oui</option>
                <option value="mais">Non, mais le prospect s'engage à le dégager d'ici la date de rdv</option>
            </select>
        </div>
        <div class="form-group">
            <label for="ancienne" class="control-label thin-weight">Y a-t-il une ancienne isolation</label>
            <select name="ancienne" id="ancienne" class="form-control" multiple required>
                <option value=""></option>
                <option value="comble">Vieille de 5 ans ou + (comble)</option>
                <option value="plafond1">4 cm ou moins de poly ou autre (plafond)</option>
                <option value="plafond2">+ de 4 cm de poly ou autre (plafond)</option>
                <option value="Autres">Autres (précisez de quoi il s'agit dans ''commentaire concernant l'isolation'')</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Commentaire" class="control-label thin-weight">Commentaire(s) Isolation</label>
            <input type="text" name="Commentaire" id="Commentaire" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="Situation" class="control-label thin-weight">Situation</label>
            <select name="Situation" id="Situation" class="form-control" required>
                <option value=""></option>
                <option value="Prop">Propriétaire</option>
                <option value="Locataire">Locataire</option>
            </select>
        </div>
        <div class="form-group">
            <label for="category" class="control-label thin-weight">Catégorie</label>
            <select name="category" id="category" class="form-control" required>
                <option value=""></option>
                <option value="Grand">Grand Précaire</option>
                <option value="Précaire">Précaire (A)</option>
                <option value="Classique">Classique (C)</option>
            </select>
        </div>
    </div>
    <div class="col-md-4 product_info">
        <div class="col-md-12">
            <h4>&nbsp</h4>
        </div>
        
        <div class="form-group">
            <label for="Mode" class="control-label thin-weight">Mode de Chauffage</label>
            <select name="Mode" id="Mode" class="form-control" required>
                <option value=""></option>
                <option value="Electrici">Electricité</option>
                <option value="Gaz">Gaz</option>
                <option value="Bois">Bois</option>
                <option value="Fioul">Fioul</option>
                <option value="Pompe">Pompe à chaleur (électricité)</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Personne" class="control-label thin-weight">Nombre de Personne(s) déclarée(s) à cette adresse</label>
            <input type="text" name="Personne" id="Personne" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="Fiscal1" class="control-label thin-weight">Numéro Fiscal 1</label>
            <input type="text" name="Fiscal1" id="Fiscal1" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="RefAvis1" class="control-label thin-weight">Référence de l'avis 1</label>
            <input type="text" name="RefAvis1" id="RefAvis1" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="Fiscal2" class="control-label thin-weight">Numéro Fiscal 2</label>
            <input type="text" name="Fiscal2" id="Fiscal2" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="RefAvis2" class="control-label thin-weight">Référence de l'avis 2</label>
            <input type="text" name="RefAvis2" id="RefAvis2" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="Revenue" class="control-label thin-weight">Revenue fiscale de référence </label>
            <input type="text" name="Revenue" id="Revenue" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="Nom_de" class="control-label thin-weight">Nom de l'agent</label>
            <input type="text" name="Nom_de" id="Nom_de" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="Date_de" class="control-label thin-weight">Date de rdv</label>
            <input type="text" name="Date_de" id="Date_de" class="form-control" required>
        </div>
    </div>
