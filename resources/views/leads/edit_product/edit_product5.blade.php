@extends('leads.edit')
@section('edit_product')
<div class="col-md-4 product_info">
        <div class="col-md-12">
            <h4>Product Info</h4>
        </div>
	<div class="form-group">
            <label for="produit1" class="control-label thin-weight">Nom du produit</label>
            <input value="{{$lead->produit1}}" type="text" name="produit1" id="produit1" class="form-control"  />
        </div>
	

	
		<div class="form-group">
            <label for="Sur_quoi" class="control-label thin-weight">Sur quoi pose-t-on l'isolation ? *</label>
            <select name="Sur_quoi[]" id="Sur_quoi" class="form-control tagging form-small" multiple >
				<option value=""></option>
                <option value="Parpaing" {{ ($lead->Sur_quoi && in_array("Parpaing",json_decode($lead->Sur_quoi)))? "selected":""}} >Parpaing</option>
                <option value="Brique creuse" {{ ($lead->Sur_quoi && in_array("Brique creuse",json_decode($lead->Sur_quoi)))? "selected":""}}>Brique creuse</option>
                <option value="Beton" {{ ($lead->Sur_quoi && in_array("Beton",json_decode($lead->Sur_quoi)))? "selected":""}}>Beton</option>
                <option value="brique pleine" {{ ($lead->Sur_quoi && in_array("brique pleine",json_decode($lead->Sur_quoi)))? "selected":""}}>brique pleine</option>
                <option value="crepi" {{ ($lead->Sur_quoi && in_array("crepi",json_decode($lead->Sur_quoi)))? "selected":""}}>crepi</option>
                <option value="autres" {{ ($lead->Sur_quoi && in_array("autres",json_decode($lead->Sur_quoi)))? "selected":""}}>autres</option>
            </select>
        </div>
	
        
        <div class="form-group">
            <label for="Surface_Comble" class="control-label thin-weight">Surface Comble</label>
            <input value="{{$lead->Surface_Comble}}" type="text" name="Surface_Comble" id="Surface_Comble" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Configuration" class="control-label thin-weight">Configuration de la maison ? *</label>
            <select name="Configuration[]" id="Configuration" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Maison de ville" {{ ($lead->Configuration && in_array("Maison de ville",json_decode($lead->Configuration))) ? "selected":""}} >Maison de ville (avec au moins une courette)</option>
                <option value="Maison individuelle" {{ ($lead->Configuration && in_array("Maison individuelle",json_decode($lead->Configuration))) ? "selected":""}} >Maison individuelle</option>
                <option value="mitoyenne un seul coté" {{ ($lead->Configuration && in_array("mitoyenne un seul coté",json_decode($lead->Configuration))) ? "selected":""}} >mitoyenne d'un seul cote</option>
                <option value="mitoyenne des deux cotés" {{ ($lead->Configuration && in_array("mitoyenne des deux cotés",json_decode($lead->Configuration))) ? "selected":""}} >mitoyenne des deux cote</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Surface_des" class="control-label thin-weight">Surface des murs en m2 de la maison</label>
            <input value="{{$lead->Surface_des}}" type="text" name="Surface_des" id="Surface_des" class="form-control"  />
        </div>
        
        <div class="form-group">
            <label for="Maison" class="control-label thin-weight">Maison de plus de 2 ans</label>
            <select name="Maison" id="Maison" class="form-control" >
                <option value=""></option>
                <option value="Oui" {{$lead->Maison == "Oui" ? "selected":""}}>Oui</option>
                <option value="Non" {{$lead->Maison == "Non" ? "selected":""}}>Non</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Zone" class="control-label thin-weight">Zone</label>
            <select name="Zone" id="Zone" class="form-control" >
                <option value=""></option>
                <option value="Zone B2" {{$lead->Zone == "Zone B2" ? "selected":""}}>Zone B2</option>
                <option value="Zone C" {{$lead->Zone == "Zone C" ? "selected":""}}>Zone C</option>
                <option value="ACTION COEUR DE VILLE" {{$lead->Zone == "ACTION COEUR DE VILLE" ? "selected":""}}>ACTION COEUR DE VILLE</option>
                <option value="Autre" {{$lead->Zone == "Autre" ? "selected":""}}>Autre</option>
            </select>
        </div>
	
	
	<div class="form-group">
            <label for="Quelle" class="control-label thin-weight">Quelle isolation à jour *</label>
            <select name="Quelle[]" id="Quelle" class="form-control tagging form-small" multiple >
				<option value=""></option>
                <option value="Combles" {{ ($lead->Quelle && in_array("Combles",json_decode($lead->Quelle)))? "selected":""}} >Combles</option>
                <option value="Fenetre (double vitrage)" {{ ($lead->Quelle && in_array("Fenetre (double vitrage)",json_decode($lead->Quelle)))? "selected":""}}>Fenetre</option>
                <option value="Sous-sol" {{ ($lead->Quelle && in_array("Sous-sol",json_decode($lead->Quelle)))? "selected":""}}>Sous-sol</option>
            </select>
        </div>
	
        
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <h4>&nbsp</h4>
        </div>
        <div class="form-group">
            <label for="Commentaire" class="control-label thin-weight">Commentaire isolation</label>
            <textarea class="form-control" name ="Commentaire">{!! $lead->Commentaire !!}</textarea>
        </div>
        <div class="form-group">
            <label for="Mode_de" class="control-label thin-weight">Mode de Chauffage</label>
            <select name="Mode_de" id="Mode_de" class="form-control" >
                <option value=""></option>
                <option value="Electrique" {{$lead->Mode_de == "Electrique" ? "selected":""}}>Electricite</option>
                <option value="Gaz" {{$lead->Mode_de == "Gaz" ? "selected":""}}>Gaz</option>
                <option value="Fioul" {{$lead->Mode_de == "Fioul" ? "selected":""}}>Fioul</option>
                <option value="Bois" {{$lead->Mode_de == "Bois" ? "selected":""}}>Bois</option>
                <option value="PAC" {{$lead->Mode_de == "PAC" ? "selected":""}}>PAC</option>
                <option value="Poele a granulés" {{$lead->Mode_de == "Poele a granulés" ? "selected":""}}>Poele a granules</option>
                <option value="Autre" {{$lead->Mode_de == "Autre" ? "selected":""}}>Autre</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Situation_de" class="control-label thin-weight">Situation de maison</label>
            <select name="Situation_de" id="Situation_de" class="form-control" >
                <option value=""></option>
                <option value="Propriétaire occupant" {{$lead->Situation_de == "Propriétaire occupant" ? "selected":""}}>Proprietaire occupant</option>
                <option value="Locataire" {{$lead->Situation_de == "Locataire" ? "selected":""}}>Locataire</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Emploi" class="control-label thin-weight">Emploi salarie</label>
            <select name="Emploi" id="Emploi" class="form-control" >
                <option value=""></option>
                <option value="Secteur Privé" {{$lead->Emploi == "Secteur Privé" ? "selected":""}}>Secteur Prive</option>
                <option value="Secteur publique" {{$lead->Emploi == "Secteur publique" ? "selected":""}}>Secteur publique</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Metier" class="control-label thin-weight">Metier</label>
            <input value="{{ $lead->Metier }}" type="text" name="Metier" id="Metier" class="form-control"  />
        </div>

        <div class="form-group">
            <label for="quelques" class="control-label thin-weight">Il y a t'il quelques choses de fixés sur mur exterieur ?</label>
            <select name="quelques[]" id="quelques" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Pergolas" {{ ($lead->quelques && in_array("Pergolas",json_decode($lead->quelques)))? "selected":""}} >Pergolas</option>
                <option value="Appentis" {{ ($lead->quelques && in_array("Appentis",json_decode($lead->quelques)))? "selected":""}} >Appentis</option>
                <option value="Verandas" {{ ($lead->quelques && in_array("Verandas",json_decode($lead->quelques)))? "selected":""}} >Verandas</option>
                <option value="Autre" {{ ($lead->quelques && in_array("Autre",json_decode($lead->quelques)))? "selected":""}} >Autre</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Nombre" class="control-label thin-weight">Nombre de Personne(s) declaree(s) a cette adresse</label>
            <input value="{{$lead->Nombre}}" type="text" name="Nombre" id="Nombre" class="form-control"  />
        </div>
    </div>
    <div class="col-md-4">        
        <div class="form-group">
            <h4>&nbsp</h4>
        </div>
        
        <div class="form-group">
            <label for="Fiscal1" class="control-label thin-weight">Numero Fiscal 1</label>
            <input value="{{$lead->Fiscal1}}" type="text" name="Fiscal1" id="Fiscal1" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="RefAvis1" class="control-label thin-weight">Reference de l'avis 1</label>
            <input value="{{$lead->RefAvis1}}" type="text" name="RefAvis1" id="RefAvis1" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Fiscal2" class="control-label thin-weight">Numero Fiscal 2</label>
            <input value="{{$lead->Fiscal2}}" type="text" name="Fiscal2" id="Fiscal2" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="RefAvis2" class="control-label thin-weight">Reference de l'avis 2</label>
            <input value="{{$lead->RefAvis2}}" type="text" name="RefAvis2" id="RefAvis2" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Revenue" class="control-label thin-weight">Revenu fiscale de reference </label>
            <input value="{{$lead->Revenue}}" type="text" name="Revenue" id="Revenue" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Nom_de" class="control-label thin-weight">Nom de l'agent</label>
            <input value="{{$lead->Nom_de}}" type="text" name="Nom_de" id="Nom_de" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Date_de" class="control-label thin-weight">Date de rdv</label>
            <input value="{{$lead->Date_de}}" type="text" name="Date_de" id="Date_de" class="form-control"  />
        </div>
    </div>
    <!-- <script>
        var data = <?php echo $lead->Demander?>;
        for(i=0;i<6;i++){
            var children =  document.getElementById("Sur_quoi").children;
            var children_value = children[i].getAttribute("value");
            for(j=0;j<6;j++){
                if(children_value==data[j]){
                    alert(children_value);
                    create_attr = document.createAttribute("selected");
                    create_attr.value = true;
                    chilren[i].setAttributeNode(create_attr);
                }
            }
        }
    </script> -->
@endsection