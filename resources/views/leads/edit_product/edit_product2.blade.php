@extends('leads.edit')
@section('edit_product')
<div class="col-md-4 product_info">
        <div class="col-md-12">
            <h4>Product Info</h4>
        </div>
	<div class="form-group">
            <label for="produit1" class="control-label thin-weight">Nom du produit</label>
            <input value="{{$lead->produit1}}" type="text" name="produit1" id="produit1" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Quelle" class="control-label thin-weight">Quelle est la partie a isoler ?</label>
            <select name="Quelle[]" id="Quelle" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Comble aménageable" {{ ($lead->Quelle && in_array("Comble aménageable",json_decode($lead->Quelle))) ? "selected":""}}>Comble aménageable</option>
                <option value="Comble perdu" {{ ($lead->Quelle && in_array("Comble perdu",json_decode($lead->Quelle))) ? "selected":""}}>Comble perdu</option>
                <option value="Rampants Interieurs" {{ ($lead->Quelle && in_array("Rampants Interieurs",json_decode($lead->Quelle))) ? "selected":""}}>Rampants Interieurs</option>
                <option value="Mur Pignon" {{ ($lead->Quelle && in_array("Mur Pignon",json_decode($lead->Quelle))) ? "selected":""}}>Mur Pignon </option>
                <option value="Plafond de garage attenant" {{ ($lead->Quelle && in_array("Plafond de garage attenant",json_decode($lead->Quelle))) ? "selected":""}}>Plafond de garage attenant</option>
                <option value="Plafond de sous-sol" {{ ($lead->Quelle && in_array("Plafond de sous-sol",json_decode($lead->Quelle))) ? "selected":""}}>Plafond de sous-sol</option>
                <option value="Plafond de cave" {{ ($lead->Quelle && in_array("Plafond de cave",json_decode($lead->Quelle))) ? "selected":""}}>Plafond de cave</option>
                <option value="Vide Sanitaire" {{ ($lead->Quelle && in_array("Vide Sanitaire",json_decode($lead->Quelle))) ? "selected":""}}>Vide Sanitaire</option>
                <option value="Mur Attenant" {{ ($lead->Quelle && in_array("Mur Attenant",json_decode($lead->Quelle))) ? "selected":""}}>Mur Attenant</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Surface" class="control-label thin-weight">Surface Comble</label>
            <input value="{{ $lead->Surface }}" type="text" name="Surface" id="Surface" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Plafond" class="control-label thin-weight">Surface Plafond-Bas (sous-sol, cave, vide sanitaire) : Rappel : Il doit y avoir obligatoirement une piece de vie au-dessus</label>
			<input value="{{ $lead->Plafond }}" type="text" name="Plafond" id="Surface" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Surface_des" class="control-label thin-weight">Surface des murs</label>
            <input type="text" name="Surface_des" id="Surface_des" class="form-control" value="{{ $lead->Surface_des }}" >
        </div>
        
        <div class="form-group">
            <label for="comble" class="control-label thin-weight">Accessibilite comble</label>
            <select name="comble[]" id="comble" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Trappe intérieure (Comble)" {{ ($lead->comble && in_array("Trappe intérieure (Comble)",json_decode($lead->comble))) ? "selected":""}}>Trappe intérieure (Comble)</option>
                <option value="Escalier intérieur (Comble)" {{ ($lead->comble && in_array("Escalier intérieur (Comble)",json_decode($lead->comble))) ? "selected":""}}>Escalier intérieur (Comble)</option>
                <option value="Entrée extérieure (Comble)" {{ ($lead->comble && in_array("Entrée extérieure (Comble)",json_decode($lead->comble))) ? "selected":""}}>Entrée extérieure (Comble)</option>
                <option value="Détuilage (Comble)" {{ ($lead->comble && in_array("Détuilage (Comble)",json_decode($lead->comble))) ? "selected":""}}>Détuilage (Comble)</option>
            </select>
        </div>
        <div class="form-group">
            <label for="isolation" class="control-label thin-weight">Sur quoi pose-t-on l'isolation ?</label>
            <select name="isolation[]" id="isolation" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Solives/ Fermettes / poutres (Comble)" {{ ($lead->isolation && in_array("Solives/ Fermettes / poutres (Comble)",json_decode($lead->isolation))) ? "selected":""}}>Solives/ Fermettes / poutres (Comble)</option>
                <option value="Plancher (Comble)" {{ ($lead->isolation && in_array("Plancher (Comble)",json_decode($lead->isolation))) ? "selected":""}}>Plancher (Comble)</option>
                <option value="Plafond Parpaings" {{ ($lead->isolation && in_array("Plafond Parpaings",json_decode($lead->isolation))) ? "selected":""}}>Plafond Parpaings</option>
                <option value="Plafond Briques" {{ ($lead->isolation && in_array("Plafond Briques",json_decode($lead->isolation))) ? "selected":""}}>Plafond Briques</option>
                <option value="Plafond Bois" {{ ($lead->isolation && in_array("Plafond Bois",json_decode($lead->isolation))) ? "selected":""}}>Plafond Bois</option>
                <option value="Plafond hourdi" {{ ($lead->isolation && in_array("Plafond hourdi",json_decode($lead->isolation))) ? "selected":""}}>Plafond hourdi</option>
            </select>
        </div>
        <div class="form-group">
            <label for="sous_sol" class="control-label thin-weight">Accessibilite sous-sol</label>
            <select name="sous_sol[]" id="sous_sol" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Porte Garage" {{ ($lead->sous_sol && in_array("Porte Garage",json_decode($lead->sous_sol))) ? "selected":""}}>Porte Garage (Garage en rez de chaussee)</option>
                <option value="Escalier" {{ ($lead->sous_sol && in_array("Escalier",json_decode($lead->sous_sol))) ? "selected":""}}>Escalier (Sous-sol, Cave)</option>
                <option value="Accès trappe extérieur" {{ ($lead->sous_sol && in_array("Accès trappe extérieur",json_decode($lead->sous_sol)))? "selected":""}}>Acces trappe exterieur Vide Sanitaire (trappe 60x70cm min)</option>
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <h4>&nbsp</h4>
        </div>
        <div class="form-group">
            <label for="du_mur" class="control-label thin-weight">Matiere du mur ?</label>
            <select name="du_mur" id="du_mur" class="form-control" >
                <option value=""></option>
                <option value="Parpaing" {{$lead->du_mur == "Parpaing" ? "selected" : ""}}>Parpaing</option>
                <option value="Brique creuse" {{$lead->du_mur == "Brique creuse" ? "selected" : ""}}>Brique creuse</option>
                <option value="Béton" {{$lead->du_mur == "Béton" ? "selected" : ""}}>Beton</option>
                <option value="Autre" {{$lead->du_mur == "Autre" ? "selected" : ""}}>Autre</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Hauteur" class="control-label thin-weight">Hauteur de Detuilage</label>
            <input value="{{$lead->Hauteur}}" type="text" name="Hauteur" id="Hauteur" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Hauteursous" class="control-label thin-weight">Hauteur sous plafond du Vide sanitaire en cm</label>
            <input value="{{$lead->Hauteursous}}" type="text" name="Hauteursous" id="Hauteursous" class="form-control" >
        </div>
        <div class="form-group">
            <label for="endroit" class="control-label thin-weight">L'endroit est-il Degage</label>
            <select name="endroit" id="endroit" class="form-control" >
                <option value=""></option>
                <option value="Oui" {{$lead->endroit == "Oui" ? "selected" : ""}}>Oui</option>
                <option value="Non" {{$lead->endroit == "Non" ? "selected" : ""}}>Non, mais le prospect s'engage a le degager d'ici la date de rdv</option>
            </select>
        </div>
        <div class="form-group">
            <label for="ancienne" class="control-label thin-weight">Y a-t-il une ancienne isolation</label>
            <select name="ancienne[]" id="ancienne" class="form-control tagging form-small" multiple >
                <option value=""></option>
                <option value="Vieille de 5 ans ou + (comble)" {{ ($lead->ancienne && in_array("Vieille de 5 ans ou + (comble)",json_decode($lead->ancienne))) ? "selected":""}}>Vieille de 5 ans ou + (comble)</option>
                <option value="4 cm ou moins de poly ou autre (plafond)" {{ ($lead->ancienne && in_array("4 cm ou moins de poly ou autre (plafond)",json_decode($lead->ancienne))) ? "selected":""}}>4 cm ou moins de poly ou autre (plafond)</option>
                <option value="+ de 4 cm de poly ou autre (plafond)" {{ ($lead->ancienne && in_array("+ de 4 cm de poly ou autre (plafond)",json_decode($lead->ancienne))) ? "selected":""}}>+ de 4 cm de poly ou autre (plafond)</option>
                <option value="Autres" {{ ($lead->ancienne && in_array("Autres",json_decode($lead->ancienne))) ? "selected":""}}>Autres (precisez de quoi il s'agit dans ''commentaire concernant l'isolation'')</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Commentaire" class="control-label thin-weight">Commentaire(s) Isolation</label>
            <input value="{{ $lead->Commentaire }}" type="text" name="Commentaire" id="Commentaire" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Situation" class="control-label thin-weight">Situation</label>
            <select name="Situation" id="Situation" class="form-control" >
                <option value=""></option>
                <option value="Propriétaire" {{ $lead->Situation == "Propriétaire"? "selected":""}}>Propriétaire</option>
                <option value="Locataire" {{ $lead->Situation == "Locataire"? "selected":""}}>Locataire</option>
            </select>
        </div>
        <div class="form-group">
            <label for="category" class="control-label thin-weight">Categorie</label>
            <select name="category" id="category" class="form-control" >
                <option value=""></option>
                <option value="Grand Précaire" {{ $lead->category == "Grand Précaire" ? "selected" : ""}}>Grand Précaire</option>
                <option value="Précaire (A)" {{ $lead->category == "Précaire (A)" ? "selected" : ""}}>Précaire (A)</option>
                <option value="Classique (C)" {{ $lead->category == "Classique (C)" ? "selected" : ""}}>Classique (C)</option>
            </select>
        </div>
    </div>
    <div class="col-md-4 product_info">
        <div class="col-md-12">
            <h4>&nbsp</h4>
        </div>
        
        <div class="form-group">
            <label for="Mode" class="control-label thin-weight">Mode de Chauffage</label>
            <select name="Mode" id="Mode" class="form-control" >
                <option value=""></option>
                <option value="Electricité" {{$lead->Mode == "Electricité" ? "selected":""}}>Electricité</option>
                <option value="Gaz" {{$lead->Mode == "Gaz" ? "selected":""}}>Gaz</option>
                <option value="Bois" {{$lead->Mode == "Bois" ? "selected":""}}>Bois</option>
                <option value="Fioul" {{$lead->Mode == "Fioul" ? "selected":""}}>Fioul</option>
                <option value="Pompe à chaleur (électricité)" {{$lead->Mode == "Pompe à chaleur (électricité)" ? "selected":""}}>Pompe à chaleur (électricité)</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Personne" class="control-label thin-weight">Nombre de Personne(s) declaree(s) a cette adresse</label>
            <input value="{{$lead->Personne}}" type="text" name="Personne" id="Personne" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Fiscal1" class="control-label thin-weight">Numero Fiscal 1</label>
            <input value="{{$lead->Fiscal1}}" type="text" name="Fiscal1" id="Fiscal1" class="form-control" >
        </div>
        <div class="form-group">
            <label for="RefAvis1" class="control-label thin-weight">Reference de l'avis 1</label>
            <input value="{{$lead->RefAvis1}}" type="text" name="RefAvis1" id="RefAvis1" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Fiscal2" class="control-label thin-weight">Numero Fiscal 2</label>
            <input value="{{$lead->Fiscal2}}" type="text" name="Fiscal2" id="Fiscal2" class="form-control" >
        </div>
        <div class="form-group">
            <label for="RefAvis2" class="control-label thin-weight">Reference de l'avis 2</label>
            <input value="{{$lead->RefAvis2}}" type="text" name="RefAvis2" id="RefAvis2" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Revenue" class="control-label thin-weight">Revenu fiscale de reference </label>
            <input value="{{$lead->Revenue}}" type="text" name="Revenue" id="Revenue" class="form-control" >
        </div>

        <div class="form-group">
            <label for="Date_de" class="control-label thin-weight">Date de rdv</label>
            <input value="{{$lead->Date_de}}" type="text" name="Date_de" id="Date_de" class="form-control" >
        </div>
    </div>

@endsection