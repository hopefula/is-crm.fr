<div class="col-md-4">
    <div class="form-group">
        <h4>Informations du client</h4>
    </div>
    <div class="form-group">
        <label for="Nom" class="control-label thin-weight">Nom</label>
        <input value="{{$lead->Nom}}" type="text" name="Nom" id="Nom" class="form-control">
    </div>
    <div class="form-group">
        <label for="lname" class="control-label thin-weight">Prénom</label>
        <input value="{{$lead->lname}}" type="text" name="lname" id="lname" class="form-control">
    </div>
        <div class="form-group">
        <label for="address" class="control-label thin-weight">Adresse</label>
        <input value="{{$lead->address}}" type="text" name="address" id="address" class="form-control">
    </div>
    <div class="form-group">
        <label for="Ville" class="control-label thin-weight">Ville</label>
        <input value="{{$lead->Ville}}" type="text" name="Ville" id="Ville" class="form-control">
    </div>
    <div class="form-group">
        <label for="Codepostal" class="control-label thin-weight">Code postal</label>
        <input value="{{$lead->Codepostal}}" type="text" name="Codepostal" id="Codepostal" class="form-control">
    </div>

</div>
<div class="col-md-4">   
    <div class="form-group">
        <h4>&nbsp</h4>
    </div> 
        <div class="form-group">
        <label for="tel1" class="control-label thin-weight">Téléphone Principale</label>
        <input value="{{$lead->tel1}}" type="text" name="tel1" id="name" class="form-control">
    </div>
        <div class="form-group">
        <label for="tel2" class="control-label thin-weight">Téléphone secondaire</label>
        <input value="{{$lead->tel2}}" type="text" name="tel2" id="tel2" class="form-control">
    </div>
    <div class="form-group">
        <label for="mobile1" class="control-label thin-weight">Téléphone mobile 1</label>
        <input value="{{$lead->mobile1}}" type="text" name="mobile1" id="mobile1" class="form-control">
    </div>
    <div class="form-group">
        <label for="mobile2" class="control-label thin-weight">Téléphone mobile 2</label>
        <input value="{{$lead->mobile2}}" type="text" name="mobile2" id="mobile2" class="form-control">
    </div>
    <div class="form-group">
        <label for="email" class="control-label thin-weight">E-mail</label>
        <input value="{{$lead->email}}" type="email" name="email" id="email" class="form-control">
    </div>
    
</div>
<div class="col-md-4">    
    <div class="form-group">
        <h4>&nbsp</h4>
    </div> 
    
        
        <div class="form-group">
        <label for="description" class="control-label thin-weight">Commentaire client</label>
        <textarea name="description" id="description" cols="50" rows="5" class="form-control">{!! $lead->description !!}</textarea>
    </div>
    <div class="form-group">
        <label for="deadline" class="control-label thin-weight">@lang('Status')</label>
        <select name="status_id" id="status" class="form-control">
            @foreach($statuses as $status => $statusK)
                <option value="{{$status}}" @if($lead->status_id == $status) selected @endif>{{$statusK}}</option>
            @endforeach
        </select>
    </div>
	<div class="form-group">
        <label for="Campagne" class="control-label thin-weight">Campagne</label>
        <select name="Campagne" id="Campagne" class="form-control">
            <option value=""></option>
            <option value="Fichier brut" @if($lead->Campagne == "Fichier brut") selected @endif>Fichier brut</option>
            <option value="Leads" @if($lead->Campagne == "Leads") selected @endif>Leads</option>
        </select>
    </div>
    
</div>