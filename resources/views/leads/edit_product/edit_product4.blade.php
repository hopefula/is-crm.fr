@extends('leads.edit')
@section('edit_product')
    
<div class="col-md-4 product_info">
        <div class="form-group">
            <h4>Informations du produit</h4>
        </div>
	<div class="form-group">
            <label for="produit1" class="control-label thin-weight">Nom du produit</label>
            <input value="{{$lead->produit1}}" type="text" name="produit1" id="produit1" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Surface" class="control-label thin-weight">Surface habitable en m2 de la maison</label>
            <input type="text" name="Surface" id="Surface" class="form-control" value ="{{ $lead->Surface}}"  />
        </div>
        <div class="form-group">
            <label for="Zone" class="control-label thin-weight">Zone</label>
            <select name="Zone" id="Zone" class="form-control" >
                <option value=""></option>
                <option value="Zone B2" {{$lead->Zone == "Zone B2" ? "selected" : ""}}>Zone B2</option>
                <option value="Zone C" {{$lead->Zone == "Zone C" ? "selected" : ""}}>Zone C</option>
                <option value="ACTION COEUR DE VILLE" {{$lead->Zone == "ACTION COEUR DE VILLE" ? "selected" : ""}}>ACTION COEUR DE VILLE</option>
                <option value="Autre" {{$lead->Zone == "Autre" ? "selected" : ""}}>Autre</option>
            </select>
        </div>

        <div class="form-group">
            <label for="Quelle" class="control-label thin-weight">Quelle isolation à jour *</label>
            <select name="Quelle[]" id="Quelle" class="form-control tagging form-small" multiple >
				<option value=""></option>
                <option value="Combles" {{ ($lead->Quelle && in_array("Combles",json_decode($lead->Quelle)))? "selected":""}} >Combles</option>
                <option value="Fenetre (double vitrage)" {{ ($lead->Quelle && in_array("Fenetre (double vitrage)",json_decode($lead->Quelle)))? "selected":""}}>Fenetre</option>
                <option value="Sous-sol" {{ ($lead->Quelle && in_array("Sous-sol",json_decode($lead->Quelle)))? "selected":""}}>Sous-sol</option>
            </select>
        </div>
	
	
        <div class="form-group">
            <label for="Commentaire" class="control-label thin-weight">Commentaire isolation</label>
            <input type="text" name="Commentaire" id="Commentaire" class="form-control" value = "{{$lead->Commentaire}}"  />
        </div>
        <div class="form-group">
            <label for="Mode_de" class="control-label thin-weight">Mode de Chauffage</label>
            <select name="Mode_de" id="Mode_de" class="form-control" >
                <option value=""></option>
                <option value="Electricité" {{$lead->Mode_de == "Electricité" ? "selected" : ""}}>Electricité</option>
                <option value="Gaz" {{$lead->Mode_de == "Gaz" ? "selected" : ""}}>Gaz</option>
                <option value="Fioul" {{$lead->Mode_de == "Fioul" ? "selected" : ""}}>Fioul</option>
                <option value="Bois" {{$lead->Mode_de == "Bois" ? "selected" : ""}}>Bois</option>
                <option value="PAC" {{$lead->Mode_de == "PAC" ? "selected" : ""}}>PAC</option>
                <option value="Poêle à granulés" {{$lead->Mode_de == "Poêle à granulés" ? "selected" : ""}}>Poêle à granulés</option>
                <option value="Autre" {{$lead->Mode_de == "Autre" ? "selected" : ""}}>Autre</option>
            </select>
        </div>
    </div>
    <div class="col-md-4 product_info">
        <div class="form-group">
            <label for="Situation" class="control-label thin-weight">Situation de maison</label>
            <select name="Situation" id="Situation" class="form-control" >
                <option value=""></option>
                <option value="Propriétaire occupant" {{$lead->Situation == "Propriétaire occupant" ? "selected":""}}>Propriétaire occupant</option>
                <option value="Locataire" {{$lead->Situation == "Locataire" ? "selected":""}}>Locataire</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Emploi" class="control-label thin-weight">Emploi salarié</label>
            <select name="Emploi" id="Emploi" class="form-control" >
                <option value=""></option>
                <option value="Secteur Privé" {{$lead->Emploi == "Secteur Privé" ? "selected":""}}>Secteur Privé</option>
                <option value="Secteur publique" {{$lead->Emploi == "Secteur publique" ? "selected":""}}>Secteur publique</option>
            </select>
        </div>
        
        <div class="form-group">
            <label for="Metier" class="control-label thin-weight">Métier</label>
            <input value="{{ $lead->Metier }}" type="text" name="Metier" id="Metier" class="form-control"  />
        </div>
    
        <div class="form-group">
            <label for="Personne" class="control-label thin-weight">Nombre de Personne(s) déclarée(s) à cette adresse</label>
            <input value="{{$lead->Personne}}" type="text" name="Personne" id="Personne" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="Fiscal1" class="control-label thin-weight">Numéro Fiscal 1</label>
            <input value="{{$lead->Fiscal1}}" type="text" name="Fiscal1" id="Fiscal1" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="RefAvis1" class="control-label thin-weight">Référence de l'avis 1</label>
            <input value="{{$lead->RefAvis1}}" type="text" name="RefAvis1" id="RefAvis1" class="form-control"  />
        </div>
    </div>
    <div class="col-md-4 product_info">
        <div class="form-group">
            <label for="Fiscal2" class="control-label thin-weight">Numéro Fiscal 2</label>
            <input value="{{$lead->Fiscal2}}" type="text" name="Fiscal2" id="Fiscal2" class="form-control"  />
        </div>
        <div class="form-group">
            <label for="RefAvis2" class="control-label thin-weight">Référence de l'avis 2</label>
            <input value="{{$lead->RefAvis2}}" type="text" name="RefAvis2" id="RefAvis2" class="form-control"  />
        </div>

        <div class="form-group">
            <label for="Revenue" class="control-label thin-weight">Revenue fiscale de référence </label>
            <input value="{{$lead->Revenue}}" type="text" name="Revenue" id="Revenue" class="form-control"  />
        </div>


        <div class="form-group">
            <label for="Revenue" class="control-label thin-weight">Date de rdv</label>
            <input value="{{$lead->Revenue}}" type="text" name="Revenue" id="Revenue" class="form-control"  />
        </div>
    </div>
    
@endsection    