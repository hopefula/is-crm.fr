@extends('leads.edit')
@section('edit_product')

<div class="col-md-4">
    <div class="form-group">
        <h4>Informations du produit</h4>
    </div>
	<div class="form-group">
            <label for="produit1" class="control-label thin-weight">Nom du produit</label>
            <input value="{{$lead->produit1}}" type="text" name="produit1" id="produit1" class="form-control"  />
        </div>
    <div class="form-group">
        <label for="Demander" class="control-label thin-weight">Demander impérativement l'ancienne marque de la chaudière murale</label>
        <input value="{{$lead->Demander}}" type="text" name="Demander" id="Demander" class="form-control">
    </div>
    <div class="form-group">
        <label for="Surface" class="control-label thin-weight">Surface habitable en m2 de la maison ?</label>
        <input value="{{$lead->Surface}}" type="text" name="Surface" id="Surface" class="form-control">
    </div>
    <div class="form-group">
        <label for="Maison" class="control-label thin-weight">Maison de plus de 2 ans</label>
        <select name="Maison" id="Maison" class="form-control">
            <option value=""></option>
            <option value="Oui" @if($lead->Maison == "Oui") selected @endif>Oui</option>
            <option value="Non" @if($lead->Maison == "Non") selected @endif>Non</option>
        </select>
    </div>
    <div class="form-group">
        <label for="Zone" class="control-label thin-weight">Zone</label>
        <select name="Zone" id="Zone" class="form-control">
            <option value=""></option>
            <option value="B2"  @if($lead->Zone == "B2") selected @endif>B2</option>
            <option value="C"  @if($lead->Zone == "C") selected @endif>C</option>
            <option value="Action" @if($lead->Zone == "Action") selected @endif>Action cœur de ville</option>
            <option value="Autres" @if($lead->Zone == "Autres") selected @endif>Autres</option>
        </select>
    </div>
    <div class="form-group">
            <label for="Quelle" class="control-label thin-weight">Quelle isolation à jour *</label>
            <select name="Quelle[]" id="Quelle" class="form-control tagging form-small" multiple >
				<option value=""></option>
                <option value="Combles" {{ ($lead->Quelle && in_array("Combles",json_decode($lead->Quelle)))? "selected":""}} >Combles</option>
                <option value="Fenetre (double vitrage)" {{ ($lead->Quelle && in_array("Fenetre (double vitrage)",json_decode($lead->Quelle)))? "selected":""}}>Fenetre</option>
                <option value="Sous-sol" {{ ($lead->Quelle && in_array("Sous-sol",json_decode($lead->Quelle)))? "selected":""}}>Sous-sol</option>
            </select>
        </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <h4>&nbsp</h4>
    </div>
    <div class="form-group">
        <label for="Commentaire" class="control-label thin-weight">Commentaire pour la pose</label>
        <input value="{{$lead->Commentaire}}" type="text" name="Commentaire" id="Commentaire" class="form-control">
    </div>
    <div class="form-group">
        <label for="Mode" class="control-label thin-weight">Mode de Chauffage</label>
        <select name="Mode" id="Mode" class="form-control">
            <option value=""></option>
            <option value="GAZ" @if($lead->Mode == "GAZ") selected @endif>GAZ</option>
            <option value="AUTRES" @if($lead->Mode == "AUTRES") selected @endif>AUTRES</option>
        </select>
    </div>
    <div class="form-group">
        <label for="Situation" class="control-label thin-weight">Situation</label>
        <select name="Situation" id="Situation" class="form-control">
            <option value=""></option>
            <option value="Prop" @if($lead->Situation == "Prop") selected @endif>Propriétaire occupant</option>
            <option value="Locataire" @if($lead->Situation == "Locataire") selected @endif>Locataire</option>
        </select>
    </div>

    <div class="form-group">
        <label for="Emploi" class="control-label thin-weight">Emploi salarié une personne dans la famille qui travaille dans le secteur privée </label>
        <select name="Emploi" id="Emploi" class="form-control">
            <option value=""></option>
            <option value="priv" @if($lead->Emploi == "priv") selected @endif>Secteur privé</option>
            <option value="publique" @if($lead->Emploi == "publique") selected @endif>Secteur publique</option>
        </select>
    </div>
    <div class="form-group">
        <label for="Quel" class="control-label thin-weight">Quel métier?</label>
        <input value="{{$lead->Quel}}" type="text" name="Quel" id="Quel" class="form-control">
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <h4>&nbsp</h4>
    </div>
    <div class="form-group">
        <label for="Nombre" class="control-label thin-weight">Nombre de Personne(s) déclarée(s) à cette adresse</label>
        <input value="{{$lead->Nombre}}" type="text" name="Nombre" id="Nombre" class="form-control">
    </div>
    <div class="form-group">
        <label for="Fiscal1" class="control-label thin-weight">Numéro Fiscal 1</label>
        <input value="{{$lead->Fiscal1}}" type="text" name="Fiscal1" id="Fiscal1" class="form-control">
    </div>
    <div class="form-group">
        <label for="RefAvis1" class="control-label thin-weight">Référence de l'avis 1</label>
        <input value="{{$lead->RefAvis1}}" type="text" name="RefAvis1" id="RefAvis1" class="form-control">
    </div>
    <div class="form-group">
        <label for="Fiscal2" class="control-label thin-weight">Numéro Fiscal 2</label>
        <input value="{{$lead->Fiscal2}}" type="text" name="Fiscal2" id="Fiscal2" class="form-control">
    </div>
    <div class="form-group">
        <label for="RefAvis2" class="control-label thin-weight">Référence de l'avis 2</label>
        <input value="{{$lead->RefAvis2}}" type="text" name="RefAvis2" id="RefAvis2" class="form-control">
    </div>
    <div class="form-group">
        <label for="Revenue" class="control-label thin-weight">Revenu fiscale de référence </label>
        <input value="{{$lead->Revenue}}" type="text" name="Revenue" id="Revenue" class="form-control">
    </div>
</div>
        
@endsection