<div class="form-group col-md-12">
    <h4>Informations du RDV</h4>
</div>
<div class="col-md-12 row">
    <div class="form-group col-md-4">
        <label for="lname" class="control-label thin-weight">Date de Création</label>
        <input type="date" name="app_created_date" id="app_created_date" class="form-control"  />
    </div>
    <div class="form-group col-md-4">
        <label for="lname" class="control-label thin-weight">Date de pré-visite</label>
        <input type="date" name="app_confirm_date" id="app_confirm_date" class="form-control"  />
    </div>
    <div class="form-group col-md-4">
        <label for="lname" class="control-label thin-weight">Date d'installation</label>
        <input type="date" name="app_install_date" id="app_install_date" class="form-control"  />
    </div>
</div>
<div class="form-group col-md-12">
    <input type="submit" class="btn btn-primary mb-2" id="createTask" value="{{__('créé le prospect')}}">
</div>

