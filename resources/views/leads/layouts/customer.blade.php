    <div class="col-md-4">
        <div class="form-group">
            <h4>Informations du client</h4>
        </div>
        <div class="form-group">
            <label for="Nom" class="control-label thin-weight">Nom</label>
            <input type="text" name="Nom" id="Nom" class="form-control" >
        </div>
        <div class="form-group">
            <label for="lname" class="control-label thin-weight">Prénom</label>
            <input type="text" name="lname" id="lname" class="form-control" >
        </div>
                <div class="form-group">
            <label for="address" class="control-label thin-weight">Adresse</label>
            <input type="text" name="address" id="address" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Ville" class="control-label thin-weight">Ville</label>
            <input type="text" name="Ville" id="Ville" class="form-control" >
        </div>
        <div class="form-group">
            <label for="Codepostal" class="control-label thin-weight">Code postal</label>
            <input type="text" name="Codepostal" id="Codepostal" class="form-control" >
        </div>

    </div>
    <div class="col-md-4">
        <div class="form-group">
            <h4>&nbsp</h4>
        </div>
                <div class="form-group">
            <label for="tel1" class="control-label thin-weight">Téléphone Principale</label>
            <input type="text" name="tel1" id="name" class="form-control" >
        </div>
                <div class="form-group">
            <label for="tel2" class="control-label thin-weight">Téléphone secondaire</label>
            <input type="text" name="tel2" id="tel2" class="form-control" >
        </div>
                <div class="form-group">
            <label for="mobile1" class="control-label thin-weight">Téléphone mobile 1</label>
            <input type="text" name="mobile1" id="mobile1" class="form-control" >
        </div>
        <div class="form-group">
            <label for="mobile2" class="control-label thin-weight">Téléphone mobile 2</label>
            <input type="text" name="mobile2" id="mobile2" class="form-control" >
        </div>
        <div class="form-group">
            <label for="email" class="control-label thin-weight">E-mail</label>
            <input type="email" name="email" id="email" class="form-control" >
        </div>
    </div>
    <div class="col-md-4">
    <div class="form-group">
            <h4>&nbsp</h4>
        </div>
        <div class="form-group">
            <label for="deadline" class="control-label thin-weight">@lang('Statut')</label>
            <select name="status_id" id="status" class="form-control" >
                @foreach($statuses as $status => $statusK)
                    <option value="{{$status}}">{{$statusK}}</option>
                @endforeach
            </select>
        </div>
		<div class="form-group">
            <label for="Campagne" class="control-label thin-weight">Campagne</label>
            <select name="Campagne" id="Campagne" class="form-control" >
                <option value=""></option>
                <option value="Fichier brut">Fichier brut</option>
                <option value="Leads">Leads</option>
            </select>
        </div>
        
        <div class="form-group">
            <label for="description" class="control-label thin-weight">Commentaire client</label>
            <textarea name="description" id="description" cols="50" rows="5" class="form-control" ></textarea>
        </div>
    </div>

