@extends('leads.detail')
@section('detail_product')
<div class="col-md-4 product_info">
        <div class="form-group">
            <h4>Informations du produit</h4>
        </div>
	<div class="form-group">
        <label for="produit1" class="control-label thin-weight">Nom du produit</label>
        <p>{{$lead->produit1}}</p>
    </div>
        <div class="form-group">
            <label for="actuelle" class="control-label thin-weight">installation de la salle de bain actuelle</label>
            <p>{{ $lead->actuelle }}</p>
        </div>
        <div class="form-group">
            <label for="Situation" class="control-label thin-weight">Situation</label>
            <p>{{$lead->Situation}}</p>
        </div>
        <div class="form-group">
            <label for="Solutions" class="control-label thin-weight">Solutions adaptées aux Personnes à Mobilité Réduite</label>
            <p>{{$lead->Solutions}}</p>
        </div>
        <div class="form-group">
            <label for="agricole" class="control-label thin-weight">retraité d’une entreprise du secteur privé âgé, y compris agricole, de 70 ans et plus</label>
            <p>{{$lead->agricole}}</p>
        </div>
        <div class="form-group">
            <label for="niveau" class="control-label thin-weight">Agé de 60 ans et un plus en situation de perte d'autonomie avec in niveau GIR de 1 à 4</label>
            <p>{{$lead->niveau}}</p>
        </div>
        
    </div>
    <div class="col-md-4 product_info">
        <div class="form-group">
            <h4>&nbsp</h4>
        </div>
        <div class="form-group">
            <label for="descendant" class="control-label thin-weight">hébergé chez un descendant salarié d’une entreprise du secteur privé</label>
            <p>{{$lead->descendant}}</p>
        </div>
        <div class="form-group">
            <label for="Personne" class="control-label thin-weight">Nombre de Personne(s) déclarée(s) à cette adresse</label>
            <p>{{$lead->Personne}}</p>
        </div>
        <div class="form-group">
            <label for="Fiscal1" class="control-label thin-weight">Numéro Fiscal 1</label>
            <p>{{$lead->Fiscal1}}</p>
        </div>
        <div class="form-group">
            <label for="RefAvis1" class="control-label thin-weight">Référence de l'avis 1</label>
            <p>{{$lead->RefAvis1}}</p>
        </div>

        <div class="form-group">
            <label for="Fiscal2" class="control-label thin-weight">Numéro Fiscal 2</label>
            <p>{{$lead->Fiscal2}}</p>
        </div>
    </div>
    
    <div class="col-md-4 product_info">
        <div class="form-group">
            <h4>&nbsp</h4>
        </div>
        <div class="form-group">
            <label for="RefAvis2" class="control-label thin-weight">Référence de l'avis 2</label>
            <p>{{$lead->RefAvis2}}</p>
        </div>
        <div class="form-group">
            <label for="Revenue" class="control-label thin-weight">Revenu fiscale de référence </label>
            <p>{{$lead->Revenue}}</p>
        </div>
        <div class="form-group">
            <label for="Nom_de" class="control-label thin-weight">Nom de l'agent</label>
            <p>{{$lead->Nom_de}}</p>
        </div>
        <div class="form-group">
            <label for="Date_de" class="control-label thin-weight">Date de rdv</label>
            <p>{{$lead->Date_de}}</p>
        </div>
    </div>

@endsection