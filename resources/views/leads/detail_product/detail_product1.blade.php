@extends('leads.detail')
@section('detail_product')

<div class="col-md-4">
    <div class="form-group">
        <h4>Informations du produit</h4>
    </div>
	<div class="form-group">
        <label for="produit1" class="control-label thin-weight">Nom du produit</label>
        <p>{{$lead->produit1}}</p>
    </div>
    <div class="form-group">
        <label for="Demander" class="control-label thin-weight">Demander impérativement l'ancienne marque de la chaudière murale</label>
        <p>{{$lead->Demander}}</p>
    </div>
    <div class="form-group">
        <label for="Surface" class="control-label thin-weight">Surface habitable en m2 de la maison ?</label>
        <p>{{$lead->Surface}}</p>
    </div>
    <div class="form-group">
        <label for="Maison" class="control-label thin-weight">Maison de plus de 2 ans</label>
        <select name="Maison" id="Maison" class="form-control" disabled>
            <option value=""></option>
            <option value="Oui" @if($lead->Maison == "Oui") selected @endif>Oui</option>
            <option value="Non" @if($lead->Maison == "Non") selected @endif>Non</option>
        </select>
    </div>
    <div class="form-group">
        <label for="Zone" class="control-label thin-weight">Zone</label>
        <select name="Zone" id="Zone" class="form-control" disabled>
            <option value=""></option>
            <option value="B2"  @if($lead->Zone == "B2") selected @endif>B2</option>
            <option value="C"  @if($lead->Zone == "C") selected @endif>C</option>
            <option value="Action" @if($lead->Zone == "Action") selected @endif>Action cœur de ville</option>
            <option value="Autres" @if($lead->Zone == "Autres") selected @endif>Autres</option>
        </select>
    </div>
    <div class="form-group">
        <label for="Quelle" class="control-label thin-weight">Quelle isolation est à jour ?</label>
        <p>{{$lead->Quelle}}</p>
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <h4>&nbsp</h4>
    </div>
    <div class="form-group">
        <label for="Commentaire" class="control-label thin-weight">Commentaire pour la pose</label>
        <p>{{$lead->Commentaire}}</p>
    </div>
    <div class="form-group">
        <label for="Mode" class="control-label thin-weight">Mode de Chauffage</label>
        <select name="Mode" id="Mode" class="form-control" disabled>
            <option value=""></option>
            <option value="GAZ" @if($lead->Mode == "GAZ") selected @endif>GAZ</option>
            <option value="AUTRES" @if($lead->Mode == "AUTRES") selected @endif>AUTRES</option>
        </select>
    </div>
    <div class="form-group">
        <label for="Situation" class="control-label thin-weight">Situation</label>
        <select name="Situation" id="Situation" class="form-control" disabled>
            <option value=""></option>
            <option value="Prop" @if($lead->Situation == "Prop") selected @endif>Propriétaire occupant</option>
            <option value="Locataire" @if($lead->Situation == "Locataire") selected @endif>Locataire</option>
        </select>
    </div>

    <div class="form-group">
        <label for="Emploi" class="control-label thin-weight">Emploi salarié une personne dans la famille qui travaille dans le secteur privée </label>
        <select name="Emploi" id="Emploi" class="form-control" disabled>
            <option value=""></option>
            <option value="priv" @if($lead->Emploi == "priv") selected @endif>Secteur privé</option>
            <option value="publique" @if($lead->Emploi == "publique") selected @endif>Secteur publique</option>
        </select>
    </div>
    <div class="form-group">
        <label for="Quel" class="control-label thin-weight">Quel métier?</label>
        <p>{{$lead->Quel}}</p>
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <h4>&nbsp</h4>
    </div>
    <div class="form-group">
        <label for="Nombre" class="control-label thin-weight">Nombre de Personne(s) déclarée(s) à cette adresse</label>
        <p>{{$lead->Nombre}}</p>
    </div>
    <div class="form-group">
        <label for="Fiscal1" class="control-label thin-weight">Numéro Fiscal 1</label>
        <p>{{$lead->Fiscal1}}</p>
    </div>
    <div class="form-group">
        <label for="RefAvis1" class="control-label thin-weight">Référence de l'avis 1</label>
        <p>{{$lead->RefAvis1}}</p>
    </div>
    <div class="form-group">
        <label for="Fiscal2" class="control-label thin-weight">Numéro Fiscal 2</label>
        <p>{{$lead->Fiscal2}}</p>
    </div>
    <div class="form-group">
        <label for="RefAvis2" class="control-label thin-weight">Référence de l'avis 2</label>
        <p>{{$lead->RefAvis2}}</p>
    </div>
    <div class="form-group">
        <label for="Revenue" class="control-label thin-weight">Revenu fiscale de référence </label>
        <p>{{$lead->Revenue}}</p>
    </div>
</div>
        
@endsection