<div class="col-md-4">
    <div class="form-group">
        <h4>Informations du client</h4>
    </div>
    <div class="form-group">
        <label for="Nom" class="control-label thin-weight">Nom</label>  : 
        <span>{{$lead->Nom}}</span>
    </div>
        <div class="form-group">
        <label for="lname" class="control-label thin-weight">Prénom</label>  : 
        <span>{{$lead->lname}}</sPan>
    </div>
    <div class="form-group">
        <label for="Ville" class="control-label thin-weight">Ville</label>  : 
        <span>{{$lead->Ville}}</span>
    </div>
    <div class="form-group">
        <label for="Codepostal" class="control-label thin-weight">Code postal</label>  : 
        <span>{{$lead->Codepostal}}</span>
    </div>
        <div class="form-group">
        <label for="address" class="control-label thin-weight">Adresse</label>  : 
        <span>{{$lead->address}}</span>
    </div>

</div>
<div class="col-md-4">   
    <div class="form-group">
        <h4>&nbsp</h4>
    </div> 
        <div class="form-group">
        <label for="tel1" class="control-label thin-weight">Téléphone Principale</label>  : 
        <span>{{$lead->tel1}}</span>
    </div>
    <div class="form-group">
        <label for="tel2" class="control-label thin-weight">Téléphone secondaire</label>  : 
        <span>{{$lead->tel2}}</span>
    </div>
    <div class="form-group">
        <label for="mobile1" class="control-label thin-weight">Téléphone mobile 1</label>  : 
        <span>{{$lead->mobile1}}</span>
    </div>
    <div class="form-group">
        <label for="mobile2" class="control-label thin-weight">Téléphone mobile 2</label>  : 
        <span>{{$lead->mobile2}}</span>
    </div>
        <div class="form-group">
        <label for="email" class="control-label thin-weight">E-mail</label>  : 
        <span>{{$lead->email}}</span>
    </div>

</div>
<div class="col-md-4">    
    <div class="form-group">
        <h4>&nbsp</h4>
    </div> 
    
        <div class="form-group">
        <label for="description" class="control-label thin-weight">Commentaire client</label>  : 
        <span>{!! $lead->description !!}</span>
    </div>
    
    <div class="form-group">
        <label for="deadline" class="control-label thin-weight">@lang('Statut')</label>  : 
        <select name="status_id" id="status" class="form-control" disabled>
            @foreach($statuses as $status => $statusK)
                <option value="{{$status}}" @if($lead->status_id == $status) selected @endif>{{$statusK}}</option>
            @endforeach
        </select>
    </div>
	<div class="form-group">
        <label for="Campagne" class="control-label thin-weight">Campagne</label>  : 
        <span>{!! $lead->Campagne !!}</span>
    </div>
    
</div>