@extends('leads.detail')
@section('detail_product')
<div class="col-md-4 product_info">
    <div class="col-md-12">
        <h4>Informations du produit</h4>
    </div>
	<div class="form-group">
        <label for="produit1" class="control-label thin-weight">Nom du produit</label>
        <p>{{$lead->produit1}}</p>
    </div>
    
	<div class="form-group">
        <label for="Sur_quoi" class="control-label thin-weight">Sur quoi pose-t-on l'isolation ?</label>
        <br>
        <?php $lead_Quelles = json_decode($lead->Sur_quoi, false); ?>
        @if(is_array( $lead_Quelles ))
            @foreach($lead_Quelles as $temp)
                <span>{{$temp}},</span>
            @endforeach
        @else
            
        @endif
    </div>
	
    <div class="form-group">
        <label for="Surface_Comble" class="control-label thin-weight">Surface Comble</label>
        <br>
        <p>{{ $lead->Surface_Comble }}<p>
    </div>
    <div class="form-group">
        <label for="Configuration" class="control-label thin-weight">Configuration de la maison ? *</label>
        <br>
        <?php $lead_Quelles = json_decode($lead->Configuration, false); ?>
        @if(is_array( $lead_Quelles ))
            @foreach($lead_Quelles as $temp)
                <span>{{$temp}},</span>
            @endforeach
        @else
            <p>{{ $lead_Quelle}}</p>
        @endif
    </div>
    <div class="form-group">
        <label for="Surface_des" class="control-label thin-weight">Surface des murs en m2 de la maison</label>
        <br>
        <p>{{ $lead->Surface_des }}<p>
    </div>
    
    <div class="form-group">
        <label for="Maison" class="control-label thin-weight">Maison de plus de 2 ans</label>
        <br>
        <p>{{ $lead->Maison }}<p>
    </div>
    <div class="form-group">
        <label for="Zone" class="control-label thin-weight">Zone</label>
        <br>
        <p>{{ $lead->Zone }}<p>
    </div>
</div>
<div class="col-md-4">
    <div class="col-md-12">
        <h4>&nbsp</h4>
    </div>
	
	
	<div class="form-group">
        <label for="Quelle" class="control-label thin-weight">Quelle isolation est à jour</label>
        <br>
        <?php $lead_Quelles = json_decode($lead->Quelle, false); ?>
        @if(is_array( $lead_Quelles ))
            @foreach($lead_Quelles as $temp)
                <span>{{$temp}},</span>
            @endforeach
        @else
            <p>{{ $lead_Quelle}}</p>
        @endif
    </div>
	
    
    <div class="form-group">
        <label for="Commentaire" class="control-label thin-weight">Commentaire isolation</label>
        <br>
        <p>{{ $lead->Commentaire }}<p>
    </div>
    <div class="form-group">
        <label for="Mode_de" class="control-label thin-weight">Mode de Chauffage</label>
        <br>
        <p>{{ $lead->Mode_de }}<p>
    </div>
    <div class="form-group">
        <label for="Situation_de" class="control-label thin-weight">Situation de maison</label>
        <br>
        <p>{{ $lead->Situation_de }}<p>
    </div>
    <div class="form-group">
        <label for="Emploi" class="control-label thin-weight">Emploi salarié</label>
        <br>
        <p>{{ $lead->Emploi }}<p>
    </div>
    <div class="form-group">
        <label for="Métier" class="control-label thin-weight">Métier</label>
        <br>
        <p>{{ $lead->Metier }}<p>
    </div>

    <div class="form-group">
        <label for="quelques" class="control-label thin-weight">Il y a t'il quelques choses de fixés sur mur extérieur ?</label>
        <br>
        <?php $lead_Quelles = json_decode($lead->quelques, false); ?>
        @if(is_array( $lead_Quelles ))
            @foreach($lead_Quelles as $temp)
                <span>{{$temp}},</span>
            @endforeach
        @else
            <p>{{ $lead_Quelle}}</p>
        @endif
    </div>
</div>
<div class="col-md-4">        
    <div class="form-group">
        <h4>&nbsp</h4>
    </div>
    <div class="form-group">
        <label for="Nombre" class="control-label thin-weight">Nombre de Personne(s) déclarée(s) à cette adresse</label>
        <br>
        <p>{{ $lead->Nombre }}<p>
    </div>
    <div class="form-group">
        <label for="Fiscal1" class="control-label thin-weight">Numéro Fiscal 1</label>
        <br>
        <p>{{ $lead->Fiscal1 }}<p>
    </div>
    <div class="form-group">
        <label for="RefAvis1" class="control-label thin-weight">Référence de l'avis 1</label>
        <br>
        <p>{{ $lead->RefAvis1 }}<p>
    </div>
    <div class="form-group">
        <label for="Fiscal2" class="control-label thin-weight">Numéro Fiscal 2</label>
        <br>
        <p>{{ $lead->Fiscal2 }}<p>
    </div>
    <div class="form-group">
        <label for="RefAvis2" class="control-label thin-weight">Référence de l'avis 2</label>
        <br>
        <p>{{ $lead->RefAvis2 }}<p>
    </div>
    <div class="form-group">
        <label for="Revenue" class="control-label thin-weight">Revenu fiscale de référence </label>
        <br>
        <p>{{ $lead->Revenue }}<p>
    </div>
    <div class="form-group">
        <label for="Nom_de" class="control-label thin-weight">Nom de l'agent</label>
        <br>
        <p>{{ $lead->Nom_de }}<p>
    </div>
    <div class="form-group">
        <label for="Date_de" class="control-label thin-weight">Date de rdv</label>
        <br>
        <p>{{ $lead->Date_de }}<p>
    </div>
</div>
@endsection