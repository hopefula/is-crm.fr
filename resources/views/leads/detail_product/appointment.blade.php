<div class="form-group col-md-12">
    <h4>Informations du RDV</h4>
</div>
<div class="col-md-12 row">
    <div class="form-group col-md-4">
        <label for="lname" class="control-label thin-weight">Date de Création</label>
        <p>{{$lead->app_created_date}}</p>
    </div>
    <div class="form-group col-md-4">
        <label for="lname" class="control-label thin-weight">Date de pré-visite</label>
        <p>{{$lead->app_confirm_date}}</p>
    </div>
    <div class="form-group col-md-4">
        <label for="lname" class="control-label thin-weight">Date d'installation</label>
        <p>{{$lead->app_install_date}}</p>
    </div>
</div>
