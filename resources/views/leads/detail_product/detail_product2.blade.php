@extends('leads.detail')
@section('detail_product')
<div class="col-md-4 product_info">
        <div class="col-md-12">
            <h4>Informations du produit</h4>
        </div>
	<div class="form-group">
        <label for="produit1" class="control-label thin-weight">Nom du produit</label>
        <p>{{$lead->produit1}}</p>
    </div>
        <div class="form-group">
            <label for="Quelle" class="control-label thin-weight">Quelle est la partie à isoler ?</label><br>
            <?php $lead_Quelles = json_decode($lead->Quelle, false); ?>
            @if(is_array( $lead_Quelles ))
                @foreach($lead_Quelles as $temp)
                    <span>{{$temp}},</span>
                @endforeach
            @else
                <p>{{$lead_Quelle}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="Surface" class="control-label thin-weight">Surface Comble</label>
            <p>{{ $lead->Surface }}</p>
        </div>
        <div class="form-group">
            <label for="Plafond" class="control-label thin-weight">Surface Plafond-Bas (sous-sol, cave, vide sanitaire) : Rappel : Il doit y avoir obligatoirement une pièce de vie au-dessus</label>
            <p>{{$lead->Plafond}}</p>
        </div>
        <div class="form-group">
            <label for="Surface_des" class="control-label thin-weight">Surface des murs</label>
            <p>{{$lead->Surface_des}}</p>
        </div>
        
        <div class="form-group">
            <label for="comble" class="control-label thin-weight">Accessibilité comble</label><br>
            @if(is_array($lead_comble))
                @foreach($lead_comble as $temp)
                    <span>{{$temp}}, </span>
                @endforeach
            @else
                <p>{{$lead_comble}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="isolation" class="control-label thin-weight">Sur quoi pose-t-on l'isolation ?</label><br>
            @if(is_array($lead_isolation))
                @foreach($lead_isolation as $temp)
                    <span>{{$temp}}, </span>
                @endforeach
            @else
                <p>{{$lead_isolation}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="sous_sol" class="control-label thin-weight">Accessibilité sous-sol</label><br>
            @if(is_array($lead_sous_sol))
                @foreach($lead_sous_sol as $temp)
                    <span>{{$temp}}, </span>
                @endforeach
            @else
                <p>{{$lead_sous_sol}}</p>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <h4>&nbsp</h4>
        </div>
        <div class="form-group">
            <label for="du_mur" class="control-label thin-weight">Matière du mur ?</label>
            <p>{{$lead->du_mur}}</p>
        </div>
        <div class="form-group">
            <label for="Hauteur" class="control-label thin-weight">Hauteur de Détuilage</label>
            <p>{{$lead->Hauteur}}</p>
        </div>
        <div class="form-group">
            <label for="Hauteursous" class="control-label thin-weight">Hauteur sous plafond du Vide sanitaire en cm</label>
            <p>{{$lead->Hauteursous}}</p>
        </div>
        <div class="form-group">
            <label for="endroit" class="control-label thin-weight">L'endroit est-il Dégagé</label>
            <p>{{$lead->endroit}}</p>
        </div>
        <div class="form-group">
            <label for="ancienne" class="control-label thin-weight">Y a-t-il une ancienne isolation</label><br>
            @if(is_array($lead_ancienne))
                @foreach($lead_ancienne as $temp)
                    <span>{{$temp}}, </span>
                @endforeach
            @else
                <p>{{$lead_ancienne}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="Commentaire" class="control-label thin-weight">Commentaire(s) Isolation</label>
            <p>{{ $lead->Commentaire }}</p>
        </div>
        <div class="form-group">
            <label for="Situation" class="control-label thin-weight">Situation</label>
            <p>{{$lead->Situation}}</p>
        </div>
        <div class="form-group">
            <label for="category" class="control-label thin-weight">Catégorie</label>
            <p>{{$lead->category}}</p>
        </div>
    </div>
    <div class="col-md-4 product_info">
        <div class="col-md-12">
            <h4>&nbsp</h4>
        </div>
        
        <div class="form-group">
            <label for="Mode" class="control-label thin-weight">Mode de Chauffage</label>
            <p>{{$lead->Mode}}</p>
        </div>
        <div class="form-group">
            <label for="Personne" class="control-label thin-weight">Nombre de Personne(s) déclarée(s) à cette adresse</label>
            <p>{{$lead->Personne}}</p>
        </div>
        <div class="form-group">
            <label for="Fiscal1" class="control-label thin-weight">Numéro Fiscal 1</label>
            <p>{{$lead->Fiscal1}}</p>
        </div>
        <div class="form-group">
            <label for="RefAvis1" class="control-label thin-weight">Référence de l'avis 1</label>
            <p>{{$lead->RefAvis1}}</p>
        </div>
        <div class="form-group">
            <label for="Fiscal2" class="control-label thin-weight">Numéro Fiscal 2</label>
            <p>{{$lead->Fiscal2}}</p>
        </div>
        <div class="form-group">
            <label for="RefAvis2" class="control-label thin-weight">Référence de l'avis 2</label>
            <p>{{$lead->RefAvis2}}</p>
        </div>
        <div class="form-group">
            <label for="Revenue" class="control-label thin-weight">Revenu fiscale de référence </label>
            <p>{{$lead->Revenue}}</p>
        </div>
        <div class="form-group">
            <label for="Nom_de" class="control-label thin-weight">Nom de l'agent</label>
            <p>{{$lead->Nom_de}}</p>
        </div>
        <div class="form-group">
            <label for="Date_de" class="control-label thin-weight">Date de rdv</label>
            <p>{{$lead->Date_de}}</p>
        </div>
    </div>

@endsection