@extends('leads.detail')
@section('detail_product')
    <div class="col-md-4 product_info">
        <div class="form-group">
            <h4>Informations du produit</h4>
        </div>
		<div class="form-group">
        <label for="produit1" class="control-label thin-weight">Nom du produit</label>
        <p>{{$lead->produit1}}</p>
    </div>
        <div class="form-group">
            <label for="Surface" class="control-label thin-weight">Surface habitable en m2 de la maison</label>
            <p>{{$lead->Surface}}</p>
        </div>
        <div class="form-group">
            <label for="Zone" class="control-label thin-weight">Zone</label>
            @if(is_array($lead->Zone))
                @foreach($lead->Zone as $temp)
                    <span>{{$temp}}, </span>
                @endforeach
            @else
                <p>{{$lead->Zone}}</p>
            @endif
        </div>

        <div class="form-group">
            <label for="Quelle" class="control-label thin-weight">Quelle isolation est à jour </label>
            @if(is_array($lead->Quelle))
                @foreach($lead->Quelle as $temp)
                    <span>{{$temp}}, </span>
                @endforeach
            @else
                <p>{{$lead->Quelle}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="Commentaire" class="control-label thin-weight">Commentaire isolation</label>
            <p>{{$lead->Commentaire}}</p>
        </div>
        <div class="form-group">
            <label for="Mode de" class="control-label thin-weight">Mode de Chauffage</label>
            @if(is_array($lead->Mode_de))
                @foreach($lead->Mode_de as $temp)
                    <span>{{$temp}}, </span>
                @endforeach
            @else
                <p>{{$lead->Mode_de}}</p>
            @endif
        </div>
    </div>
    <div class="col-md-4 product_info">
        <div class="form-group">
            <label for="Situation" class="control-label thin-weight">Situation de maison</label>
            @if(is_array($lead->Situation))
                @foreach($lead->Situation as $temp)
                    <span>{{$temp}}, </span>
                @endforeach
            @else
                <p>{{$lead->Situation}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="Emploi" class="control-label thin-weight">Emploi salarié</label>
            @if(is_array($lead->Emploi))
                @foreach($lead->Emploi as $temp)
                    <span>{{$temp}}, </span>
                @endforeach
            @else
                <p>{{$lead->Emploi}}</p>
            @endif
        </div>
        
        <div class="form-group">
            <label for="Metier" class="control-label thin-weight">Métier</label>
            <p>{{$lead->Metier}}</p>
        </div>
    
        <div class="form-group">
            <label for="Personne" class="control-label thin-weight">Nombre de Personne(s) déclarée(s) à cette adresse</label>
            <p>{{$lead->Personne}}</p>
        </div>
        <div class="form-group">
            <label for="Fiscal1" class="control-label thin-weight">Numéro Fiscal 1</label>
            <p>{{$lead->Fiscal1}}</p>
        </div>
        <div class="form-group">
            <label for="RefAvis1" class="control-label thin-weight">Référence de l'avis 1</label>
            <p>{{$lead->RefAvis1}}</p>
        </div>
    </div>
    <div class="col-md-4 product_info">
        <div class="form-group">
            <label for="Fiscal2" class="control-label thin-weight">Numéro Fiscal 2</label>
            <p>{{$lead->Fiscal2}}</p>
        </div>
        <div class="form-group">
            <label for="RefAvis2" class="control-label thin-weight">Référence de l'avis 2</label>
            <p>{{$lead->RefAvis2}}</p>
        </div>

        <div class="form-group">
            <label for="Revenue" class="control-label thin-weight">Revenu fiscale de référence </label>
            <p>{{$lead->Revenue}}</p>
        </div>
        <div class="form-group">
            <label for="avis2" class="control-label thin-weight">Nom de l'agent</label>
            <p>{{$lead->avis2}}</p>
        </div>

        <div class="form-group">
            <label for="Revenue" class="control-label thin-weight">Date de rdv</label>
            <p>{{$lead->Revenue}}</p>
        </div>
    </div>
@endsection    