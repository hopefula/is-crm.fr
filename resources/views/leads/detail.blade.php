@extends('layouts.master')
@section('heading')
    {{ __('Edit lead') }}
@stop
 
@section('content')
 
<div class="row">
    <div id="tabsWithIcons" class="col-lg-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">

            <div class="widget-header d-none">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Create New Lead</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area rounded-pills-icon">
                <a class="btn btn-info" href="{{route('new_web.leads.show_all')}}">Retour</a>

                <ul class="nav nav-pills mb-4 mt-3  justify-content-center" id="rounded-pills-icon-tab" role="tablist">
                    <li class="nav-item ml-2 mr-2 d-none">
                        <a class="nav-link mb-2 text-center" id="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg> 
                        Chaudière</a>
                    </li>
                    <li class="nav-item ml-2 mr-2 d-none">
                        <a class="nav-link mb-2 text-center" data-toggle="pill" aria-selected="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user">
                        <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                        <circle cx="12" cy="7" r="4"></circle></svg>
                        ISOLATION INTERIEUR</a>
                    </li>
                    <li class="nav-item ml-2 mr-2  d-none">
                        <a class="nav-link mb-2 text-center" aria-controls="rounded-pills-icon-contact" aria-selected="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone">
                        <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg>
                        de la maison</a>
                    </li>
                    <li class="nav-item ml-2 mr-2  d-none">
                        <a class="nav-link mb-2 text-center" aria-selected="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg> 
                        POMPE A CHALEURE</a>
                    </li>
                    <li class="nav-item ml-2 mr-2  d-none">
                        <a class="nav-link mb-2 active text-center" aria-selected="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg> 
                        isolation par l’extérieur</a>
                    </li>
                </ul>
                <div class="tab-content" id="rounded-pills-icon-tabContent">
                    <div class="tab-pane fade show active" id="rounded-pills-icon-home" role="tabpanel" aria-labelledby="rounded-pills-icon-home-tab">
                        <form action="{{route('comments.create', $lead->id)}}" method="post" id="createTaskForm">
                            {{csrf_field()}}
                            <input type="hidden" name="lead_type" value='{{$lead->lead_type}}'>
                            <input type="hidden" name="type" value='lead'>
                            <input type="hidden" name="lead_id" value='{{$lead->id}}'>
                            <div class="row">
                                @include('leads.detail_product.customer')
                                @yield('detail_product')
                                @include('leads.detail_product.appointment')
                            </div>
                            
                            @foreach($lead->comments as $comment)
                            <div class="widget widget-activity-four col-md-4 mb-4">
                                <div class="widget-content">
                                    @if(Auth()->user()->userRole->role_id == 1 || Auth()->user()->userRole->role_id == 2)
                                    <span class="close" style="cursor:pointer; margin-right:10px;" onclick = "dell(this)" id ={{$comment->id}}>x</span>
                                    @endif
                                    <div class="mt-container mx-auto ps ps--active-y col-md-12">
                                        <div class="timeline-line">
                                            
                                            <div class="item-timeline timeline-primary">
                                                <div class="t-text">
                                                    <p>  {!! $comment->description !!}</p>
                                                </div>
                                            </div>

                                            <div class="item-timeline timeline-success">
                                                <div class="t-dot" data-original-title="" title="">
                                                </div>
                                                <div class="t-text">
                                                    <p><strong>{{$comment->user->name}} </strong></p>
                                                    <p class="t-time">
                                                        <p class="smalltext">{{ __('Créé le') }}:
                                                        {{ date(carbonFullDateWithText(), strtotime($comment->created_at))}}
                                                        <!-- @if($comment->updated_at != $comment->created_at)
                                                            <br/>{{ __('Modified') }} : {{date(carbonFullDateWithText(), strtotime($comment->updated_at))}}
                                                        @endif</p> -->
                                                    </p>
                                                </div>
                                            </div>

                                        </div>                                    
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            
                                <div class="form-group col-md-4">
                                    <label class="control-label thin-weight">Ajouter un commentaire</label>
                                    <textarea name="description" class="form-control"></textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="submit" class="btn btn-primary mb-2" id="createTask" value="{{__('Mettre à jour')}}">
                                </div>
                        </form>
                    </div>
                    
                </div>

            </div>
        </div>
    </div>
</div>

@stop
@push('style')
    <style>
        .picker, .picker__holder {
            width: 128%;
        }
        .picker--time .picker__holder {
            width: 30%;
        }
        .picker--time {
            min-width: 0px;
            max-width: 0px;
        }
    </style>
    <link href="{{asset('assets/css/scrollspyNav.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('plugins/animate/animate.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{asset('plugins/sweetalerts/promise-polyfill.js')}}"></script>
    <link href="{{asset('plugins/sweetalerts/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('plugins/sweetalerts/sweetalert.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/components/custom-sweetalert.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/elements/alert.css')}}">
      <style>
          .btn-light { border-color: transparent; }
      </style>
      <link rel="stylesheet" type="text/css" href="{{asset('plugins/editors/quill/quill.snow.css')}}">
      <link href="{{asset('assets/css/apps/mailbox.css')}}" rel="stylesheet" type="text/css" />
      <script src="plugins/sweetalerts/promise-polyfill.js"></script>
      <link href="{{asset('plugins/sweetalerts/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
      <link href="{{asset('plugins/sweetalerts/sweetalert.css')}}" rel="stylesheet" type="text/css" />
      <link href="{{asset('plugins/notification/snackbar/snackbar.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
<script src="{{asset('assets/js/ie11fix/fn.fix-padStart.js')}}"></script>
      <script src="{{asset('plugins/editors/quill/quill.js')}}"></script>
      <script src="{{asset('plugins/sweetalerts/sweetalert2.min.js')}}"></script>
      <script src="{{asset('plugins/notification/snackbar/snackbar.min.js')}}"></script>
      <!-- <script src="{{asset('assets/js/apps/custom-mailbox.js')}}"></script> -->
      <script src="{{asset('assets/js/scrollspyNav.js')}}"></script>
      <script src="{{asset('plugins/sweetalerts/sweetalert2.min.js')}}"></script>
      <script src="{{asset('plugins/sweetalerts/custom-sweetalert.js')}}"></script>
      
      <script>
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
            function dell(e){ 
                var i = e.getAttribute("id");
                e.parentElement.style.display = 'none';
                var data = {
                    "_token": "{{ csrf_token() }}",  
                    id: i
                };
                $.ajax({
                  url: '/comments/delete/'+i,
                  method: 'post',
                  data:{id: i},
                  success: function(res){
                      console.log(res)
                  }
                });
            }
    </script> 
      @if(session()->get('errors'))
        <script>
            const toast = swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                padding: '2em'
            });

            toast({
                type: 'error',
                title: "{{ session()->get('errors')->first() }}",
                padding: '2em',
            })
        </script>
        
    @endif
    
@endpush

