@extends('layouts.master')

@section('heading')
    {{ __('Edit user') }}
@stop

@section('content')
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
        <div class="widget-content widget-content-area br-6">
            <a class="btn btn-info" href="{{route('users.index')}}">Retour</a>

    {!! Form::model($user, [
            'method' => 'PATCH',
            'route' => ['users.update', $user->external_id],
            'files'=>true,
            'enctype' => 'multipart/form-data'
            ]) !!}
            {!! Form::close() !!}
            <form   action="{{route('users.update', [$user->external_id])}}"
                    method="POST"
                    enctype="multipart/form-data"
                    data-file="true"
            >
            @method('PATCH')
            {{csrf_field()}}
    @include('users.form', ['submitButtonText' =>  __('Valider')])
            </form>
    </div>
</div>
<!-- <script>
    function ddd(){
        var role_id = document.getElementById("role_id").value;
        var team1 = document.getElementById("team1");
        var team2 = document.getElementById("team2");
        if(role_id == 4){
            team2.style.display = "block";
            team1.style.display = "none"
        }
        else{
            team2.style.display = "none";
            team1.style.display = "block"
        }
    }
</script> -->
@stop
