@extends('layouts.master')
@section('heading')
    {{ __('Create user') }}
@stop

@section('content')
<div class="col-lg-12 layout-spacing">
    <div class="statbox widget box box-shadow">
        <div class="widget-header">                                
            <div class="row">
                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>Création d'un nouvel utilisateur</h4><br>
                    <a class="btn btn-info" href="{{route('users.index')}}">Retour</a><br>
                </div>                  
            </div>
        </div>
        <div class="widget-content widget-content-area">
            

        {!! Form::open([
                'route' => 'users.store',
                'files'=>true,
                'enctype' => 'multipart/form-data'

                ]) !!}
        @include('users.form', ['submitButtonText' => __('Valider')])

        {!! Form::close() !!}

        </div>
    </div>
</div>
@stop